import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import * as moment from 'jalali-moment';
import { IDatePickerConfig } from 'ngx-jdatepicker';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent implements OnInit ,OnChanges {
  @Input() selectedDate : any;
  @Input() disabled : any;
  @Input() drop : any;

  @Output() DATE_VALUE = new EventEmitter();
  datePickerConfig: IDatePickerConfig = {
    drops:'down',
    format: 'jYYYY-jMM-jD'
  };
  constructor() { 
  }
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {    
    this.datePickerConfig.drops = this.drop

  }

  getDate(event : any){
    if (event !== undefined) {
      // const date = moment(event).locale('en').format('YYYY-MM-DD');
      this.DATE_VALUE.emit(event);
    } else {
      this.DATE_VALUE.emit('');
    }
    
  }

}
