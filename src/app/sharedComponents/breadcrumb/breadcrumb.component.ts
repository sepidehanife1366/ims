import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreadcrumbService } from 'src/app/core/services/breadCrumb.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  breadcrumbs$: any = {
    label: '',
    url: ''
  };

  constructor(
    private readonly breadcrumbService: BreadcrumbService,
    private activatedRoute: ActivatedRoute,

  ) {
    this.breadcrumbs$ = breadcrumbService.breadcrumbs$.subscribe(res => {
      activatedRoute.queryParams.subscribe(
        params => {
          if (params['id'] != undefined && params['action'] != undefined) {
            this.breadcrumbs$.label = 'صفحه اصلی/ ویرایش'

          }
          else {
            this.breadcrumbs$ = res[0]

          }
        });
    });
  }

  ngOnInit(): void {

  }

}
