import { Component, ElementRef, HostListener, OnInit, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ProfileService } from 'src/app/core/services/profile.service';
import { UserService } from 'src/app/core/services/user.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: any;
  isMobile = false;
  notifs = false;
  notiflist: any = [];
  notifcount = [];
  cartables: any = []
  tooltip = false
  constructor(
    private authservice: AuthenticationService,
    private renderer: Renderer2,
    private elementRef: ElementRef,
    private nofificationservice: NotificationService,
    private router: Router,
    private domSanitizer: DomSanitizer,
    private userservice: UserService,
    private permissionsService: NgxPermissionsService

  ) { }

  ngOnInit(): void {
    this.responsiveMode();
    this.loadUser();
    this.loadCartables();
    setInterval(() => {
      this.loadNotification();
      this.loadCartables();
    }, 6000);
    // this._permo
  }
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    if (event.target.innerWidth <= 992) {
      this.isMobile = true
    }
    else {
      this.isMobile = false
    }
  }

  loadUser() {
    this.authservice.currentUser.subscribe(res => {
      if (res != null) {
        this.user = res
      }
      else {
        this.user = this.authservice.getUserConfig()
      }
    });
  }

  signOut() {
    this.authservice.signout();
  }

  responsiveMode() {
    if (window.innerWidth <= 992) {
      this.isMobile = true
    }
    else {
      this.isMobile = false
    }
  }
  menu_bar(mode: number) {
    let Element = this.elementRef.nativeElement.querySelector(
      '._menubar'
    ) as NodeList[];
    if (mode === 0) {
      this.renderer.setStyle(Element, 'right', '0');
    }
    else {
      this.renderer.setStyle(Element, 'right', '-250px');
    }
  }

  loadNotification() {
    const userId = JSON.parse(localStorage.getItem('userConfig') as any)?.userId
    this.nofificationservice.GetAllNotifications(userId).subscribe(response => {
      this.notiflist = response;
      this.notifcount = this.notiflist.map((c: any) => !c.hasSeen)
    })
  }
  toggleNotif() {
    this.notifs = !this.notifs
  }

  getmention(item: any) {
    this.nofificationservice.getnotification.next(item)
    const mention = { uId: item.userId, pId: item.recordId }
    localStorage.setItem('%%%G%%M', JSON.stringify(mention));
    this.router.navigate(['/'], { queryParams: { pid: item.recordId, uid: item.userId } })
  }

  loadCartables() {
    this.userservice.GetUserCartable().subscribe({
      next: (response) => {
        this.cartables = response
        this.userservice.cartables.next(this.cartables)
      }
    })
  }

  _permo(){
    this.permissionsService.loadPermissions(['Permissions.Graph.View'])
  }

}
