import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
  @Input() menu_Items: any = [];
  @Input() topicmode: boolean = false;
  @Output() GET_ACCORDION_ITEM = new EventEmitter<any>();
  mode = 0
  constructor() { }

  ngOnInit(): void {

  }

  ToggleItem(item: any) {
    if (item && item.childs.length > 0)
    item.isOpen = !item?.isOpen;
  }

  getItem(e : any , item : any ){
    this.GET_ACCORDION_ITEM.emit(item);
  }
  getItemvalue(e : any , item : any){
    this.GET_ACCORDION_ITEM.emit({checked : e.target.checked , item : item});

  }
  getchildsItem(e : any){
    this.GET_ACCORDION_ITEM.emit(e);

  }
}
