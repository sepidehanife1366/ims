import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { HeaderComponent } from './header/header.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { JalaliDatePipe, JalaliPipe } from '../core/pipes/jalalimoment.pipe';
import { NgSelectModule } from '@ng-select/ng-select';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import { ModalModule } from 'ngx-bootstrap/modal';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { NgxJDatePickerModule } from 'ngx-jdatepicker';
import {MatStepperModule} from '@angular/material/stepper';
import { AlertModule } from 'ngx-bootstrap/alert';
import {MatMenuModule} from '@angular/material/menu';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatListModule} from '@angular/material/list';
import { PackagetreeComponent } from './packagetree/packagetree.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AccordionComponent } from './packagetree/accordion/accordion.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatBadgeModule} from '@angular/material/badge';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AppTooltipDirective } from '../core/tooltip/tooltip';
import { ChatComponent } from '../routes/panel/chat/chat.component';
import { ChatSearchComponent } from '../routes/panel/chat/components/chat-search/chat-search.component';
import { MessageComponent } from '../routes/panel/chat/components/message/message.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ChatroomComponent } from '../routes/panel/chat/components/chatroom/chatroom.component';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    DragDropModule,
    CKEditorModule,
    PaginationModule.forRoot(),  
    MatSelectModule,
    MatRadioModule,
    ModalModule.forRoot(),
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    NgxJDatePickerModule,
    MatStepperModule,
    AlertModule.forRoot(),
    MatMenuModule,
    MatCheckboxModule,
    MatListModule,
    AccordionModule.forRoot(),
    MatExpansionModule,
    MatBadgeModule,
    MatTooltipModule,
    TooltipModule.forRoot(),
    MatAutocompleteModule,
    InfiniteScrollModule,
    NgxPermissionsModule
    
  ],
  declarations: [
    HeaderComponent,
    JalaliPipe ,
    JalaliDatePipe,
    DatepickerComponent,
    PackagetreeComponent,
    AccordionComponent,
    BreadcrumbComponent,
    AppTooltipDirective,
    ChatComponent,
    ChatSearchComponent,
    MessageComponent,
    ChatroomComponent
    

  ],
  exports: [
    DragDropModule,
    CKEditorModule,
    HeaderComponent,
    PaginationModule,
    JalaliPipe,
    JalaliDatePipe,
    NgSelectModule,
    MatSelectModule,
    MatRadioModule,
    JalaliDatePipe,
    ModalModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    NgxJDatePickerModule,
    MatStepperModule,
    AlertModule,
    DatepickerComponent,
    MatMenuModule,
    MatCheckboxModule,
    MatListModule,
    AccordionModule,
    PackagetreeComponent,
    AccordionComponent,
    MatExpansionModule,
    MatBadgeModule,
    BreadcrumbComponent,
    MatTooltipModule,
    TooltipModule,
    AppTooltipDirective,
    ChatComponent,
    ChatSearchComponent,
    MessageComponent,
    MatAutocompleteModule,
    InfiniteScrollModule,
    NgxPermissionsModule
  ]
})

export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
    };
  }
}