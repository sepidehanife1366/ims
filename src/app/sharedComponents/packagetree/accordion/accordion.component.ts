import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit , OnChanges{
  @Input() menu_Items: any = [];
  @Input() topicmode: any;
  @Input() toolbar : any;
  @Input() chekboxmode : any
  @Output() GETITEM = new EventEmitter<any>();
  @Output() ADD_PACKAGE = new EventEmitter<any>();

  mode = 0
  editedValue = ''
  constructor() { }
  ngOnChanges(): void {
    
    
  }
  
  ngOnInit(): void {    
  }

  ToggleItem(item: any) {
      if (item != undefined && item.childs.length > 0)
      item.isOpen = !item?.isOpen;
  }

  getItem(item : any ){
      this.GETITEM.emit({item : item , topicMode : this.topicmode});
  }
  getchildsItem(e : any){
    this.GETITEM.emit(e);
  }
  getItemvalue(e : any , item : any){    
    this.GETITEM.emit({checked : e.checked , item : item , topicMode : this.topicmode});

  }
  hover(e : any , item : any , mode : any){
    if (mode === 0) {
      item.showTools = true
    }
    else{
      item.showTools = false
    }
  }
  addpackage(e : any){
    this.GETITEM.emit({mode : 'add' , item : e , topicMode : this.topicmode});
  }
  editpackage(e : any){
    e.isEdit = true
  }
  removepackage(e : any){
    this.GETITEM.emit({mode : 'remove' , item : e , topicMode : this.topicmode});
  }
  getEditedvalue(value : any){    
    this.editedValue = value.target.value
  }
  focusOut(e : any , item : any){
    item.isEdit = false
    this.GETITEM.emit({mode : 'edit' , item : item , value :  this.editedValue , topicMode : this.topicmode});
  }

}
