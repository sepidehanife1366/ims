import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { PackageService } from 'src/app/core/services/package.service';
import { TopicService } from 'src/app/core/services/topic.service';

@Component({
  selector: 'app-packagetree',
  templateUrl: './packagetree.component.html',
  styleUrls: ['./packagetree.component.scss']
})
export class PackagetreeComponent implements OnInit, OnChanges {
  @Input() title = ''
  @Input() tree: any;
  @Input() topicmode: boolean | any;
  @Input() ralatedtopicsMode: boolean = false;
  @Input() toolbar: any
  @Input() nonBorder: any;
  @Input() chekboxmode: any;
  @Input() showaddIcon: any = true;
  @Input() packageId : any;

  items: any
  @Output() GETITEM = new EventEmitter<any>()

  selectedTopics: any = []
  packages: any;
  counter = 0
  topiccounter = 0
  max = [];
  baseParent = {
    title: '',
    item : {
      parentId: null,
      id: null,
    },
    isOpen: false,
    
  }
  currentpackageId : any
  currentTopicId : any


  constructor(
    private packageservice: PackageService,
    private topicservice: TopicService
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnInit(): void {
  }
  ToggleItem(item: any) {
    item.isOpen = !item?.isOpen;
  }

  getPackageTree() {
    this.packageservice.GetPackageTree().subscribe({
      next: (response: any) => {
        this.tree = {
          tree: response,
          topicMode: false,
        };
      },
      error: (err) => {

      }
    })
  }
  loadTopics(packageId: any) {
    const model = {
      packageId: packageId
    }
    this.topicservice.GetTopicTree(model).subscribe({
      next: (response) => {
        this.tree = response

        this.tree = {
          tree: response,
          topicMode: true
        };
      }
    })
  }

  GET_ITEM(item: any) {
    if (item.mode === 'add') {
      this.ADD_PACKAGE(item);
      return
    }
   else if (item.mode === 'edit') {
      this.EDIT_PACKAGE(item)
      return
    }
  else if (item.mode === 'remove') {
      this.REMOVE_PACKAGE(item)
      return
    }
   else {
      this.GETITEM.emit(item)
    }
  }

  ADD_PACKAGE(e: any) {
    if (!e.topicMode && !this.topicmode) {
      this.counter = this.counter + 1
      let title = 'new-' + (this.counter)
      const model = {
        packageTitle: title,
        parentId: e.item.id
      }
      this.packageservice.CreatePackage(model).subscribe({
        next: (response) => {
          this.getPackageTree();
        }
      })
    }
    else {

      this.topiccounter = this.topiccounter + 1
      let title = 'new-' + (this.topiccounter)
      const model = {
        title: title,
        parentId: e.item.id
      }
      this.topicservice.CreateTopic(model).subscribe({
        next: (response) => {
          if (response) {
            this.addtopictopackage(response , this.packageId )

          }
        }
      })

    }
  }

  addtopictopackage(topicId: any , packageId : any){
    const model = {
      packageId: packageId,
      topicId: topicId
    }
    this.packageservice.AddTopicToPackage(model).subscribe({
      next : (response)=>{
        this.loadTopics(packageId)
      }
    })

  }
  EDIT_PACKAGE(e: any) {
    let model = []
    if (!e.topicMode) {
      
      model.push({ path: '/title', op: 'add', value: e.value })

      this.packageservice.UpdatePackage(e.item.id, model).subscribe({
        next: (response) => {
          this.getPackageTree()
        }
      })

    }
    else {
      model.push({ path: '/title', op: 'add', value: e.value })
      this.topicservice.UpdateTopic(e.item.id, model).subscribe({
        next: (response) => {
          this.loadTopics(this.packageId)
        }
      })

    }

  }
  REMOVE_PACKAGE(e: any) {
    if (!e.topicMode && !this.topicmode) {
      const model = {
        id: e.item.id
      }
      this.packageservice.RemovePackage(model).subscribe({
        next: (response) => {
          this.getPackageTree()
        }
      })

    }
    else {
      const model = {
        id: e.item.id
      }
      this.topicservice.DeleteTopic(model).subscribe({
        next: (response) => {
          // this.deleteTopicFromPackage(response , this.packageId)
         this.loadTopics(this.packageId)
        }
      })
    }

  }
  deleteTopicFromPackage(topicId: any , packageId : any){
    const model = {
      packageId: packageId,
      topicId: topicId
    }
    this.packageservice.DeleteTopicFromPackage(model).subscribe({
      next : (response)=>{
      }
    })

  }


}
