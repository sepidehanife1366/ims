import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackagetreeComponent } from './packagetree.component';

describe('PackagetreeComponent', () => {
  let component: PackagetreeComponent;
  let fixture: ComponentFixture<PackagetreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackagetreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackagetreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
