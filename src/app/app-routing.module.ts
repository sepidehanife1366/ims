import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { IsLoggedGuard } from './core/guards/isLogged.guard';
import { IsGuestGuard } from './core/guards/isguest.guard';
import { PanelComponent } from './routes/panel/panel.component';
import { NotfoundComponent } from './routes/notfound/notfound.component';

const routes: Routes = [

        {
        path: 'auth',
        canActivate: [IsGuestGuard],
        component: AppComponent,
        children: [
          {
            path: '',
            canActivate: [],
            children: [
              { path: '', loadChildren: () => import('./routes/account/account.module').then(m => m.AccountModule) },
            ]
          },
        ]
      },
      {
        path: '',
        canActivate: [IsLoggedGuard],
        component : PanelComponent,
        children: [
          { path: '', loadChildren: () => import('./routes/panel/panel.module').then(m => m.PanelModule) },
        ]
      },
      {
        path: '**',
        component : NotfoundComponent,
      },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
