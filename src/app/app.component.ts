import { Component } from '@angular/core';
import { UserService } from './core/services/user.service';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'سامانه مدیریت یکپارچه دانشی';
  constructor(
    private userservice : UserService,
    private permissionsService: NgxPermissionsService

  ){
    // this.checkValidToken();
    this._currentuser();
  }


  _currentuser(){
    this.userservice.GetCurrentUser().subscribe(response =>{
      this.permissionsService.loadPermissions(response.permissions)
    })
  }


}
