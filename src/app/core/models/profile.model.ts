
export interface ProfileModel {
    about : string;
    birthDate : string;
    cityId : number;
    firstname : string;
    id : number;
    job : string;
    lastname : string;
    nationalCode : string;
    picture : string;
    phoneNumber : string ;
    profileAwards: [];
    profileCertificates : [];
    profileContactInfos :[];
    profileEducations : [];
    profileJobs :[];
    profileLanguages :[],
    email : string,
    username : string,
    usersubject : number
  }