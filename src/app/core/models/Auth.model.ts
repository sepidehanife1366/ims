export class UserConfig{
 constructor(
   public token: string,
   public isLogged: boolean,
   public userType: any,
   public username : string,
   public firstname : string ,
   public lastname : string ,
   public picture : string,
   public userId : number
 ){

 }
}

export interface reqModel {
  username : string;
  password : string;
}
export interface resModel {
  email : string;
  id : number;
  roles : any;
  token : string;
  successful :boolean;
  username : string;
}
