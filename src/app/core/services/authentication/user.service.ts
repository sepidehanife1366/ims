import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetCurrentUser() {
    return this.http.get<any>('user/GetCurrentUser').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetUserlist(model : any) {
    return this.http.post<any>('user/GetUserlist',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetLightUserList(model : any) {
    return this.http.post<any>('user/GetlightUserlist',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetUserRoleList(model : any) {
    return this.http.post<any>('user/GetRoleList',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetPermissionList(model : any) {
    return this.http.post<any>('user/GetPermissionList',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetRolePermission(model : any) {
    return this.http.post<any>('user/SetRolePermission',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertRole(model : any) {
    return this.http.post<any>('user/UpsertRole',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  AssignRoleToUser(model : any) {
    return this.http.post<any>('user/AssignRoleToUser',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertUser(model : any) {
    return this.http.post<any>('user/UpsertUser',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  NewChat(model : any) {
    return this.http.post<any>('user/NewChat',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SearchUserName(model : any) {
    return this.http.post<any>('user/SerchUserName',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetUserById(model : any) {
    return this.http.post<any>('user/GetUserById',model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}