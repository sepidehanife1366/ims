import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CloudStorageService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  UploadFile(model : any) {
    return this.http.post<any>('ClouadStorage/UploadFile' , model ).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}