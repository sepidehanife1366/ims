import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TopicService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetTopicTree(model : any) {
    return this.http.post<any>('Topic/GetTopicTree' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  CreateOrUpdateTopic(model : any) {
    return this.http.post<any>('Topic/CreateOrUpdateTopic' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  DeleteTopic(model : any) {
    return this.http.delete<any>('Topic/DeleteTopic' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  
  GetTopic(model : any) {
    return this.http.get<any>('Topic/GetTopic?').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetTopicManager(model : any) {
    return this.http.post<any>('Topic/SetTopicManager' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetTopicMember(model : any) {
    return this.http.post<any>('Topic/SetTopicMember' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetTopicExpert(model : any) {
    return this.http.post<any>('Topic/SetTopicExpert' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}