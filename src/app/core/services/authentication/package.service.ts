import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PackageService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetPackageTree() {
    return this.http.get<any>('Package/GetPackageTree').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetPackage() {
    return this.http.get<any>('Package/GetPackage').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetLightPackage() {
    return this.http.get<any>('Package/GetlightPackage').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  
  CreateOrUpdatePackage(model : any) {
    return this.http.post<any>('Package/CreateOrUpdatePackage' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}