import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetForms(){
    return this.http.get<any>('Form/getforms').pipe(
      map(
        (response : any) =>{
        return response
      })
    )
  }
  
  GetFormById(model : any) {
    return this.http.get<any>('Form/GetFormById?Id=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  CreateOrUpdateform(model : any) {
    return this.http.post<any>('Form/CreateOrUpdareform' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  DeleteForm(model : any) {
    return this.http.delete<any>('Form/deleteform' , {body: model}).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

}