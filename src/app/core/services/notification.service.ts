import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  getnotification = new BehaviorSubject(null);

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetAllNotifications(model : any) {
    return this.http.get<any>('Notification/GetAllNotifications/GetAllNotifications?userId=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetMention() {
    return this.http.get<any>('Notification/GetMention/GetMention').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}