import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  UserListReport(model : any) {
    return this.http.post<any>('Report/UserListReport' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  TopicReport(model : any) {
    return this.http.post<any>('Report/TopicReport' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}