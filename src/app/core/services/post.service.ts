import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PostService {
  getdashbord = new BehaviorSubject(null);


  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetAllPostes(model : any) {
    return this.http.post<any>('post/getallposts' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  CreateOrUpdatepost(model : any) {
    return this.http.post<any>('post/CreateOrUpdatepost' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  CreateOrUpdateEntityIntraction(model : any) {
    return this.http.post<any>('Post/CreateOrUpdateEntityIntraction' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  DeleteEntityIntraction(model : any) {
    return this.http.post<any>('post/DeleteEntityIntraction' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetEntityIntractions(model : any) {
    return this.http.get<any>('post/GetEntityIntractions?RecordId=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  CreateOrUpdateComment(model : any) {
    return this.http.post<any>('post/createorupdatecomment' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  DeleteComment(model : any) {    
    return this.http.delete<any>('post/deletecomment',{body: model}).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetPostComments(postId : any) {
    return this.http.post<any>('post/getpostcomments', postId).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}

