import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  profileInfo = new BehaviorSubject(null);
  relaodData = new BehaviorSubject(false);
  relaodprofile = new BehaviorSubject(false);


  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetUserProfileById(userId : number) {
    return this.http.get<any>('Profile/GetUserProfileById?userId=' + userId).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpdateProfileInformation(model : any) {
    return this.http.post<any>('profile/updateprofileinformation' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertProfileAward(model : any) {
    return this.http.post<any>('profile/UpsertProfileAward' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertProfileCertificate(model : any) {
    return this.http.post<any>('profile/UpsertProfileCertificate' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertProfileContactInfo(model : any) {
    return this.http.post<any>('profile/UpsertProfileContactInfo' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertProfileEducation(model : any) {
    return this.http.post<any>('profile/UpsertProfileEducation' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertProfileJob(model : any) {
    return this.http.post<any>('profile/UpsertProfileJob' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertProfileLanguage(model : any) {
    return this.http.post<any>('profile/UpsertProfileLanguage' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  RemoveProfileCertificate(model : any) {
    return this.http.post<any>('profile/RemoveProfileCertificate' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  RemoveProfileAward(model : any) {
    return this.http.post<any>('profile/RemoveProfileAward' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  RemoveProfileEducation(model : any) {
    return this.http.post<any>('profile/RemoveProfileEducation' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  RemoveProfileJob(model : any) {
    return this.http.post<any>('profile/RemoveProfileJob' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  RemoveProfileLanguage(model : any) {
    return this.http.post<any>('profile/RemoveProfileLanguage' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetProfileColleagues(profileId : any) {
    return this.http.get<any>('profile/GetProfileColleagues?profileId=' + profileId).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }


}


