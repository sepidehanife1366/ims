import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RxStompService } from '@stomp/ng2-stompjs';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  roomId = new BehaviorSubject(null);
  webSocketRecive = new Subject<null>();
  subscription !: Subscription
  constructor(private rxStompService: RxStompService, private httpClient: HttpClient) { }
  disconnected() {
    if (this.subscription != null) {
    this.subscription.unsubscribe();
    }
    this.webSocketRecive.next(null);
  }
  websoket(id: any) {
    this.subscription = this.rxStompService.watch('/exchange/' + id).subscribe(message => {
      this.onRecived(message.body);
    });
  }
  public onSend(id: any, message: any) {
    this.rxStompService.publish({ destination: '/exchange/' + id, body: message })
  }
  private onRecived(msg: any) {
    this.webSocketRecive.next(msg);
  }
}
