import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CloudStorageService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  UploadFile(model : any) {
    return this.http.post<any>('ClouadStorage/UploadFile' , model ).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetVideoAsStream(model : any) {
    // const headerDict = {
    //   'Content-Type': 'Content-Type: application/octet-stream',
    //   'Accept': 'application/octet-stream',
    //   'Access-Control-Allow-Headers': 'Content-Type',
    // }
    // const headers = new HttpHeaders(headerDict)
    return this.http.get<any>('ClouadStorage/GetVideoAsStream?filePath=' + model , {responseType : 'blob' as any} ).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetFile(model : any) {
    return this.http.get<any>('ClouadStorage/GetFile?filePath=' + model, {responseType : 'blob' as any} ).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetImageBase64(model : any) {
      const headerDict = {
      'Content-Type': 'text/plain',
      'Accept': 'text/plain',
    }
    const headers = new HttpHeaders(headerDict)
    return this.http.post<any>('ClouadStorage/GetImageBase64' , model ,  {responseType : 'text' as any} ).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  blobFile(url : any){
    return this.http.get<any>(url , {responseType : 'blob' as any})
  }
}