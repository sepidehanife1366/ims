import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetMySpaces() {
    return this.http.get<any>('chat/GetMySpaces').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertSpace(model : any) {
    return this.http.post<any>('chat/UpsertSpace' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpsertRoom(model : any) {
    return this.http.post<any>('chat/UpsertRoom' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetRoom(model : any) {
    return this.http.get<any>('chat/GetRoom?RoomId=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetMyRooms() {
    return this.http.get<any>('chat/GetMyRooms').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetRoomList(model : any) {
    return this.http.post<any>('chat/GetRoomList' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  GetSpaceRooms(model : any) {
    return this.http.get<any>('chat/GetSpaceRooms?SpaceId=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetRoomMessages(model : any) {
    return this.http.post<any>('chat/GetRoomMessages' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetPublicRooms(model : any) {
    return this.http.post<any>('chat/GetPublicRooms' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  JoinToRoom(model : any) {
    return this.http.post<any>('chat/JoinToRoom' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetRoomMembers(model : any) {
    return this.http.post<any>('chat/GetRoomMembers' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  DeleteRoom(model : any) {
    return this.http.get<any>('chat/deleteroom/'+ model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

}