import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PackageService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetPackageTree() {
    return this.http.get<any>('Package/GetPackageTree').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetPackage(model : any) {
    return this.http.get<any>('Package/GetPackage?id=' + model ).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetLightPackage() {
    return this.http.get<any>('Package/GetalllightPackages').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  
  CreatePackage(model : any) {
    return this.http.post<any>('Package/CreatePackage' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpdatePackage(id : any , model : any) {
    return this.http.patch<any>('Package/UpdatePackage/'+ id , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  RemovePackage(model : any) {
    return this.http.delete<any>('Package/DeletePackage' , {body :model}).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetEvaluationSetting(model : any) {
    return this.http.get<any>('Package/GetEvaluationSetting?PackageId=' + model ).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpdateEvaluationSetting(model : any) {
    return this.http.put<any>('Package/UpdateEvaluationSetting' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetEvaluationQuestions(model : any) {
    return this.http.get<any>('Package/GetEvaluationQuestions?EvaluationSettingId=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  UpsertEvaluationQuestion(model : any) {
    return this.http.post<any>('Package/UpsertEvaluationQuestion' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  DeleteEvaluationQuestion(model : any) {
    return this.http.delete<any>('Package/DeleteEvaluationQuestion' , {body : model}).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
    
  AddTopicToPackage(model : any) {
    return this.http.post<any>('Package/AddTopicToPackage' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  DeleteTopicFromPackage(model : any) {
    return this.http.post<any>('Package/DeleteTopicFromPackage' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetPackageKnowledgEngineerTopic(model : any) {
    return this.http.post<any>('Package/SetPackageKnowledgEngineerTopic' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

}