import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TopicService {

  topicIds = new BehaviorSubject(null)
  initial = new BehaviorSubject(false)

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetTopicTree(model : any) {
    return this.http.post<any>('Topic/GetTopicTree' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  CreateTopic(model : any) {
    return this.http.post<any>('Topic/CreateTopic' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpdateTopic(id : any ,model : any ) {
    return this.http.patch<any>('Topic/UpdateTopic/'+id , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  DeleteTopic(model : any) {
    return this.http.delete<any>('Topic/DeleteTopic' , {body :model}).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  
  GetTopic(model : any) {
    return this.http.get<any>('Topic/GetTopic?id=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetTopicManager(model : any) {
    return this.http.post<any>('Topic/SetTopicManager' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetTopicMember(model : any) {
    return this.http.post<any>('Topic/SetTopicMember' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  SetTopicExpert(model : any) {
    return this.http.post<any>('Topic/SetTopicExpert' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpdateTopicSetting(model : any) {
    return this.http.put<any>('Topic/UpdateTopicSetting' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetTopicUserPermissions(model : any) {
    return this.http.get<any>('Topic/GetTopicUserPermissions?TopicSettingId=' + model).pipe(
      map(
        (response: any) => {
          return response
        }),
        
    )
  }

  UpsertTopicUserPermission(model : any) {
    return this.http.post<any>('Topic/UpsertTopicUserPermission' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  DeleteTopicUserPermission(model : any) {
    return this.http.delete<any>('Topic/DeleteTopicUserPermission/' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetTopicSetting(model : any) {
    return this.http.get<any>('Topic/GetTopicSetting?TopicId=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetTopicManager(model : any) {
    return this.http.get<any>('Topic/GetTopicManager/' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetTopicMember(model : any) {
    return this.http.get<any>('Topic/GetTopicMember/' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetTopicExpert(model : any) {
    return this.http.get<any>('Topic/GetTopicExpert/' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetLightTopic(model : any) {
    return this.http.get<any>('Topic/GetLightTopic?PackageId=' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  

  
  
}
