import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map , BehaviorSubject, Observable } from 'rxjs';
import { UserConfig, reqModel, resModel } from '../models/Auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  LoggedIn :boolean |any ;
  usercfg : UserConfig = {
    token: '',
    isLogged: false,
    userType: [],
    username : '' ,
    firstname : '' ,
    lastname : '' ,
    picture : '' ,
    userId : 0
  };

  userConfig = new BehaviorSubject<UserConfig>(this.usercfg);
  token = new BehaviorSubject<string>('');
  currentUser = new BehaviorSubject<null>(null);

  private static hasToken(): boolean {
  const token = JSON.parse(localStorage.getItem('userConfig') as any)?.token;    
    if (token) {
      return true;
    }
    return false;
  }

  isLogin(): boolean {
    return AuthenticationService.hasToken();
  }


  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    // this.autoLogin();
   }

  public setUserConfig(
    userType: any, 
    token: string, 
    isLogged: boolean , username : string , firstname : string , lastname : string , picture : string
   , userId : number ) {
    const userConfig = new UserConfig(token, isLogged, userType , username  , firstname , lastname , picture , userId);
    this.userConfig.next(userConfig);
    localStorage.setItem('userConfig', JSON.stringify(userConfig));
  }

  getUserConfig() {
    // in future we will have to check userConfig first
    const localData = localStorage.getItem('userConfig');
    return JSON.parse(localData === null ? 'null' : localData);
  }

  autoLogin() {
    // check localStorage to see if there is any user login info
    // then set app mode to logged in
    const localData: string | null = localStorage.getItem('userConfig');
    if (localData === null) {
      this.setUserConfig('', '', false , '' , '' , '' , '' , 0);
      return;
    }

    const userData: {
      userType: any;
      token: string;
      isLogged: boolean;
      usename : string;
      firstname : string ;
      lastname : string;
      picture : string ,
      userId : number
    } = JSON.parse(localData === null ? 'null' : localData);

    if (userData.isLogged) {
      this.setUserConfig(userData.userType, userData.token, true , userData.usename , userData.firstname , userData.lastname , userData.picture , userData.userId);
    }
  }

  login(model : reqModel){
    return this.http.post<resModel>('Authentication/Login' , model).pipe(
      map(
        (response : resModel) =>{
        return response
      })
    )
  }

  signout(){
    localStorage.clear();
    this.userConfig.next({
      token: '',
      isLogged: false,
      userType: [],
      username : '' ,
      firstname  : '' ,
      lastname : '' ,
      picture : '' ,
      userId : 0
    });
    this.router.navigate(['/auth'])
  }

  TwoFactorLogin(model : any){
    return this.http.post<resModel>('Authentication/TwoFactorLogin' , model).pipe(
      map(
        (response : resModel) =>{
        return response
      })
    )
  }
  ResendToken(model : any){
    return this.http.post<resModel>('Authentication/resendtoken' , model).pipe(
      map(
        (response : any) =>{
        return response
      })
    )
  }
  ChangePassword(model : any){
    return this.http.post<any>('Authentication/changepassword' , model).pipe(
      map(
        (response : any) =>{
        return response
      })
    )
  }
  ForgetPassword(model : any){
    return this.http.post<any>('Authentication/forgetpassword' , model).pipe(
      map(
        (response : any) =>{
        return response
      })
    )
  }
  ResetPassword(model : any){
    return this.http.post<any>('Authentication/resetpassword' , model).pipe(
      map(
        (response : any) =>{
        return response
      })
    )
  }
  IsValidToken(){
    return this.http.get<any>('Authentication/isvalidtoken').pipe(
      map(
        (response : any) =>{          
        return response
      })
    )
  }


}
