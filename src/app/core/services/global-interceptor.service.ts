import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from "rxjs";
import { catchError, exhaustMap, map, take } from 'rxjs/operators';
import { Router } from "@angular/router";
import { environment } from "src/environments/environment.prod";
import { Exepetions } from "../enums/exeptions.enum";
import { ToastrService } from "ngx-toastr";
import { AuthenticationService } from "./authentication.service";


@Injectable({
  providedIn: 'root'
})
export class GlobalHttpInterceptorService implements HttpInterceptor {

  constructor(
    public router: Router,
    private authservice: AuthenticationService,
    private toastService : ToastrService
  ) {

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token')    
    req = req.clone({
      url: environment.httpConfig.url + req.url, setHeaders: { Authorization: `Bearer ${token}` }
    });
    return next.handle(req).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse) {          
          if (error.error instanceof ErrorEvent) {
              console.error("Err");

          } else {
            this.checkErrorSttatus(error.status);
          } 
      } else {
          console.error("err");
      }
        return throwError(error.status);
      })
    )
  }

  checkErrorSttatus(errNumber: number) {
    switch (errNumber) {
      case 401:
        {
          this.toastService.error("نام کاربری یا رمز عبور اشتباه است", '401',
            { timeOut: 2000, enableHtml: true, closeButton: false, positionClass: 'toast-top-center' });
          sessionStorage.clear();
          this.router.navigate(['/auth'])
          break;
        }

      case 403:
        {
          this.toastService.error("دسترسی غیر مجاز", '403',
            { timeOut: 2000, enableHtml: true, closeButton: false, positionClass: 'toast-top-center' });
          break;
        }

      case 500:
        {
          this.toastService.error("خطای داخلی سرور", '500',
            { timeOut: 2000, enableHtml: true, closeButton: false, positionClass: 'toast-top-center' });

          break;
        }

      case 503:
        {
          this.toastService.error("سرویس خارج از دسترس", '500',
            { timeOut: 2000, enableHtml: true, closeButton: false, positionClass: 'toast-top-center' });

          break;
        }

      case 404:
        {
          this.toastService.error("NotFound", '404', { timeOut: 2000, enableHtml: true, closeButton: false , positionClass: 'toast-top-center' });
          break;
        }

      case 504:
        {
          this.toastService.error("TimeOut", '504', { timeOut: 2000, enableHtml: true, closeButton: false , positionClass: 'toast-top-center'  });
          break;
        }
    }
  }


  signout() {
    this.authservice.signout();
  }
}


