import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  search = new BehaviorSubject(null)

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GeneralSearch(model : any) {
    return this.http.post<any>('Search/GeneralSearch' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  AdvanceSearch(model : any) {
    return this.http.post<any>('Search/AdvanceSearch' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  MemberSearch(model : any) {
    return this.http.post<any>('Search/MemberSearch' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  getLightUserList(username: string): Observable<any> {
    return this.http.post('User/GetLightUserList', { filter: username }).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
  getUserGraph(userId: number): Observable<any> {
    return this.http.get('Report/Graph?UserId=' + userId).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
}