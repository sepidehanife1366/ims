import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  GetSubjects() {
    return this.http.get<any>('Subject/GetSubjects').pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  CreateOrUpdateSubject(model : any) {
    return this.http.post<any>('Subject/CreateOrUpdateSubject' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  GetSubject(model : any) {
    return this.http.get<any>('Subject/GetSubject/' +  model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
  UpdateSubjectStatus(model : any) {
    return this.http.post<any>('Subject/UpdateSubjectStatus' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  GetUserSubjects(id : any) {
    return this.http.get<any>('subject/GetUserSubjects?id=' + id).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }


  GetSubjectEvaluationQuestions(model : any) {
    return this.http.get<any>('subject/getsubjectevaluationquestions/' + model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }

  SubmitSubjectAnswers(model : any) {
    return this.http.post<any>('Subject/SubmitSubjectAnswers' , model).pipe(
      map(
        (response: any) => {
          return response
        })
    )
  }
}