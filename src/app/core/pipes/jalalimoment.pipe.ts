
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'jalali-moment';

@Pipe({
  name: 'jalali'
})
export class JalaliPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value != undefined) {
      try {
        let MomentDate = moment(value, 'YYYY/MM/DD');
        return MomentDate.locale('fa').format('YYYY/M/D ساعت HH:mm');
      } catch (error) {
        console.log('not a valid date');
      }
    }
  }
}
@Pipe({
  name: 'jalali_date'
})
export class JalaliDatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value != undefined) {
      try {
        let MomentDate = moment(value, 'YYYY/MM/DD');
        return MomentDate.locale('fa').format('YYYY/M/D');
      } catch (error) {
        console.log('not a valid date');
      }
    }
  }
}