import {Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot} from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'

})
export class IsGuestGuard implements CanActivate {
  constructor(
    private router: Router,
    private authSV: AuthenticationService,
  ) {
  }


  canActivate(next: ActivatedRouteSnapshot) {
    if (this.authSV.isLogin()) {
      return this.router.navigate(['/']);
    } else {
      return true;
    }
  }
}
