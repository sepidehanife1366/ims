import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'

})
export class IsLoggedGuard implements CanActivate {
  constructor(
    private router: Router,
    private authSV: AuthenticationService
  ) {
  }


  canActivate(next: ActivatedRouteSnapshot) {
    if (this.authSV.isLogin()) {
      return true;
    } else {
      return this.router.navigate(['/auth']);
    }
  }
}