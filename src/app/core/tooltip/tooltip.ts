import { Directive, Input, ElementRef, HostListener, Renderer2, OnInit, ViewChild } from '@angular/core';

@Directive({
  selector: '[app-tooltip]'
})
export class AppTooltipDirective implements OnInit {
  @Input('app-tooltip') tooltipTitle !: string;
  tooltip: HTMLElement | any;
  tooltipHeader: HTMLElement | any;
  offset = 16;
  placement = 'top'
  @ViewChild('tooltip') tooltipref:ElementRef | any;

  constructor(private el: ElementRef, private renderer: Renderer2) {
   }

  ngOnInit() {
    
    //this.showTooltip();
  }
  @HostListener('mouseover')
  onMouseIn() {
    if (!this.tooltip)this.showTooltip();
  }

  @HostListener('mouseleave')
  onMouseOut() {
    if (this.tooltip) this.hideTooltip();
  }

  showTooltip() {
    this.tooltip = this.renderer.createElement('span');
    this.tooltipHeader = this.renderer.createComment('span');

    this.renderer.appendChild(
      this.tooltip,
      this.renderer.createText(this.tooltipTitle)
    );

    this.renderer.appendChild(this.el.nativeElement, this.tooltip);

    this.renderer.addClass(this.tooltip, 'ng-tooltip');
    this.renderer.addClass(this.tooltip, `ng-tooltip-top`);
    this.renderer.addClass(this.tooltip, 'ng-tooltip-show');
  }

  hideTooltip() {
    this.renderer.removeChild(document.body, this.tooltip);
    this.tooltip = null;
  }


}