import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReportService } from 'src/app/core/services/report.service';

@Component({
  selector: 'app-topicreport',
  templateUrl: './topicreport.component.html',
  styleUrls: ['./topicreport.component.scss']
})
export class TopicreportComponent implements OnInit {

  appform : FormGroup | any
  selectedDate = {
    from: "",
    to: ""
  }
  reports : any =[]

  constructor(
    private formBuilder: FormBuilder,
    private reportservice : ReportService
  ) { }



  ngOnInit(): void {
    this.initialize();
  }
  initialize(){
    this.appform = this.formBuilder.group({
      from: [null],
      to: [null]
    });
  }
  submit() {
    this.appform.controls['from'].setValue(this.selectedDate.from);
    this.appform.controls['to'].setValue(this.selectedDate.to);

    const model =  {
      model : this.appform.value
    }
    this.reportservice.TopicReport(model).subscribe({
      next: (response) => {
        this.reports = response
      }
    })


  }

}
