import { Component, OnInit } from '@angular/core';
import * as moment from 'jalali-moment';
import { ReportService } from 'src/app/core/services/report.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-userreport',
  templateUrl: './userreport.component.html',
  styleUrls: ['./userreport.component.scss']
})
export class UserreportComponent implements OnInit  {
  appform : FormGroup | any
  selectedDate : any = {
    birthFrom: null,
    birthTo: null,
    createFrom: null,
    createTo: null
  }
  reports : any =[]

  constructor(
    private reportservice: ReportService,
    private formBuilder: FormBuilder,

  ) { }

  ngOnInit(): void {
    this.initialize();
  }
  initialize(){
    this.appform = this.formBuilder.group({
      birthFrom: [null],
      birthTo: [null],
      createFrom: [null],
      createTo: [null]
    });
  }

  submit() {
    this.appform.controls['birthFrom'].setValue(this.selectedDate.birthFrom);
    this.appform.controls['birthTo'].setValue(this.selectedDate.birthTo);
    this.appform.controls['createFrom'].setValue(this.selectedDate.createFrom);
    this.appform.controls['createTo'].setValue(this.selectedDate.createTo);
    const model =  {
      model : this.appform.value
    }
    this.reportservice.UserListReport(model).subscribe({
      next: (response) => {
        this.reports = response
      }
    })


  }

}
