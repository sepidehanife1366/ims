import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
  title = ''

  constructor(
    private router : Router,
    private route : ActivatedRoute,

  ) { 
    router.events.subscribe((res)=>{
      if (res instanceof ActivationEnd) {
        if (res.snapshot.data['header'] != undefined) {
          this.title = res.snapshot.data['header']
        }

      }
      
    })

  }

  ngOnInit(): void {
  }
  getUrl(url : any){
    return this.router.url === url
  }

}
