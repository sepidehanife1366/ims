import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SharedModule } from 'src/app/sharedComponents/shared.module';
import { UserreportComponent } from './userreport/userreport.component';
import { TopicreportComponent } from './topicreport/topicreport.component';



const routes: Routes = [
        {
          path: 'users',
          component: UserreportComponent ,
          data : {
            title : 'صفحه اصلی/ گزارش کاربران',
            header : 'گزارش کاربران'
          }
        },
        {
          path: 'topics',
          component: TopicreportComponent ,
          data : {
            title : 'صفحه اصلی/ گزارش موضوعات',
            header : 'گزارش موضوعات'

          }
        }

];

@NgModule({
  declarations: [
    UserreportComponent,
    TopicreportComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule.forRoot(),
    AccordionModule.forRoot(),

  ]
})
export class ReportModule { }