import { AfterViewChecked, AfterViewInit, Component, ElementRef, HostListener, OnChanges, OnInit, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { FormService } from 'src/app/core/services/form.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { PackageService } from 'src/app/core/services/package.service';
import { PostService } from 'src/app/core/services/post.service';
import { SubjectService } from 'src/app/core/services/subject.service';
import { TopicService } from 'src/app/core/services/topic.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit , AfterViewInit , OnChanges , AfterViewChecked{
  @ViewChild ('scroller') scroller : HTMLElement | any
  appform: FormGroup | any;
  roles = {
    superAdmin: 0,
    user: 0
  };
  packages: any;
  postslist: any = [];
  displayCreatetopic = false;
  packageItem = {
    has: null,
    required: null,
    forms: null,
    lightTopics: null,
    subject: null,
    topicTree: null,
    editSubject: false
  };
  topics: any;
  mentions = [];
  packageIndex = 0
  imageUrl: any = null;
  selectedImage: any = ''
  forms: any = []
  currentPackageId: any
  subject: any
  isvisible = false;

  currentPage=1;
  itemsPerPage=5;
  totalCount = 0


  constructor(
    private packageservice: PackageService,
    private postservice: PostService,
    private authservice: AuthenticationService,
    private router: Router,
    private topicservice: TopicService,
    private notificationservice: NotificationService,
    private activatedroute: ActivatedRoute,
    private formbuild: FormBuilder,
    private domSanitizer: DomSanitizer,
    private upload: CloudStorageService,
    private formservice: FormService,
    private activatedRoute: ActivatedRoute,
    private subjectservice: SubjectService,
    private toast : ToastrService

  ) {

    activatedRoute.queryParams.subscribe(
      params => {
        if (params['id'] != undefined && params['action'] != undefined) {
          this.loadSubject(params['id'])
          this.displayCreatetopic = true
          this.isvisible = false
          this.packageItem.editSubject = true
        }
        else {
          this.packageItem.editSubject = false
          this.isvisible = true
          this.packageItem.subject = null;
          this.packageItem.topicTree = null;
          this.displayCreatetopic = false
          this.loadPostsData({
            page: this.currentPage,
            pageCount: this.itemsPerPage
          });
          this.getmentionpost();
          this.getPackageTree();
        }
      });
  }
  @HostListener('window:scroll', ['$event']) 
  scrollToTopBtn(event : Event) {    
    if (document.documentElement.scrollTop >= 300) {
      this.scroller.nativeElement.classList.add('d-block')
      this.scroller.nativeElement.classList.remove('d-none')
    }
    else{
      this.scroller.nativeElement.classList.add('d-none')
      this.scroller.nativeElement.classList.remove('d-block')
    }
  }
  ngAfterViewChecked(): void {
  }
  ngOnChanges(changes: SimpleChanges): void {

  }
  ngAfterViewInit(): void {    
  }

  ngOnInit(): void {    
    this.initialize();
    this.checkValidToken();

  }
  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      creatorId: [''],
      title: [''],
      postContent: ['', Validators.required],
      fileItem: [],
      private: [false],
      userId: ''
    })
  }
  getPackageTree() {
    this.packageservice.GetPackageTree().subscribe({
      next: (response: any) => {
        this.packages = {
          tree: response,
          topicMode: false
        };
      },
      error: (err) => {
        console.log('err');
      }
    })
  }
  submit() {
    if (!this.appform.valid) {
      return
    }
    const model = {
      input: this.appform.value
    }
    this.postservice.CreateOrUpdatepost(model).subscribe({
      next: (response) => {
        this.loadPostsData({
          page: this.currentPage,
          pageCount: this.itemsPerPage
        });
        this.initialize();
        this.selectedImage = '';
      }
    })
  }
  checkValidToken() {
    this.authservice.IsValidToken().subscribe({
      next: (response) => {
        if (response.isValid) {
          return true
        }
        else {
          return this.router.navigate(['/auth']);
        }
      }
    })
  }

  getRole() {
    const roles = JSON.parse(localStorage.getItem('userConfig') as any).userType;
    if (roles[0] === 'super_admin') {
      this.roles.superAdmin = 1;
      this.roles.user = 0;
    }
    else {
      this.roles.superAdmin = 0;
      this.roles.user = 1;
    }
  }
  uploadimage() {
    const element = document.getElementById('_input') as any;
    element.click();
  }
  getimageValue(e: any) {
    const file = e.target.files[0];
    this.uploadFile(file)
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageUrl = this.domSanitizer.bypassSecurityTrustUrl('data:image/png;base64' + reader.result);
    };
  }
  uploadFile(file: any) {
    const formData = new FormData();
    formData.append('file', file);
    this.upload.UploadFile(formData).subscribe(response => {
      this.selectedImage = {
        type : response.fileType, 
        name : file.name    
      }
      const model =
      {
        title: "",
        fileName: response.cloudFileName.split('.')[0],
        fileType: '.' + response.cloudFileName.split('.')[1]
      }

      this.appform.controls['fileItem'].setValue(model)
    })
  }

  loadPostsData = (model: any) => {
    const postId = this.activatedroute.snapshot.queryParamMap.get('pid');
    const userId = this.activatedroute.snapshot.queryParamMap.get('uid');
    this.postservice.GetAllPostes(model).subscribe({
      next: (response) => {
        this.postslist = response.items;
        this.totalCount = response.totalCount
        this.mentions = response;
        this.postslist.forEach((element: any, i: number) => {
          element.comments = [];
          this.getInteractions(element.id, i)
        });
        if (postId && userId) {
          this.loadmentionpost(postId, userId)
        }
      },
      error: err => console.log(err)
    })
  }


  appendData= ()=>{
    this.postservice.GetAllPostes({
      page: this.currentPage,
      pageCount: this.itemsPerPage
    }).subscribe({
     next:response=>this.postslist = [...this.postslist,...response.items],
     error:err=>console.log(err),
    })
  }
  onScroll= ()=>{
    this.currentPage++;
    const max = Math.round(this.totalCount / this.currentPage)
    if (this.currentPage > Math.round(this.totalCount / this.currentPage) +1 ) {
      return
    }
    this.appendData();
   }

  getInteractions(postId: any, index: number) {
    this.postservice.GetEntityIntractions(postId).subscribe({
      next: (response) => {
        const userId = JSON.parse(localStorage.getItem('userConfig') as any).userId;
        let plus = response.filter((e: any) => e.intractionType === 0)
        let minus = response.filter((e: any) => e.intractionType === 1)
        this.postslist[index].interactions = {
          plus: { action: false, plus: plus },
          minus: { action: false, minus: minus }
        }
        response.forEach((element: any) => {
          if (element.userId === userId && element.intractionType === 0) {
            this.postslist[index].interactions.plus.action = true
          }
          if (element.userId === userId && element.intractionType === 1) {
            this.postslist[index].interactions.minus.action = true
          }

        });

      }
    })
  }

  GETITEM(e: any) {
    if (!e.item.topicMode) {
      this.displayCreatetopic = true;
      this.currentPackageId = e.item.id
      this.getpackage(e.item.id);
      this.geteveluatedsetting(e.item.id);
      this.topicservice.initial.next(true);
    }
  }

  displayPosts() {
    this.displayCreatetopic = false;
    this.router.navigate(['/panel'])
  }

  getmentionpost() {
    this.notificationservice.getnotification.subscribe((result: any) => {
      if (result != null) {
        this.loadmentionpost(result.recordId, result.userId)
      }
      else {
        const postId = this.activatedroute.snapshot.queryParamMap.get('pid');
        const userId = this.activatedroute.snapshot.queryParamMap.get('uid');
        if (postId && userId) {
          this.loadmentionpost(postId, userId)
        }
      }
    })
  }
  loadmentionpost(postId: any, userId: any) {
    this.postslist = this.mentions.filter((m: any) => m.id === +postId && m.creatorUserId === +userId);

  }
  RELOADPOSTS(e: any) {
    this.loadPostsData({
      page: this.currentPage,
      pageCount: this.itemsPerPage
    });
    return;
  }

  getpackage(id: any) {
    this.packageservice.GetPackage(id).subscribe({
      next: (response) => {
        if (response.formId != null) {
          this.loadForms(response.formId)
        }
        this.packageItem.has = response;
      }
    })
  }

  geteveluatedsetting(id: any) {
    this.packageservice.GetEvaluationSetting(id).subscribe({
      next: (response) => {
        this.packageItem.required = response
        if (response.relatedTopicDecision === 0 || response.relatedTopicDecision === 1 || response.relatedTopicDecision === null) {
          this.loadlighttopic(id)
        }
        else {
          this.loadTopics(id)
        }
      }
    })

  }
  loadlighttopic(id: any) {
    this.topicservice.GetLightTopic(id).subscribe({
      next: (response) => {
        this.packageItem.lightTopics = response
      }
    })

  }
  loadTopics(packageId: number) {
    const model = {
      packageId: packageId
    }
    this.topicservice.GetTopicTree(model).subscribe(response => {
      this.topics = {
        tree: response,
        topicMode: true
      };
      this.packageItem.topicTree = this.topics
    })
  }

  loadForms(id: any) {
    let content = []
    this.formservice.GetFormById(id).subscribe({
      next: (response) => {
        content = JSON.parse(response[0].content.replace(/'/g, '"'))
        this.packageItem.forms = content
      }
    })
  }
  loadSubject(id: any) {
    this.subjectservice.GetSubject(id).subscribe({
      next: (response) => {
        this.subject = response
        this.packageItem.subject = response
        if (response.form != null) {
          this.packageItem.forms = JSON.parse(response.form);
          this.currentPackageId = response.packageId
          this.getpackage(response.packageId);
          this.geteveluatedsetting(response.packageId);
        }
      }
    })
  }

  scrollTop(){
    window.scrollTo({ top: 100, left: 100, behavior: 'smooth' })
  }


}
