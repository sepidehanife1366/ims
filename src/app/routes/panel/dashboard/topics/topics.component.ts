import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { PackageService } from 'src/app/core/services/package.service';
import { TopicService } from 'src/app/core/services/topic.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Observable, map } from 'rxjs';
import { StepperOrientation } from '@angular/cdk/stepper';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { SubjectService } from 'src/app/core/services/subject.service';
import { MatStepper } from '@angular/material/stepper';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.scss']
})
export class TopicsComponent implements OnInit, OnChanges {
  @Input() packageItem: any;
  firstFormGroup: FormGroup | any;
  secondFormGroup: FormGroup | any;
  thirdFormGroup: FormGroup | any;
  forthFormGroup: FormGroup | any;
  @ViewChild('editor') editor: ElementRef<any> | any
  @ViewChild('stepper') stepper !: MatStepper;
  @ViewChildren('form') formComponent!: QueryList<any>;

  editorData = ''
  model = {
    editorData: ''
  }
  public Editor = ClassicEditor;
  uploadImages: any = [];
  package: any
  @Input() topics: any
  @Output() GETACCORDIONITEM_PACKAGE = new EventEmitter<any>();
  selectedtopics: any = [];
  selectedImage: any = [];
  forms: any = []
  stepperOrientation: Observable<StepperOrientation>;
  topicIds: any;
  @Input() currentPackageId: any;
  Configuration: any = [];
  disabled = false;
  submitted_1 = false
  submitted_2 = false
  requiredforms: any = [];
  isExpert = false;
  modalRef?: BsModalRef;
  questions: any[] = [];
  subjectAnswerModel: any = {
    subjectId: 0,
    answers: []
  }

  constructor(
    private formbuild: FormBuilder,
    private upload: CloudStorageService,
    private domSanitizer: DomSanitizer,
    private packageservice: PackageService,
    private topicservice: TopicService,
    private breakpointObserver: BreakpointObserver,
    private subjectservice: SubjectService,
    private toastservice: ToastrService,
    private router: Router,
    private userservice: UserService,
    private authentication: AuthenticationService,
    private modalservice: BsModalService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 992px)')
      .pipe(map(({ matches }) => (matches ? 'horizontal' : 'vertical')));
  }
  ngOnChanges(changes: SimpleChanges): void {

  }

  ngOnInit(): void {
    this.initialize();
    this.topicservice.initial.subscribe(result => {
      if (result) {
        this.initialize();
        if (this.packageItem.subject != null) {
          this.subjectsetValue(this.packageItem.subject)
        }
        if (this.packageItem.forms != null) {
          const titleformValid = this.packageItem.forms.some((s: any) => s.type === 'TitleField');
          if (titleformValid) {
            const s = this.packageItem.forms.find((s: any) => s.type === 'TitleField');
            s.isvalid = true
          }
        }
      }
    })
    setTimeout(() => {
      if (this.packageItem.forms != null) {
        this.Configuration = this.packageItem.forms
        const titleformValid = this.packageItem.forms.some((s: any) => s.type === 'TitleField');
        if (titleformValid) {
          const s = this.packageItem.forms.find((s: any) => s.type === 'TitleField');
          s.isvalid = true
        }
      }
      if (this.packageItem.subject != null) {
        this.subjectsetValue(this.packageItem.subject)
      }
    }, 1000);

  }

  validateClass(controlname: any, form: any) {
    if (form?.controls[controlname].errors != null && (this.submitted_1 && this.submitted_2)) {
      return true
    }
    return false;

  }
  getPackageTree() {
    this.packageservice.GetPackageTree().subscribe({
      next: (response: any) => {
        this.package = {
          tree: response,
          topicMode: false
        };
      },
      error: (err) => {

      }
    })
  }

  loadTopics(packageId: number) {
    const model = {
      packageId: packageId
    }
    this.topicservice.GetTopicTree(model).subscribe(response => {
      this.topics = response
      this.topics.forEach((element: any) => {
        element.isOpen = false;
        element.topicMode = true
      });
    })
  }

  initialize() {
    this.firstFormGroup = this.formbuild.group({
      creatorId: [''],
      id: [null],
      packageId: [null],
      title: ['', Validators.required],
      keyWord: [''],
      abstract: [''],
      imgtitle: [''],
      fileItems: [''],
    })
    this.secondFormGroup = this.formbuild.group({
      selectedtopic: ['', Validators.required],
    })
    this.thirdFormGroup = this.formbuild.group({
      form: ['', Validators.required],
    })
    this.forthFormGroup = this.formbuild.group({
      encyclopedia: ['', Validators.required],
    })
    setTimeout(() => {
      this.firstFormGroup.controls['abstract'].setErrors(this.packageItem?.required?.requieredSummary ? { 'required': true } : null);
      this.firstFormGroup.controls['imgtitle'].setErrors(this.packageItem?.required?.requieredAttachment ? { 'required': true } : null);
      this.firstFormGroup.controls['fileItems'].setErrors(this.packageItem?.required?.requieredAttachment ? { 'required': true } : null);
      this.firstFormGroup.controls['keyWord'].setErrors(this.packageItem?.required?.requieredKeyWord ? { 'required': true } : null);
      const creatorId = JSON.parse(localStorage.getItem('userConfig') as any).userId
      this.firstFormGroup.controls['creatorId'].setValue(creatorId);
    }, 500);

    if (this.stepper?.selectedIndex != undefined) {
      this.stepper.selectedIndex = 0;
      this.stepper.reset();
    }
    this.selectedtopics = [];
    this.Configuration = [];
    this.submitted_1 = false
    this.submitted_2 = false;
    this.uploadImages = []
  }

  submit() {
    const model = {
      input: {
        creatorId: this.firstFormGroup.controls['creatorId'].value,
        id: this.firstFormGroup.controls['id'].value,
        packageId: this.currentPackageId,
        title: this.firstFormGroup.controls['title'].value,
        keyWord: this.firstFormGroup.controls['keyWord'].value,
        abstract: this.firstFormGroup.controls['abstract'].value,
        subjectContent: "",
        encyclopedia: this.forthFormGroup.controls['encyclopedia'].value,
        form: this.thirdFormGroup.controls['form'].value,
        topicIds: this.topicIds,
        fileItems: this.selectedImage
      }
    }

    this.subjectservice.CreateOrUpdateSubject(model).subscribe({
      next: (response) => {
        if (response != undefined) {
          this.firstFormGroup.controls['id'].setValue(response);
          this.toastservice.success('ذخیره شد');
          this.disabled = true;
        }
      }
    })
  }

  uploadimage() {
    const element = document.getElementById('_input') as any;
    element.click();
  }

  getimageValue(e: any) {
    const file = e.target.files[0];
    this.uploadFile(file)
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.domSanitizer.bypassSecurityTrustUrl('data:image/png;base64' + reader.result)
      this.uploadImages.push({
        type: file.type,
        name: file.name,
        url: this.domSanitizer.bypassSecurityTrustUrl('data:image/png;base64' + reader.result),
        title: this.firstFormGroup.controls.imgtitle.value
      })
    };
  }

  uploadFile(file: any) {
    const formData = new FormData();
    formData.append('file', file);
    this.upload.UploadFile(formData).subscribe(response => {
      const model =
      {
        title: "",
        fileName: response.cloudFileName.split('.')[0],
        fileType: '.' + response.cloudFileName.split('.')[1]
      }
      this.selectedImage.push(model)
      this.firstFormGroup.controls['fileItems'].setValue(this.selectedImage)
    })
  }

  GETITEM(e: any) {
    if (!e.topicMode) {
      this.initialize();
    }
    else {
      this.GETCHECKBOXITEM(e)
    }
  }

  GETCHECKBOXITEM(e: any) {
    if (e.checked) {
      this.selectedtopics.push(e.item)
    }
    else {
      this.selectedtopics = this.selectedtopics.filter(function (item: any) {
        return item !== e.item
      })
    }
    this.topicIds = this.selectedtopics.map((s: any) => s.id);
    if (this.topicIds.length > 0) {
      this.secondFormGroup.controls['selectedtopic'].setValue(this.topicIds);
    }
    else {
      this.secondFormGroup.controls['selectedtopic'].setValue(null);

    }

  }

  editonChange(e: any) {
    this.forthFormGroup.controls['encyclopedia'].setValue(this.model.editorData)
  }

  configuration(e: any) {
    this.Configuration = this.Configuration.filter(function (item: any) {
      return item.index !== e.index
    })

    this.Configuration.push(e);

    const valid = this.Configuration.some((s: any) => s['configuration'].required && (s.isvalid === false || s.isvalid === null));
    if (!valid) {

      this.thirdFormGroup.controls['form'].setValue(JSON.stringify(this.Configuration));

    }
    else {
      this.thirdFormGroup.controls['form'].setValue(null);
    }
  }

  getlightTopicvalue(e: any, item: any) {
    const model = {
      checked: e.checked,
      item: item
    }
    this.GETCHECKBOXITEM(model)
  }

  subjectsetValue(data: any) {
    this.firstFormGroup.controls['id'].setValue(data.id);
    this.firstFormGroup.controls['title'].setValue(data.title);
    this.firstFormGroup.controls['keyWord'].setValue(data.keyWord);
    this.firstFormGroup.controls['abstract'].setValue(data.abstract);
    this.firstFormGroup.controls['imgtitle'].setValue(data.imgtitle);
    this.firstFormGroup.controls['fileItems'].setValue(data.fileItems);
    this.firstFormGroup.controls['packageId'].setValue(data.packageId);
    const forms = JSON.parse(data.form);
    this.packageItem.forms = forms
    this.thirdFormGroup.controls['form'].setValue(JSON.stringify(forms));
    this.model.editorData = data.encyclopedia
    this.forthFormGroup.controls['encyclopedia'].setValue(data.encyclopedia)
    setTimeout(() => {
      if (this.packageItem.topicTree != null) {
        this.packageItem.topicTree.tree.forEach((element: any) => {
          data.topics.forEach((el: any) => {
            const find = this.searchTree(element, el.id);
            if (find != undefined) {
              find.checked = true
              const model = {
                checked: find.checked,
                item: find
              }
              this.GETCHECKBOXITEM(model)
            }
          });

        });
      }
      else if (this.packageItem.lightTopics != null) {
        data.topics.forEach((element: any) => {
          const find = this.packageItem.lightTopics.find((s: any) => s.id === element.id);
          if (find != undefined) {
            find.checked = true
            const model = {
              checked: find.checked,
              item: find
            }
            this.GETCHECKBOXITEM(model)
          }
        })
      }
    }, 1000);
  }

  searchTree(element: any, id: any): any {
    if (element.id == id) {
      return element;
    } else if (element.children != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < element.children.length; i++) {
        result = this.searchTree(element.children[i], id);
      }
      return result;
    }
    return null;
  }

  submitStatus(mode: any, template: any) {
    const model = {
      subjectId: this.firstFormGroup.controls['id'].value,
      actionType: mode
    }
    const approverRole = this.activatedRoute.snapshot.queryParamMap.get('approverRole');
    if (Number(approverRole) === 2 && mode === 1) {
      this.modalRef = this.modalservice.show(template, { backdrop: 'static', keyboard: false })
      setTimeout(() => {
        this.loadEvaluationQuestions(model.subjectId)
      }, 200);
    }
    else {
      this._UpdateSubjectStatus(model)
    }
  }

  loadEvaluationQuestions(subjectId: any) {
    this.subjectservice.GetSubjectEvaluationQuestions(subjectId).subscribe({
      next: (response) => {
        this.questions = response;
        this.questions.forEach(q => {
          q.subjectId = subjectId;
          q.score = 0
        })
      }
    })
  }

  _UpdateSubjectStatus(model: any) {
    this.subjectservice.UpdateSubjectStatus(model).subscribe({
      next: (response) => {
        if (response.resultCode === 0) {
          this.initialize();
          this.router.navigate(['/panel']);
          this.userservice.reload.next(true);
          this.topicservice.initial.next(true);
        }
      }
    })
  }

  next(e: any, mode: any) {
    this.submitted_2 = false
    this.submitted_1 = false
    if (mode === 0) {
      this.submitted_1 = true
    }
    else {
      this.submitted_2 = true
    }

  }

  reset(form: any, mode: any) {
    if (mode === 0) {
      this.selectedtopics.forEach((el: any) => {
        el.checked = false
      })
    }
    else {
      this.model.editorData = ''
    }
    form.reset();
  }

  submitForm() {
    const form = this.formComponent.toArray();
    form.forEach(el => {
      if (el.item['configuration'].required && (el.item.isvalid === false || el.item.isvalid === null)) {
        el.valid = false;
        el.item.isvalid = false
      }

    })

  }

  _getquestionvalue(e: any, item: any) {
    item.score = +e.value
  }

  questionSubmit() {
    const promis = new Promise<void>((resolve, reject) => {
      this.questions.forEach((el, i, array) => {
        this.subjectAnswerModel.subjectId = el.subjectId;
        this.subjectAnswerModel.answers.push({
          evaluationQuestionId: el.evaluationSettingId,
          score: el.score
        })
        if (i === array.length - 1) { setTimeout(() => { resolve(); }, 100); }
      })
    })
    promis.then(result => {
      this._answerquestions(this.subjectAnswerModel)
    })
  }

  _answerquestions(model: any) {
    this.subjectservice.SubmitSubjectAnswers(model).subscribe({
      next: (response) => {
        const model = {
          subjectId: this.firstFormGroup.controls['id'].value,
          actionType: 1
        }
        this._UpdateSubjectStatus(model)
        this.modalRef?.hide();
      }
    })
  }
}
