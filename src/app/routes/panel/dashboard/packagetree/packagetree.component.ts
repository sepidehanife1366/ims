import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-packagetree',
  templateUrl: './packagetree.component.html',
  styleUrls: ['./packagetree.component.scss']
})
export class PackagetreeComponent implements OnInit , OnChanges {
  @Input() title =''
  @Input() treepackage: any;
  @Input() ralatedtopicsMode : boolean = false;

  @Output() _GET_ACCORDION_ITEM = new EventEmitter<any>()
  @Output() GETTOPICS = new EventEmitter<any>()

  selectedTopics : any = []

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {    

  }

  ngOnInit(): void {
    
  }

  GET_ACCORDION_ITEM(item : any){    
    this._GET_ACCORDION_ITEM.emit({topicmode : this.ralatedtopicsMode , item : item})
  }

}
