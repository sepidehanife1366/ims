import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, Renderer2, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as FileSaver from 'file-saver';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { PostService } from 'src/app/core/services/post.service';


@Component({
  selector: 'app-postcard',
  templateUrl: './postcard.component.html',
  styleUrls: ['./postcard.component.scss']
})
export class PostcardComponent implements OnInit , OnChanges , AfterViewInit {
  appform : FormGroup | any;
  @Input() postslist : any;
  comments : any = [] ;
  displaycomments = false;
  postIndex =-1;
  @Output() RELOADPOSTS = new EventEmitter<any>()

  constructor(
    private postservice : PostService ,
    private formbuild : FormBuilder,
    private cloudservice : CloudStorageService
  ) { }
  ngAfterViewInit(): void {
 
    }
  ngOnChanges(changes: SimpleChanges): void {

  }

  ngOnInit(): void {
    this.initialize();
    this.postslist.forEach((post : any) => {
      post.isOpen = false;
    });
  }

  loadImage(path : any , post : any){
    const model ={
      imageQuality: 0,
      filePath: path
    }
    this.cloudservice.GetImageBase64(model).subscribe({
      next : (response)=>{
       post.picture = 'data:image/png;base64,' + response;

      }
    })
  }
  initialize(){
    this.appform = this.formbuild.group({
      comment : [null],
    })
  }

  toggleComments(item : any , index : number){
    item.isOpen = !item.isOpen
    if (item.isOpen) {this.getComments(item.id,index)}
    else {this.postslist[index].comments = []}; 
  }

  getComments(postId : any , postItemindex : number){
    const model = {
        page: 0,
        pageCount: 0,
        postId: postId
    }
    this.postservice.GetPostComments(model).subscribe(response =>{
      this.comments = response ,
      this.postslist[postItemindex].comments = this.comments;
    })
  }
  remove(comment : any , postItemindex : any){
    const model = {
      id : comment.id
    }
   this.postservice.DeleteComment(model).subscribe(response =>{
      this.getComments(comment.postId , postItemindex);
   })
  }

  interactionPost(item : any , mode : number , action : boolean){
    const userId = JSON.parse(localStorage.getItem('userConfig') as any).userId;
    if (!action) {
      const createmodel = {
        entity: {
          userId: userId,
          recordId: item.id.toString(),
          intractionType: mode,
          entityName: mode === 0 ? 0 : 1
        }
      }
      this.postservice.CreateOrUpdateEntityIntraction(createmodel).subscribe({
        next : (response) =>{
          this.RELOADPOSTS.emit(true);
        }
      })
    }
    else{
      const removemodel = {
          userId: userId,
          recordId: item.id.toString(),
      }
      this.postservice.DeleteEntityIntraction(removemodel).subscribe({
        next : (response) =>{
          this.RELOADPOSTS.emit(true); 
                   
        }
      })
    }
  }
  submit(id : any , index : number){
    const userId = JSON.parse(localStorage.getItem('userConfig') as any).userId
    const model =
    {
      entity: {
        id: null,
        userId: userId,
        postId: id,
        comment: this.appform.controls['comment'].value,
        parentId: ''
      }
    }    
    this.postservice.CreateOrUpdateComment(model).subscribe({
      next : (response) =>{
        this.initialize();
        this.getComments(id , index);
      }
    })
  }

  _download(item : any) {
    this._getFile(item);
  }

  _getFile(item : any){
    this.cloudservice.GetFile(item.fileName).subscribe(res=>{
      const blob = new Blob([res]);
       FileSaver.saveAs(blob ,item.fileName + '.'+ item.fileExtention);
    })
  }

}
