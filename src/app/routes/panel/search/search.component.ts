import { Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, ActivationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  title =''
  constructor(
    private route : ActivatedRoute,
     private router : Router
  ) { 
    router.events.subscribe((res)=>{
      if (res instanceof ActivationEnd) {
        if (res.snapshot.data['title'] != undefined) {
          this.title = res.snapshot.data['title']
        }

      }
      
    })
  }
  ngOnInit(): void {
  }

}
