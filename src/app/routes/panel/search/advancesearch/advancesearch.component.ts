import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PackageService } from 'src/app/core/services/package.service';
import { SearchService } from 'src/app/core/services/search.service';
import { TopicService } from 'src/app/core/services/topic.service';

@Component({
  selector: 'app-advancesearch',
  templateUrl: './advancesearch.component.html',
  styleUrls: ['./advancesearch.component.scss']
})
export class AdvancesearchComponent implements OnInit {
  title = ''
  packages : any;
  topics : any
  panelOpenState = false;
  appform : any;
  selectedTopics : any =[]
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-lg'
  };
  modalRef?: BsModalRef;
  startdate : any
  enddate : any
  searches : any =[]
  pages : any =[];
  currentPage =0

  constructor(
    private packageservice : PackageService,
    private route : ActivatedRoute,
    private router : Router,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private topicservice : TopicService,
    private searchservice : SearchService

  ) {
    this.title = route.snapshot.data['title']
   }

  ngOnInit(): void {
    this.getPackageTree();
    this.initialize();
  }
  initialize(){
    this.appform = this.formBuilder.group({
        query: "",
        onlyMySubjects : false
    });
  }

  getPackageTree() {
    this.packageservice.GetPackageTree().subscribe({
      next: (response: any) => {
        this.packages = {
          tree : response ,
          topicMode : false
        };        
      },
      error: (err) => {

      }
    })
  }
  GETITEM(e : any , mode : any){
    if (mode === 0) {
      this.loadTopics(e.item.id)
    }
    else{
      if (e.checked) {
        this.selectedTopics.push(e.item.id)
      }
      else{
        this.selectedTopics = this.selectedTopics.filter(function(item: any) {
          return item !== e.item.id
      })
      }
    }

  }
  DATE_VALUE(e : any , mode : any){
    if (mode === 0) {
      this.startdate = e
    }
    else{
      this.enddate = e
    }    
  }
  getTopic(template : any){
    this.modalRef = this.modalService.show(template , this.config)

  }
  loadTopics(packageId: any) {
    const model = {
      packageId: packageId
    }
    this.topicservice.GetTopicTree(model).subscribe({
      next: (response) => {
        this.topics = {
          tree: response,
          topicMode: true
        };
      }
    })
  }
  submit(){
    this.loadsearchItems(0)

  }
  loadsearchItems(page : number){
    const model ={
      entity: {
        startDate: this.startdate,
        endDate: this.enddate,
        query: this.appform.controls.query.value,
        relatedTopics: this.selectedTopics,
        onlyMySubjects: this.appform.controls.onlyMySubjects.value
      }
    }

    this.searchservice.AdvanceSearch(model).subscribe({
      next : (response)=>{
        this.searches= response.map((s : any)=> s.items); 
        this.pages = response.map((s : any)=> s.totalCount).reduce((a : any, b : any) => a + b, 0);
        this.searches = [... new Set(this.searches)]
        this.searches = this.searches[page]
        
      }
    })
  }
  navigateTo(item : any){
    if (item.type === 0) {
      this.router.navigate(['/profile'] , { queryParams : {id : item.id }})
      
    }
    else{
      this.router.navigate(['/subject'] , { queryParams : {id : item.id }})

    }
  }
  pageChanged(e : any){
    this.loadsearchItems(e.page);
  }



}
