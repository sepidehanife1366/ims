import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchService } from 'src/app/core/services/search.service';

@Component({
  selector: 'app-membersearch',
  templateUrl: './membersearch.component.html',
  styleUrls: ['./membersearch.component.scss']
})
export class MembersearchComponent implements OnInit {

  title = ''
  appform : any;
  searches : any =[]
  currentPage = 0;
   pages : any =[]
  
    constructor(
      private route : ActivatedRoute,
       private router : Router,
       private formBuilder: FormBuilder,
       private searchservice : SearchService
    ) { 
      this.title = route.snapshot.data['title']
      
    }
    ngOnInit(): void {
      this.initialize();
    }
  
  
    initialize(){
      this.appform = this.formBuilder.group({
        member: false,
        expert: false,
        registarer: false,
        resume: false,
        city: false,
        job: false,
        query: ''
      });
    }
    submit(){
      this.loadsearchItems(0)
    }
    loadsearchItems(page : number){
      const model = {
        entity: this.appform.value
      }
      this.searchservice.MemberSearch(model).subscribe({
        next : (response)=>{
          this.searches= response.map((s : any)=> s.items); 
          this.pages = response.map((s : any)=> s.totalCount).reduce((a : any, b : any) => a + b, 0);
          this.searches = [... new Set(this.searches)]
          this.searches = this.searches[page]       
        }
      })
    }
  
    navigateTo(item : any){
      if (item.type === 0) {
        this.router.navigate(['/profile'] , { queryParams : {id : item.id }})
        
      }
      else{
        this.router.navigate(['/subject'] , { queryParams : {id : item.id }})
  
      }
    }
    pageChanged(e : any){
      this.loadsearchItems(e.page);
    }
  

}
