import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SharedModule } from 'src/app/sharedComponents/shared.module';
import { GeneralsearchComponent } from './generalsearch/generalsearch.component';
import { AdvancesearchComponent } from './advancesearch/advancesearch.component';
import { MembersearchComponent } from './membersearch/membersearch.component';



const routes: Routes = [
        {
          path: '',
          component: GeneralsearchComponent ,
          data: {
            title: 'جستجوی عمومی',
          }
        }
        ,
        {
          path: 'advance',
          component: AdvancesearchComponent ,
          data: {
            title: 'جستجوی پیشرفته',
          }
        }
        ,
        {
          path: 'member',
          component: MembersearchComponent ,
          data: {
            title: 'جستجوی اعضا'
          }
        }

];

@NgModule({
  declarations: [
    GeneralsearchComponent,
    AdvancesearchComponent,
    MembersearchComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule.forRoot(),
    RouterModule.forChild(routes),
    AccordionModule.forRoot(),

  ]
})
export class SearchModule { }