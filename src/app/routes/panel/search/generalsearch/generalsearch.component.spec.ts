import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralsearchComponent } from './generalsearch.component';

describe('GeneralsearchComponent', () => {
  let component: GeneralsearchComponent;
  let fixture: ComponentFixture<GeneralsearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralsearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
