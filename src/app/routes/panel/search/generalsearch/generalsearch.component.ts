import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchService } from 'src/app/core/services/search.service';

@Component({
  selector: 'app-generalsearch',
  templateUrl: './generalsearch.component.html',
  styleUrls: ['./generalsearch.component.scss']
})
export class GeneralsearchComponent implements OnInit {
title = ''
appform : any;
searches : any =[] 
currentPage = 0;
pages : any =[]
  constructor(
    private route : ActivatedRoute,
     private router : Router,
     private formBuilder: FormBuilder,
     private searchservice : SearchService
  ) { 
    this.title = route.snapshot.data['title']
    
  }
  ngOnInit(): void {
    this.initialize();
    this.BehaviorFn();
  }

  BehaviorFn(){
    let  search = JSON.parse(sessionStorage.getItem('%%search%') as any) ;
    if (search != undefined) {
      this.appform.controls['query'].setValue(search.query)
      this.appform.controls['subject'].setValue(search.subject)
      this.appform.controls['user'].setValue(search.user)
      this.appform.controls['keyWord'].setValue(search.keyWord)
      this.loadsearchItems(0 , search)

    }
  }


  initialize(){
    this.appform = this.formBuilder.group({
        query: "",
        subject: false,
        user: false,
        keyWord: false
    });
  }
  submit(){
   this.loadsearchItems(0 , this.appform.value)
  }
  loadsearchItems(page : number , data : any){
    sessionStorage.setItem('%%search%' , JSON.stringify(this.appform.value));
    const model = {
      entity: data
    }
    this.searchservice.GeneralSearch(model).subscribe({
      next : (response)=>{
        this.searches= response.map((s : any)=> s.items); 
        this.pages = response.map((s : any)=> s.totalCount).reduce((a : any, b : any) => a + b, 0);
        this.searches = [...new Set(this.searches)]
        this.searches = this.searches[page]
      }
    })
  }

  navigateTo(item : any){
    if (item.type === 0) {
      this.router.navigate(['/profile'] , { queryParams : {id : item.id }})
      
    }
    else{
      this.router.navigate(['/subject'] , { queryParams : {id : item.id }})

    }
  }
  pageChanged(e : any){
    this.loadsearchItems(e.page , this.appform.value);
  }

}
