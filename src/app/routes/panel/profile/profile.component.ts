import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxPermissionsService } from 'ngx-permissions';
import { ProfileModel } from 'src/app/core/models/profile.model';
import { ProfileService } from 'src/app/core/services/profile.service';
import { SubjectService } from 'src/app/core/services/subject.service';
import { UserService } from 'src/app/core/services/user.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  url = environment.httpConfig.imageUrl

  profileInfo: ProfileModel | any
  colleagues: any = [];
  colleaguemode = false
  isEdit = false;
  modalRef?: BsModalRef;
  subjects: any[] = [];
  modal = {
    title: ''
  }
  constructor(
    private userservice: UserService,
    private profileservice: ProfileService,
    private router: Router,
    private route: ActivatedRoute,
    private subjectservice: SubjectService,
    private modalservice: BsModalService,
    private permissionsService: NgxPermissionsService

  ) {
    route.queryParams.subscribe(
      params => {
        if (params['id'] != undefined) {
          this.getuserProfile(this.route.snapshot.queryParams["id"]);
          this.isEdit = true
        }
        else {
          this.getCurrentuser();
          this.colleaguemode = false;
          this.isEdit = false
        }
      })
  }

  ngOnInit(): void { }

  async getCurrentuser() {
    this.userservice.GetCurrentUser().subscribe(async response => {
      this.permissionsService.loadPermissions(response.permissions)
      await this.getuserProfile(response.id);
      await this.getColleagues(response.id);
      this.getuserSubject(response.id)
    })
  }

  getuserProfile(userId: number) {
    this.profileservice.GetUserProfileById(userId).subscribe(response => {
      if (response != undefined) {
        this.profileInfo = response;
        setTimeout(() => {
          this.profileInfo.email = response?.email;
          this.profileInfo.username = response?.userName
        }, 500);
      }
    })
  }

  getColleagues(id: number) {
    this.profileservice.GetProfileColleagues(id).subscribe(response => {
      this.colleagues = response
    })
  }
  _colleagueprofileItem(e: any) {
    this.colleaguemode = e.colleague
    this.router.navigate(['profile'], { queryParams: { id: e.item.profileId } })
    this.getuserProfile(e.item.profileId);
    this.getColleagues(e.item.profileId);
  }

  getuserSubject(id: any) {
    this.subjectservice.GetUserSubjects(id).subscribe(response => {
      setTimeout(() => {
        this.profileInfo.usersubject = response?.length;
        this.subjects = response;
      }, 500);
    })
  }
  openmodal(temp: any, name: string) {
    this.modal.title = name
    this.modalRef = this.modalservice.show(temp);
  }
  loadsubjects(id: any) {
    this.subjectservice.GetUserSubjects(id).subscribe({
      next: (response) => {
        this.subjects = response;
      }
    })
  }
  _subjects(item: any) {
    this.router.navigate(['/subject'], { queryParams: { id: item.id } })
    setTimeout(() => {
      this.modalRef?.hide();
    }, 50);
  }

}
