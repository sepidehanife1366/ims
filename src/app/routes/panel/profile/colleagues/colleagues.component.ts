import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-colleagues',
  templateUrl: './colleagues.component.html',
  styleUrls: ['./colleagues.component.scss']
})
export class ColleaguesComponent implements OnInit {

  @Input() colleagues : any;
  @Output() _colleagueprofileItem = new EventEmitter<any>()


  constructor() { }

  ngOnInit(): void {
  }

  getcolleagueprofile(item : any){
    this._colleagueprofileItem.emit({colleague : true , item :item})
  }

}
