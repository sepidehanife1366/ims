import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/core/services/profile.service';
import { UserService } from 'src/app/core/services/user.service';


@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {

  profileInfo : any

  constructor(
    private userservice: UserService,
    private profileservice: ProfileService
  ) { }

  ngOnInit(): void {
    this.profileservice.relaodData.subscribe(result =>{
      if (result) {
        this.getCurrentuser();
      }
      else{
        this.getCurrentuser();
      }
    })

  }

  getCurrentuser() {
    this.userservice.GetCurrentUser().subscribe(response => {
      this.getuserProfile(response.id)
    })
  }

  getuserProfile(userId: number) {
    this.profileservice.GetUserProfileById(userId).subscribe(response => {
      if (response != undefined) {
        this.profileInfo = response
        this.profileservice.profileInfo.next(response)
      }
    })
  }
}
