import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileeducationComponent } from './profileeducation.component';

describe('ProfileeducationComponent', () => {
  let component: ProfileeducationComponent;
  let fixture: ComponentFixture<ProfileeducationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileeducationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileeducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
