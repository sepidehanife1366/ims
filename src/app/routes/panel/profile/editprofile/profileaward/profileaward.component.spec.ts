import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileawardComponent } from './profileaward.component';

describe('ProfileawardComponent', () => {
  let component: ProfileawardComponent;
  let fixture: ComponentFixture<ProfileawardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileawardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileawardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
