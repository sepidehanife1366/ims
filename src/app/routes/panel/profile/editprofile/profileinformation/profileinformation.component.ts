import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'jalali-moment';
import { ToastrService } from 'ngx-toastr';
import { ProfileService } from 'src/app/core/services/profile.service';
import { UserService } from 'src/app/core/services/user.service';


@Component({
  selector: 'app-profileinformation',
  templateUrl: './profileinformation.component.html',
  styleUrls: ['./profileinformation.component.scss']
})
export class ProfileinformationComponent implements OnInit {
  appform : FormGroup | any
  _nationalCode = true;
  imageUrl : any;
  submited = false
  selectedDate: any;

  constructor(
    private formbuild : FormBuilder ,
    private profileservice : ProfileService,
    private domSanitizer: DomSanitizer,
    private userservice: UserService,
    private toastrservice : ToastrService
  ) { }

  ngOnInit(): void {
    this.initialize();
    this.loadprofileInfo();

  }

  loadprofileInfo(){
    this.profileservice.profileInfo.subscribe((response : any) =>{
      if (response !=null) {
        this.appform.controls.id.setValue(response.id);
        this.appform.controls.profileId.setValue(response.profileId);
        this.appform.controls.job.setValue(response.job);
        this.appform.controls.lastname.setValue(response.lastname);
        this.appform.controls.firstname.setValue(response.firstname);
        this.appform.controls.picture.setValue(response.picture);
        this.appform.controls.nationalCode.setValue(response.nationalCode);
        this.appform.controls.about.setValue(response.about);
        this.appform.controls.birthDate.setValue(response.birthDate);
        this.selectedDate = moment(response.birthDate);
      }
  
    })
  }

  
  initialize(){
    this.appform = this.formbuild.group({
      id : [null],
      profileId : [null],
      job : ['' , Validators.required],
      lastname : ['' , Validators.required],
      firstname : ['' , Validators.required],
      picture : [null],
      nationalCode : ['' , Validators.required],
      about : ['' , Validators.required],
      birthDate : ['' , Validators.required]
    })
  }

  uploadimage() {
    const element = document.getElementById('_input') as any;
    element.click();
  }
  getimageValue(e: any) {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {      
     this.imageUrl = this.domSanitizer.bypassSecurityTrustUrl('data:image/png;base64' + reader.result);
     this.appform.controls.picture.setValue(reader.result);
    };
    

  }
  submit(){
    this.submited = true
    this._nationalCode = this.isValidIranianNationalCode(this.appform.controls.nationalCode.value) as any;
    if (!this.appform.valid || !this._nationalCode) {
      return;
    }
    this.profileservice.UpdateProfileInformation({
      id: this.appform.controls.id.value,
      birthDate: moment(this.selectedDate).locale('en').format('YYYY-MM-DD'),
      job: this.appform.controls.job.value,
      profileId: this.appform.controls.profileId.value,
      lastname: this.appform.controls.lastname.value,
      firstname: this.appform.controls.firstname.value,
      nationalCode: this.appform.controls.nationalCode.value,
      about: this.appform.controls.about.value,
      picture: this.appform.controls.picture.value,


    }).subscribe(response =>{
      this.getCurrentuser();
      this.submited = false;
      this.toastrservice.success('ذخیره شد')
    }
    ,
    err =>{
      this.submited = false;
      this.toastrservice.error('خطا')

    })
  }

  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null && this.submited) {
      return true
    }
    return false;
  }
  getCurrentuser() {
    this.userservice.GetCurrentUser().subscribe(response => {
      this.getuserProfile(response.id)
    })
  }

  getuserProfile(userId: number) {
    this.profileservice.GetUserProfileById(userId).subscribe(response => {
      if (response != undefined) {
        this.profileservice.profileInfo.next(response)
      }
    })
  }

  isValidIranianNationalCode(nationalId : any) {

    if (!nationalId.length) {
      this._nationalCode = false;
      return 
    }
    // STEP 0: Validate national Id

    // Check length is 10
    if (nationalId.length < 8 || 10 < nationalId.length) {
      this._nationalCode = true;
      return false;
    }

    // Check if all of the numbers are the same
    if (
      nationalId == "0000000000" ||
      nationalId == "1111111111" ||
      nationalId == "2222222222" ||
      nationalId == "3333333333" ||
      nationalId == "4444444444" ||
      nationalId == "5555555555" ||
      nationalId == "6666666666" ||
      nationalId == "7777777777" ||
      nationalId == "8888888888" ||
      nationalId == "9999999999"
    ) {
      this._nationalCode = true;
      return false;
    }

    // STEP 00 : if nationalId.lenght==8 add two zero on the left
    if (nationalId.length < 10) {
      let zeroNeeded = 10 - nationalId.length;

      let zeroString = "";
      if (zeroNeeded == 2) {
        zeroString = "00";
      } else {
        zeroString = "0";
      }

      nationalId = zeroString.concat(nationalId);
    }

    // STEP 1: Sum all numbers
    let sum = 0;
    for (let i = 0; i < 9; i++) {
      sum += nationalId.charAt(i) * (10 - i);
    }

    // STEP 2: MOD ON 11
    let mod = sum % 11;

    // STEP 3: Check with 2
    let finalValue;
    if (mod >= 2) {
      finalValue = 11 - mod;
    } else {
      finalValue = mod;
    }

    // STEP 4: Final Step check with control value
    if (finalValue == nationalId.charAt(9)) {
      this._nationalCode = false;
      return true;
    } else {
      this._nationalCode = true;
      return false;
    }
  }

}
