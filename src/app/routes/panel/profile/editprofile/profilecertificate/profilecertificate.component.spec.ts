import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilecertificateComponent } from './profilecertificate.component';

describe('ProfilecertificateComponent', () => {
  let component: ProfilecertificateComponent;
  let fixture: ComponentFixture<ProfilecertificateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilecertificateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilecertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
