import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'jalali-moment';
import { ToastrService } from 'ngx-toastr';
import { ProfileService } from 'src/app/core/services/profile.service';

@Component({
  selector: 'app-profilecertificate',
  templateUrl: './profilecertificate.component.html',
  styleUrls: ['./profilecertificate.component.scss']
})
export class ProfilecertificateComponent implements OnInit {
  moment: any = moment;
  profileCertificates : any;
  appform : FormGroup | any;
  title = 'دوره های آموزشی';
  submited = false;
  startDate: any;
  endDate: any;

  constructor(
    private profileservice : ProfileService,
    private formbuild : FormBuilder,
    private toast: ToastrService

  ) { }

  ngOnInit(): void {
    this.initialize();
    this.loadprofileInfo();
  }
  initialize(){
    this.appform = this.formbuild.group({
      id : [null],
      title : ['' , Validators.required],
      location : ['' , Validators.required],
      start : ['' , Validators.required],
      end : ['' , Validators.required],
      profileId : [null],
      description : [''],
    })
    this.startDate = null;
    this.endDate = null;
    this.submited = false;
  
  }

  loadprofileInfo(){
    this.profileservice.profileInfo.subscribe((response : any) =>{
      if (response !=null) {
        this.profileCertificates = response.profileCertificates;
        this.appform.controls.profileId.setValue(response.id)

      }
    })
  }
  submit(){
  this.submited = true
    if (!this.appform.valid) {
      return;
    }
    this.profileservice.UpsertProfileCertificate(
      {
        id: this.appform.controls.id.value,
        start: moment(this.startDate).locale('en').format('YYYY-MM-DD'),
        end: moment(this.endDate).locale('en').format('YYYY-MM-DD'),
        description: this.appform.controls.description.value,
        profileId: this.appform.controls.profileId.value,
        location: this.appform.controls.location.value,
        title: this.appform.controls.title.value,
      }
    ).subscribe({
      next : (response)=> {
        if (response.resultCode === 0) {
          this.toast.success('با موفقیت ذخیره شد');
          this.profileservice.relaodData.next(true);
          this.submited = false;
          this.initialize();
        }

      },
      error :()=>{
      this.submited = false

      }
    })
  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null && this.submited) {
      return true
    }
    return false;
  }
  edit(item : any){
    this.appform.controls.id.setValue(item.id);
    this.appform.controls.title.setValue(item.title);
    this.appform.controls.location.setValue(item.location);
    this.appform.controls.profileId.setValue(item.ownerId);
    this.appform.controls.start.setValue(item.start);
    this.appform.controls.end.setValue(item.end);
    this.appform.controls.description.setValue(item.description);
    this.startDate = moment(item.start);
    this.endDate = moment(item.end);

  }
  remove(id : number){
    const Model = {
      id : id
    }
    this.profileservice.RemoveProfileCertificate(Model).subscribe(response =>{
      if (response.resultCode === 0) {
        this.toast.success('حذف شد');
        this.profileservice.relaodData.next(true);
      }
      else {
        this.toast.error('خطا');

      }
    })

  }
  DATE_VALUE(e: any, mode: any) {
    if (mode === 0) {
      this.appform.controls.start.setValue(e);
    }
    else {
      this.appform.controls.end.setValue(e);

    }
  }
}
