import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilelanguageComponent } from './profilelanguage.component';

describe('ProfilelanguageComponent', () => {
  let component: ProfilelanguageComponent;
  let fixture: ComponentFixture<ProfilelanguageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilelanguageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilelanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
