import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ProfileService } from 'src/app/core/services/profile.service';

@Component({
  selector: 'app-profilelanguage',
  templateUrl: './profilelanguage.component.html',
  styleUrls: ['./profilelanguage.component.scss']
})
export class ProfilelanguageComponent implements OnInit {
  profileLanguages: any;
  appform: FormGroup | any;
  title = 'زبان های خارجه'
  submited = false

  constructor(
    private profileservice: ProfileService,
    private formbuild: FormBuilder,
    private toast: ToastrService

  ) { }

  ngOnInit(): void {
    this.initialize();
    this.loadprofileInfo();
  }
  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['', Validators.required],
      location: [''],
      languageLevel: ['', Validators.required],
      // start : ['' , Validators.required],
      // end : ['' , Validators.required],
      profileId: [null],
      // description : [''],
    })
  }

  loadprofileInfo() {
    this.profileservice.profileInfo.subscribe((response: any) => {
      if (response != null) {
        this.profileLanguages = response.profileLanguages;
        this.appform.controls.profileId.setValue(response.id)
      }
    })
  }
  submit() {
    this.submited = true
    if (!this.appform.valid) {
      return;
    }
    this.profileservice.UpsertProfileLanguage(this.appform.value).subscribe({
      next: (response) => {
        if (response.resultCode === 0) {
          this.toast.success('با موفقیت ذخیره شد');
          this.profileservice.relaodData.next(true);
          this.submited = false;
          this.initialize();
        }
      },
      error: () => {
        this.submited = false
        console.log('err');
      }
    })

  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null && this.submited) {
      return true
    }
    return false;
  }
  edit(item: any) {
    this.appform.controls.id.setValue(item.id);
    this.appform.controls.title.setValue(item.title);
    // this.appform.controls.location.setValue(item.location);
    this.appform.controls.languageLevel.setValue(item.languageLevel);
    this.appform.controls.profileId.setValue(item.ownerId);
  }
  remove(id: number) {
    const Model = {
      id: id
    }
    this.profileservice.RemoveProfileLanguage(Model).subscribe(response => {
      if (response.resultCode === 0) {
        this.toast.success('حذف شد');
        this.profileservice.relaodData.next(true);
      }
    })

  }

}
