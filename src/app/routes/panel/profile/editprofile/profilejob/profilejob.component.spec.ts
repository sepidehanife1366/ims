import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilejobComponent } from './profilejob.component';

describe('ProfilejobComponent', () => {
  let component: ProfilejobComponent;
  let fixture: ComponentFixture<ProfilejobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilejobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilejobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
