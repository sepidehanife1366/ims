import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ProfileinformationComponent } from './profileinformation/profileinformation.component';
import { ProfileawardComponent } from './profileaward/profileaward.component';
import { SharedModule } from 'src/app/sharedComponents/shared.module';
import { ProfilelanguageComponent } from './profilelanguage/profilelanguage.component';
import { ProfilejobComponent } from './profilejob/profilejob.component';
import { ProfilecertificateComponent } from './profilecertificate/profilecertificate.component';
import { ProfileeducationComponent } from './profileeducation/profileeducation.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';


const routes: Routes = [
        {
          path: '',
          component: ProfileinformationComponent ,
          data : {
            title : 'صفحه اصلی/ اطلاعات شخصی'
          }
        }
        ,
        {
          path: 'profileAward',
          component: ProfileawardComponent ,
          data : {
            title : 'صفحه اصلی/ جوایز و افتخارات'
          }
        }
        ,
        {
          path: 'profileLanguage',
          component: ProfilelanguageComponent ,
          data : {
            title : 'صفحه اصلی/ زبان های خارجه'
          }
        }
        ,
        {
          path: 'profileJob',
          component: ProfilejobComponent ,
          data : {
            title : 'صفحه اصلی/  پیشینه شغلی'
          }
        },
        {
          path: 'profileEducation',
          component: ProfileeducationComponent ,
          data : {
            title : 'صفحه اصلی/ پیشینه آموزشی'
          }
        },
        {
          path: 'profileCertificate',
          component: ProfilecertificateComponent ,
          data : {
            title : 'صفحه اصلی/ دوره های آموزشی'
          }
        } ,
        {
          path: 'changepassword',
          component: ChangepasswordComponent ,
          data : {
            title : 'صفحه اصلی/ تغییر کلمه عبور'
          }
        }
];

@NgModule({
  declarations: [
    ProfileinformationComponent,
    ProfileawardComponent,
    ProfilelanguageComponent,
    ProfilejobComponent,
    ProfilecertificateComponent,
    ProfileeducationComponent,
    ChangepasswordComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule.forRoot(),
    AccordionModule.forRoot(),

  ]
})
export class EditprofileModule { }