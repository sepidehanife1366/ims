import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/core/services/authentication.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {

  profileLanguages : any;
  appform : FormGroup | any;
  title = 'تغییر کلمه عبور'

  constructor(
    // private profileservice : ProfileService,
    private authservice : AuthenticationService,
    private formbuild : FormBuilder,
    private toast: ToastrService

  ) { }

  ngOnInit(): void {
    this.initialize();
  }
  initialize(){
    this.appform = this.formbuild.group({
      currentPassword : ['' , Validators.required],
      newPassword : ['' , Validators.required],
    })
  }


  submit(){    
    if (!this.appform.valid) {
      return;
    }
    this.authservice.ChangePassword(this.appform.value).subscribe({
      next : (response) =>{
        if (response.resultCode === 0) {
          this.toast.success('با موفقیت انجام شد');
        } else {
          this.toast.error('خطا');
        }
        
      },
      error :(err)=>{
        console.log('opps');
        
      }
    })

  }

}
