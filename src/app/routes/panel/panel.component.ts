import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';


@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
  animations: [
    trigger('pallete', [
      state('visible',
        style({
          height: '150px',
          opacity: 1
        })),
      state('void,hidden', style({
        height: '0',
        opacity: 0
      })),
      transition('*=>vissible', animate('250ms')),
      transition('*=>void , *=>vissible', animate('180ms')),
    ])
  ]
})
export class PanelComponent implements OnInit {
  breadcrump : any;
  display = false;
  isVissible = false;
  colors =[
    {id : 'default' , color : 'rgb(32, 38, 57)' },
    {id : 'purlpe' , color : 'rgb(98, 29, 84)' },
    {id : 'green' , color : 'rgb(67, 83, 52)' },
    {id : 'dark' , color : 'rgb(35, 31, 32)' },

  ]
  constructor(
    private authservice : AuthenticationService,
    private router : Router,
    public route : ActivatedRoute,

  ){
    // this.checkValidToken();
    
  }

  ngOnInit(): void {
  }
  chatDisplay(){
   this.display = true
   this.isVissible = false;
  }
  close(e : any){
    this.display = e
  }

  togglepallete(){
    this.isVissible = !this.isVissible
  }

  onThemeSwitchChange(item : any) {
    document.body.setAttribute('data-theme', item.id);
  }


}
