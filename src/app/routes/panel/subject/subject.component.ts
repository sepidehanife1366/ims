import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as FileSaver from 'file-saver';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { SubjectService } from 'src/app/core/services/subject.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {
  panelOpenState = true;
  subject : any;
  formsItems : any
  constructor(
    private activatedRoute: ActivatedRoute, 
    private router:Router,
    private subjectservice : SubjectService,
    private cloudservice: CloudStorageService,

    ) {
    
      activatedRoute.queryParams.subscribe(
        params => {
         this.loadSubject(params['id'])
        });

   }

  ngOnInit(): void {
  }

  loadSubject(id : any){
    this.subjectservice.GetSubject(id).subscribe({
      next : (response)=>{
        this.subject = response
        if (JSON.parse(response.form)[0] != null) {
          this.formsItems = JSON.parse(response.form);
        }
      }
    })
  }
  _download(item : any) {
    this._getFile(item);
  }

  _getFile(item : any){
    this.cloudservice.GetFile(item.fileName + item.fileType).subscribe(res=>{
      const blob = new Blob([res]);
       FileSaver.saveAs(blob ,"file" + item.fileType);
    })
  }

}
