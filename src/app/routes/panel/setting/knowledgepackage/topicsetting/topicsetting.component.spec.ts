import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicsettingComponent } from './topicsetting.component';

describe('TopicsettingComponent', () => {
  let component: TopicsettingComponent;
  let fixture: ComponentFixture<TopicsettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopicsettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
