import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { FormService } from 'src/app/core/services/form.service';
import { PackageService } from 'src/app/core/services/package.service';
import { TopicService } from 'src/app/core/services/topic.service';
import { SettopicComponent } from './settopic/settopic.component';
import { UserService } from 'src/app/core/services/user.service';
import { SetknowledgeengineertopicComponent } from './setknowledgeengineertopic/setknowledgeengineertopic.component';

@Component({
  selector: 'app-knowledgepackage',
  templateUrl: './knowledgepackage.component.html',
  styleUrls: ['./knowledgepackage.component.scss']
})
export class KnowledgepackageComponent implements OnInit {
  bsModalRef?: BsModalRef;
  packages: any
  appform: FormGroup | any;
  knowledgemanager = [
    { id: null, name: '...' },
    { id: 0, name: 'مدیر حوزه ثبت' },
    { id: 1, name: 'مدیر مجموعه' },
    { id: 2, name: 'گروهی معین' },
    { id: 3, name: 'ثبت کننده' },
  ]
  disabled = true;
  forms: any = [];
  topics: any = [];
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-lg'
  };
  users: any = [];
  Roles: any = [];
  changeOptions = -1;
  allLightPackages: any
  currentPackageId: any

  @ViewChild('title', { static: true, read: ElementRef }) _title !: ElementRef;

  constructor(
    private packageservice: PackageService,
    private formbuild: FormBuilder,
    private toastservice: ToastrService,
    private formservice: FormService,
    private topicservice: TopicService,
    private modalService: BsModalService,
    private userservice: UserService

  ) { }


  ngOnInit(): void {
    this.getPackageTree();
    this.initialize();
    this.loadusers();
    this.loadRoles()
  }
  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      formId: [null],
      title: ['', Validators.required],
      comment: ['', Validators.required],
      successMessage: ['', Validators.required],
      knowlegWorkerType: [null, Validators.required],
      maximumAcceptableLevel: ['', Validators.required],
      managerPackageId: [null],
      canGroupRegistar: [false],
      hasNoContent: [false],
      canBeEvaluated: [false],
      canEditByExpert: [false],
      canEditByRegistar: [false],
      canEditByParticipant: [false],
      canEditByKnowledgWorker: [false],
      canEditByMembers: [false],
      hasBrowser: [false],
      hasEncyclopedia: [false],
      hasExperts: [false],
      hasForm: [false],
      hasGroup: [false],
      hasMembers: [false],
      hasPov: [false],
    })
  }


  getPackageTree() {
    this.packageservice.GetPackageTree().subscribe({
      next: (response: any) => {
        this.packages = {
          tree: response,
          topicMode: false
        };
      },
      error: (err) => {

      }
    })
  }
  GETITEM(e: any) {
    this.currentPackageId = e.item.id
    this.disabled = false;
    this.loadpackage(e.item.id);
    this.loadForms();
    this.loadTopics(e.item.id);
  }
  loadusers() {
    const model = {
      filter: ''
    }
    this.userservice.GetLightUserList(model).subscribe({
      next: (response) => {
        response.forEach((element: any) => {
          this.users.push({
            name: element.firstname + ' ' + element.lastname,
            username: element.userName,
            id: element.id,
            picture : element.picture
          })

        });
      }
    })

  }
  loadRoles() {
    const model = {
      filter: ''
    }
    this.userservice.GetUserRoleList(model).subscribe({
      next: (response) => {
        this.Roles = response.items
      }
    })

  }
  GET_topic_ITEM(e: any) {
    const initialState: ModalOptions = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg',
      initialState: {
        list: {
          users: this.users,
          roles: this.Roles
        },
        title: e.item.title,
        topicId: e.item.id,
      }
    };
    setTimeout(() => {
      this.bsModalRef = this.modalService.show(SettopicComponent, initialState);
    }, 200);
  }

  loadForms() {
    this.formservice.GetForms().subscribe({
      next: (response) => {
        this.forms = response
      }
    })
  }
  loadTopics(packageId: any) {
    const model = {
      packageId: packageId
    }
    this.topicservice.GetTopicTree(model).subscribe({
      next: (response) => {
        this.topics = response
        this.topics = {
          tree: response,
          topicMode: true
        };
      }
    })
  }
  alert() {
    if (this.disabled) {
      this.toastservice.warning('بسته مورد نظر را انتخاب نمایید')
    }
  }

  save(mode: number) {
    const model = [];
    if (mode === 0) {
      model.push(
        { path: '/title', op: 'add', value: this.appform.controls['title'].value },
        { path: '/comment', op: 'add', value: this.appform.controls['comment'].value },
        { path: '/successMessage', op: 'add', value: this.appform.controls['successMessage'].value },
        { path: '/knowlegWorkerType', op: 'add', value: this.appform.controls['knowlegWorkerType'].value },
        { path: '/maximumAcceptableLevel', op: 'add', value: this.appform.controls['maximumAcceptableLevel'].value },
        { path: '/canGroupRegistar', op: 'add', value: this.appform.controls['canGroupRegistar'].value },
        { path: '/hasNoContent', op: 'add', value: this.appform.controls['hasNoContent'].value },
        { path: '/canBeEvaluated', op: 'add', value: this.appform.controls['canBeEvaluated'].value },
        { path: '/canEditByExpert', op: 'add', value: this.appform.controls['canEditByExpert'].value },
        { path: '/canEditByRegistar', op: 'add', value: this.appform.controls['canEditByRegistar'].value },
        { path: '/canEditByParticipant', op: 'add', value: this.appform.controls['canEditByParticipant'].value },
        { path: '/canEditByMembers', op: 'add', value: this.appform.controls['canEditByMembers'].value },
        { path: '/canEditByKnowledgWorker', op: 'add', value: this.appform.controls['canEditByKnowledgWorker'].value },
        { path: '/managerPackageId', op: 'add', value: this.appform.controls['managerPackageId'].value },

      )
    }
    if (mode === 1) {
      model.push(
        { path: '/formId', op: 'add', value: this.appform.controls['formId'].value },

      )
    }
    if (mode === 3) {
      model.push(
        { path: '/hasBrowser', op: 'add', value: this.appform.controls['hasBrowser'].value },
        { path: '/hasEncyclopedia', op: 'add', value: this.appform.controls['hasEncyclopedia'].value },
        { path: '/hasExperts', op: 'add', value: this.appform.controls['hasExperts'].value },
        { path: '/hasForm', op: 'add', value: this.appform.controls['hasForm'].value },
        { path: '/hasGroup', op: 'add', value: this.appform.controls['hasGroup'].value },
        { path: '/hasMembers', op: 'add', value: this.appform.controls['hasMembers'].value },
        { path: '/hasPov', op: 'add', value: this.appform.controls['hasPov'].value },
      )
    }

    this.packageservice.UpdatePackage(this.appform.controls['id'].value, model).subscribe({
      next: (response) => {
        if (response != undefined) {
          this.toastservice.success('با موفقیت ذخیره شد')
          this.topicservice.topicIds.subscribe((res: any) => {
            if (res != null) {
              if (res.length > 0) {
                this.SetPackageKnowledgEngineerTopic(this.currentPackageId, res)
              }
            }
          })
        }
      }
    })
  }

  SetPackageKnowledgEngineerTopic(packageId: any, data: any) {
    const model = {
      packageId: packageId,
      topicId: data
    }
    this.packageservice.SetPackageKnowledgEngineerTopic(model).subscribe({
      next: (response) => {

      }
    })

  }

  loadpackage(id: any) {
    this.appform.controls['id'].setValue(id);
    this.packageservice.GetPackage(id).subscribe({
      next: (response) => {
        this.formSetvalue(response);
        this.loadlightpack();
      }
    })
  }
  formSetvalue(data: any) {
    this.changeOptions = data.knowlegWorkerType
    this.appform.controls['formId'].setValue(data.formId);
    this.appform.controls['title'].setValue(data.title);
    (this._title.nativeElement as HTMLInputElement).readOnly = true;
    this.appform.controls['comment'].setValue(data.comment);
    this.appform.controls['successMessage'].setValue(data.successMessage);
    this.appform.controls['knowlegWorkerType'].setValue(data.knowlegWorkerType);
    this.appform.controls['maximumAcceptableLevel'].setValue(data.maximumAcceptableLevel);
    this.appform.controls['canGroupRegistar'].setValue(data.canGroupRegistar);
    this.appform.controls['hasNoContent'].setValue(data.hasNoContent);
    this.appform.controls['canBeEvaluated'].setValue(data.canBeEvaluated);
    this.appform.controls['canEditByExpert'].setValue(data.canEditByExpert);
    this.appform.controls['canEditByRegistar'].setValue(data.canEditByRegistar);
    this.appform.controls['canEditByParticipant'].setValue(data.canEditByParticipant);
    this.appform.controls['canEditByKnowledgWorker'].setValue(data.canEditByKnowledgWorker);
    this.appform.controls['canEditByMembers'].setValue(data.canEditByMembers);
    this.appform.controls['hasBrowser'].setValue(data.hasBrowser);
    this.appform.controls['hasExperts'].setValue(data.hasExperts);
    this.appform.controls['hasEncyclopedia'].setValue(data.hasEncyclopedia);
    this.appform.controls['hasForm'].setValue(data.hasForm);
    this.appform.controls['hasGroup'].setValue(data.hasGroup);
    this.appform.controls['hasMembers'].setValue(data.hasMembers);
    this.appform.controls['hasPov'].setValue(data.hasPov);
    this.appform.controls['managerPackageId'].setValue(data.managerPackageId);
  }

  openOption(e: any) {
    this.changeOptions = e.value
    if (Number(e.value) === 1) {
      this.loadlightpack();
    }
  }
  loadlightpack() {
    this.packageservice.GetLightPackage().subscribe({
      next: (response) => {
        this.allLightPackages = response
      }
    })
  }
  openmodal() {
    const initialState: ModalOptions = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg',
      initialState: {
        list: this.packages,
        title: '',
        packageId: this.currentPackageId
      }
    };
    setTimeout(() => {
      this.bsModalRef = this.modalService.show(SetknowledgeengineertopicComponent, initialState);
    }, 200);

  }


}
