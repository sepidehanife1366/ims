import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettopicComponent } from './settopic.component';

describe('SettopicComponent', () => {
  let component: SettopicComponent;
  let fixture: ComponentFixture<SettopicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettopicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
