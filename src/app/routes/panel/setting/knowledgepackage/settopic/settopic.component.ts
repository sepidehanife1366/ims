import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { TopicService } from 'src/app/core/services/topic.service';
import * as moment from 'jalali-moment';

@Component({
  selector: 'app-settopic',
  templateUrl: './settopic.component.html',
  styleUrls: ['./settopic.component.scss']
})
export class SettopicComponent implements OnInit, OnChanges {
  title?: string
  list: any;
  topicId?: any;

  submitModel = {
    member: {
      topicId: null,
      userIds: []
    },
    manager: {
      topicId: null,
      userIds: []
    },
    expert: {
      topicId: null,
      userIds: []
    }
  }
  updateTopicsetting = {
    entity: {
      id: null,
      defualtView: false,
      defualtSummaryView: false,
      defualtDownloadView: false,
      allExpertsApprove: false,
      allMembersApprove: false,
      allAdminsApprove: false,

    }
  }

  TopicpermissionModal = false;
  TopicUserPermissions: any = [];

  types = [
    { id: null, name: '...' },
    { id: 0, name: 'کاربر' },
    { id: 1, name: 'گروه' },

  ]


  updateUserPermmisiontype: any =
    {
      entity: {
        id: null,
        type: null,
        topicSettingId: null,
        topicPermissions: [],
        topicMembers: []
      }
    }
  _usertype = false;
  show = false;
  selectedDate: any
  topicUserPermmisiontypelist = [
    {
      value: 0,
      title: 'مشاهده',
      isdisabled: true,
      ischecked: false,
      date : ''
    },
    {
      value: 1,
      title: 'مشاهده چکیده',
      isdisabled: true,
      ischecked: false,
      date : ''
    },
    {
      value: 2,
      title: 'ویرایش',
      isdisabled: true,
      ischecked: false,
      date : ''
    },
    {
      value: 3,
      title: 'حذف',
      isdisabled: true,
      ischecked: false,
      date : ''
    },
    {
      value: 4,
      title: 'دانلود',
      isdisabled: true,
      ischecked: false,
      date : ''
    }
  ];
  memberselected : any;
  expertselected : any;
  managerselected: any;
  selectedgroup : any = []
  constructor
    (public bsModalRef: BsModalRef,
      private topicservice: TopicService,
      private toastservice: ToastrService

    ) { }
  ngOnChanges(): void {}

  ngOnInit(): void {
    this.selectedUsersLoad(this.topicId)
  }
  submit(Topicpermission: boolean) {
    if (!Topicpermission) {
        this.setmember(this.submitModel.member);
        this.setmanager(this.submitModel.manager);
        this.setexpert(this.submitModel.expert);
    }
    else {
      this.updateUserPermmisiontype.entity.topicSettingId = this.updateTopicsetting.entity.id;
      this.updateUserPermmisiontype.entity.topicMembers = [...new Set(this.updateUserPermmisiontype.entity.topicMembers)]

      if ( this.updateUserPermmisiontype.entity.type == null) {
        return
      }
      this.topicservice.UpsertTopicUserPermission(this.updateUserPermmisiontype).subscribe({
        next: (response) => {
          if (response.resultCode === 0) {
            this.toastservice.success('با موفقیت ذخیره شد');
            this.loadTopicUserPermissions(this.updateTopicsetting.entity.id);
            
            setTimeout(() => {
              this.topicUserPermmisiontypelist.forEach(el =>{
                el.ischecked = false;
                el.date = '';
                el.isdisabled= true;
              })
              this.updateUserPermmisiontype.entity.topicMembers = []
              this.updateUserPermmisiontype.entity.type = null
              this.updateUserPermmisiontype.entity.topicPermissions = [];
              this.show = false
            }, 200);
 

          }
        }
      })
    }


  }
  setmember(model: any) {
    this.topicservice.SetTopicMember(model).subscribe({
      next: (res) => {
        if (res.resultCode === 0) {
          this.toastservice.success('با موفقیت ذخیره شد')

        }
        else {
          this.toastservice.error('خطا')
        }
      },
      error: (err) => {
        console.log('opps');
      }
    })
  }
  setmanager(model: any) {
    this.topicservice.SetTopicManager(model).subscribe({
      next: (res) => {
        if (res.resultCode === 0) {
          this.toastservice.success('با موفقیت ذخیره شد')

        }
        else {
          this.toastservice.error('خطا')
        }
      },
      error: (err) => {
        console.log('opps');
      }
    })
  }
  setexpert(model: any) {
    this.topicservice.SetTopicExpert(model).subscribe({
      next: (res) => {
        if (res.resultCode === 0) {
          this.toastservice.success('با موفقیت ذخیره شد')

        }
        else {
          this.toastservice.error('خطا')
        }
      },
      error: (err) => {
        console.log('opps');
      }
    })
  }

  selectedUsers(e: any, mode: any) {
    let selected = e;
    const ids = selected.map((s: any) => s.id)
    if (mode === 0) {
      this.submitModel.member.topicId = this.topicId;
      this.submitModel.member.userIds = ids
    }
    if (mode === 1) {
      this.submitModel.manager.topicId = this.topicId;
      this.submitModel.manager.userIds = ids
    }
    if (mode === 2) {
      this.submitModel.expert.topicId = this.topicId;
      this.submitModel.expert.userIds = ids
    }
  }

  openTopicpermissionModal() {
    this.TopicpermissionModal = true;
    this.gettopicsettingId(this.topicId);
  }

  gettopicsettingId(topicId: any) {
    this.topicservice.GetTopicSetting(topicId).subscribe({
      next: (response) => {
        this.updateTopicsetting.entity.id = response.id
        this.updateTopicsetting.entity.defualtView = response.defualtView
        this.updateTopicsetting.entity.defualtSummaryView = response.defualtSummaryView
        this.updateTopicsetting.entity.defualtDownloadView = response.defualtDownloadView
        this.updateTopicsetting.entity.allAdminsApprove = response.allAdminsApprove
        this.updateTopicsetting.entity.allExpertsApprove = response.allExpertsApprove
        this.updateTopicsetting.entity.allMembersApprove = response.allMembersApprove;
        this.loadTopicUserPermissions(response.id)

      }
    })
  }
  updateTopicSetting() {
    this.topicservice.UpdateTopicSetting(this.updateTopicsetting).subscribe({
      next: (response) => {
        this.toastservice.success('با موفقیت ذخیره شد')
      }
    })

  }

  loadTopicUserPermissions(id: any) {
    this.topicservice.GetTopicUserPermissions(id).subscribe({
      next: (response) => {
        this.TopicUserPermissions = response;
      },
      error: (err) => {
        console.log('opps');

      }
    })
  }

  update(item: any) {
    item.topicPermissions.forEach((_item : any) => {
      this.topicUserPermmisiontypelist.forEach((type : any) => {
        if (_item.permissionType === type.value) {
          type.ischecked = true;
          type.isdisabled = false;
          if (_item.expirationDate !='' && _item.expirationDate != null ) {
            type.date = moment(_item.expirationDate);
          }
        }
      });
    });
    if (item.type != null) {
      this.show = true
    }
    this.updateUserPermmisiontype.entity.type = item.type
    this.updateUserPermmisiontype.entity.topicMembers = item.topicUsers.map((s : any)=> s.id)
    this.updateUserPermmisiontype.entity.id = item.id;
    this.updateUserPermmisiontype.entity.topicPermissions = item.topicPermissions;
    this.selectedgroup = item.topicGroups.map((s : any)=> s.id)    
  }
  remove(item: any) {
    this.topicservice.DeleteTopicUserPermission(item.id).subscribe({
      next: (response) => {
        if (response.resultCode === 0) {
          this.loadTopicUserPermissions(item.topicSettingId)
          this.toastservice.success('حذف شد')
        }

      }
    })

  }
  selectedUsertype(e: any, mode: any) {
    let selected = e;
    if (mode === 0) {
      if (selected.id === null) {
        this.show = false
      }
      else {
        this.show = true
        if (selected.id === 0) {
          this._usertype = true
        }
        else {
          this._usertype = false;
        }
      }
      const type = selected.id;
      this.updateUserPermmisiontype.entity.type = type;
    }
    else {
      this.updateUserPermmisiontype.entity.topicMembers = []
      let ids = selected.map((s: any) => s.id)
      ids = [...new Set(ids)]
      this.updateUserPermmisiontype.entity.topicMembers = ids;
    }
  }
  DATE_VALUE(e: any, value: any) {
    if (e != undefined) {
      this.updateUserPermmisiontype.entity.topicPermissions.forEach((el: any) => {
        if (el.permissionType === value) {
          el.expirationDate = moment(this.selectedDate)
        }
      })
    }

  }
  getvalue(e: any, i: any, item: any) {
    const checked = e.target.checked;
    item.isdisabled = !item.isdisabled;
    if (checked) {
      item.ischecked = true
      this.updateUserPermmisiontype.entity.topicPermissions.push(
        {
          permissionType: item.value,
          expirationDate: ''
        }
      )
    }
    else {
      item.ischecked = false;
      item.date = ''
      this.updateUserPermmisiontype.entity.topicPermissions = this.updateUserPermmisiontype.entity.topicPermissions.filter(function (el: any) {
        return el.permissionType !== item.value
      })
    }

  }
  selectedUsersLoad(id : any){
    this.getmember(id);
    this.getexpert(id);
    this.getmanager(id);
  }
  getmember(id : any){
    this.topicservice.GetTopicMember(id).subscribe({
      next : (response)=>{
        this.memberselected = response.map((s : any)=> s.userId)
        this.submitModel.member.userIds = this.memberselected
        this.submitModel.member.topicId = this.topicId

      }
    })

  }
  getexpert(id : any){
    this.topicservice.GetTopicExpert(id).subscribe({
      next : (response)=>{
        this.expertselected = response.map((s : any)=> s.userId);
        this.submitModel.expert.userIds = this.expertselected
        this.submitModel.expert.topicId = this.topicId
      }
    })
  }
  getmanager(id : any){
    this.topicservice.GetTopicManager(id).subscribe({
      next : (response)=>{
        this.managerselected = response.map((s : any)=> s.userId);
        this.submitModel.manager.userIds = this.managerselected
        this.submitModel.manager.topicId = this.topicId

      }
    })
  }


}
