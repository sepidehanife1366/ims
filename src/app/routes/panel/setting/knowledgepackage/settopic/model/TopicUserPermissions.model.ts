export class TopicUserPermissionsModel {
    constructor(
        public view : boolean,
        public edit : boolean,
        public remove : boolean,
        public summaryview : boolean,
        public download : boolean,
        public expirationDate : string,

    ) {
        
    }
}


