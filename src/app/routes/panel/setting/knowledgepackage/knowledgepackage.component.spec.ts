import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KnowledgepackageComponent } from './knowledgepackage.component';

describe('KnowledgepackageComponent', () => {
  let component: KnowledgepackageComponent;
  let fixture: ComponentFixture<KnowledgepackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KnowledgepackageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KnowledgepackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
