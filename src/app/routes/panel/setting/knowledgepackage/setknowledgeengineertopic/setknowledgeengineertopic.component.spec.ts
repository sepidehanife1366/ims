import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetknowledgeengineertopicComponent } from './setknowledgeengineertopic.component';

describe('SetknowledgeengineertopicComponent', () => {
  let component: SetknowledgeengineertopicComponent;
  let fixture: ComponentFixture<SetknowledgeengineertopicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetknowledgeengineertopicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetknowledgeengineertopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
