import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PackageService } from 'src/app/core/services/package.service';
import { TopicService } from 'src/app/core/services/topic.service';

@Component({
  selector: 'app-setknowledgeengineertopic',
  templateUrl: './setknowledgeengineertopic.component.html',
  styleUrls: ['./setknowledgeengineertopic.component.scss']
})
export class SetknowledgeengineertopicComponent implements OnInit {
  title?: string
  list: any;
  topicId?: any;
  packages : any;
  topics : any;
  packageId : any;
  selectedtopics : any = [];
  topicIds : any
  constructor(
    private packageservice: PackageService,
    private topicservice : TopicService,
    public bsModalRef: BsModalRef,
    public bs : BsModalService

  ) { }

  ngOnInit(): void {
    this.getPackageTree();    
    this.loadTopics(this.packageId);
  }

  getPackageTree() {
    this.packageservice.GetPackageTree().subscribe({
      next: (response: any) => {
        this.packages = {
          tree: response,
          topicMode: false
        };
      },
      error: (err) => {

      }
    })
  }

  loadTopics(packageId : any){
    const model ={
      packageId : packageId
    }
    this.topicservice.GetTopicTree(model).subscribe({
      next : (response)=>{
        this.topics = response
        this.topics = {
          tree : response ,
          topicMode : true
        };  
      }
    })
  }

  getItem(e : any){
    this.loadTopics(e.item.id);
  }
  gettopicItem(e : any){
    if (e.checked) {
      this.selectedtopics.push(e.item)
    }
    else {
      this.selectedtopics = this.selectedtopics.filter(function (item: any) {
        return item !== e.item
      })
    }
   this.topicIds = this.selectedtopics.map((s : any)=> s.id);
  }
  submit(){
    this.topicservice.topicIds.next(this.topicIds)
    this.bsModalRef.hide();
  }

}
