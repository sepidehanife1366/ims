import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {
  users: any = [];
  title = 'لیست سطوح دسترسی ها';
  totalItems = 0
  currentPage = 0;
  modalRef?: BsModalRef;
  roles: any = [];

  pagefilter = {
    page: 0,
    pageCount: 10,
    sorting: "",
    filter: ""
  }

  modal = {
    title: ''
  }

  _selectedModel: any = {
    userId: 0,
    roleIds: []
  }

  _rolesselected: any = []

  constructor(
    private userservice: UserService,
    private modalService: BsModalService,
  ) { }

  ngOnInit(): void {
    this.LoadUsersList(this.pagefilter)
  }

  LoadUsersList(pagefilter: any) {
    this.userservice.GetUserlist(pagefilter).subscribe({
      next: (response) => {
        this.users = response.items
      }
    })
  }

  getrolesList(model: any) {
    return new Promise(resolve => {
      setTimeout(() => {
        this.userservice.GetUserRoleList(model).subscribe(response => {
          this.roles = response
          resolve(response)
        }, err => console.log('err'))
      }, 0);
    });
  }

  assignRole(item: any, template: any, name: string) {
    this.modal.title = name
    this.getrolesList({
      page: 0,
      pageCount: 0,
      sorting: "",
      filter: ""
    }).then(res => {
      this.modalRef = this.modalService.show(template , {backdrop : 'static' , keyboard : false});
      this._selectedModel.userId = item.id
    })
  }

  pageChange(e: any) {
    this.pagefilter.page = e.page
    this.LoadUsersList(this.pagefilter)
  }

  _selectedRoles(e: any, item: any) {
    item.selected = e
    this._rolesselected.push(item);
    this._rolesselected = [...new Set(this._rolesselected)]
  }

  setroles(rolesselected: any) {
    this._selectedModel.roleIds = [];
    const promise = new Promise<void>((resolve, reject) => {
      rolesselected.forEach((element: any, index: any, array: any) => {
        if (element.selected) {
          this._selectedModel.roleIds.push(element.id)
        }
        if (index === array.length - 1) {
          setTimeout(() => { resolve(); }, 500);
        }
      });
    })

    promise.then(res => {
      this._Assignroles(this._selectedModel).then(res => {
        this.modalRef?.hide();
      })
    })
  }

  _Assignroles(model: any) {
    return new Promise(resolve => {
      setTimeout(() => {
        this.userservice.AssignRoleToUser(model).subscribe(response => {
          resolve(response)
        }, err => {
          console.log('err');
        })
      }, 0);
    });
  }
}
