import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: any = []
  pagefilter = {
    page: 0,
    pageCount: 10,
    sorting: "",
    filter: ""
  }

  totalItems = 0
  currentPage = 0;

  constructor(
    private userservice: UserService,
    private router : Router,
    private domSanitizer: DomSanitizer,

  ) { }

  ngOnInit(): void {
    this.loadUsers(this.pagefilter)
  }
  setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }

  loadUsers(filter : any) {
    this.userservice.GetUserlist(filter).subscribe({
      next: (response) => {
        this.users = response.items
        this.totalItems = response.totalCount;
        this.users.forEach((element : any) => {
          if (element.picture != '') {
            element.picture = this.domSanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + element.picture);
          }
        });

      }
    })
  }
  updateUser(status : any , item : any){
    if (status === 1 && item != null) {
      this.router.navigate(['/updateuser'] ,{queryParams : {id : item.id} })
    }
    else{
      this.router.navigate(['/updateuser'])
    }
    localStorage.setItem('%Up%User' , status)

  }
  pageChange(e : any){
   this.pagefilter.page = e.page
   this.loadUsers(this.pagefilter)
  }

}
