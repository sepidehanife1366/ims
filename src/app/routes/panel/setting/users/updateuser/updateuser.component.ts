import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.scss']
})
export class UpdateuserComponent implements OnInit {

  appform: FormGroup | any
  _nationalCode = false;
  _isEdit = false;
  title = {
    add: 'ایجاد کاربر جدید',
    isEdit: 'ویرایش کاربر'
  }
  selectedImage: any = ''
  imageUrl: any 
  submitted = false
  

  constructor(
    private formbuild: FormBuilder,
    private userservice: UserService,
    private upload: CloudStorageService,
    private domSanitizer: DomSanitizer,
    private router : Router,
    private activatedroute: ActivatedRoute,
    private toastservice : ToastrService
  ) { 
    activatedroute.queryParams.subscribe(
      params => {
        if (params['id'] != undefined) {
          this.getUser(params['id'])

        }

      });
  }

  ngOnInit(): void {
    this.initialize();
    this.getstatus();
  }
  getUser(id : any){
    const model ={
      userId : id
    }
    this.userservice.GetUserById(model).subscribe({
      next :(response)=>{
        this.formsetValue(response)

      }
    })

  }

  formsetValue(data : any){
    this.appform.controls['id'].setValue(data.id)
    this.appform.controls['userName'].setValue(data.userName)
    this.appform.controls['password'].setValue('')
    this.appform.controls['lastname'].setValue(data.lastname)
    this.appform.controls['firstname'].setValue(data.firstname)
    this.appform.controls['email'].setValue(data.email)
    this.appform.controls['picture'].setValue(data.pictureName)
    this.appform.controls['nationalCode'].setValue(data.nationalCode)
    this.appform.controls['phoneNumber'].setValue(data.phoneNumber)    
    this.selectedImage =  'http://188.121.109.156:5036/uploadTemp/'+ data.pictureName
  }

  getstatus() {
    this._isEdit = this.getIsEdit();
  }
  validateClass(controlname: any) {
      if (this.appform?.controls[controlname].errors != null  && this.submitted) {
        return true
      }
      return false;
  }

  getIsEdit() {
    if (JSON.parse(localStorage.getItem('%Up%User') as any) === 0) {
      return false
    }
    return true
  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      userName: ['', Validators.required],
      password: [''],
      lastname: ['', Validators.required],
      firstname: ['', Validators.required],
      email: ['' , Validators.required],
      picture: [null , Validators.required],
      nationalCode: ['', Validators.required],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern("^(\\+98|0)?9\\d{9}$")])],
    })
    this.appform.controls['password'].setErrors(!this._isEdit ? {'required': true } : null);
  }
  submit(isEdit: boolean) {
    this.submitted = true
    if (!this.appform.valid) {
      return;
    }
    this.appform.controls.userName.setValue(this.appform.controls.userName.value.replaceAll(' ' , ''))
    this.appform.controls.nationalCode.setValue(this.appform.controls.nationalCode.value)

    this.userservice.UpsertUser(this.appform.value).subscribe({
      next: (response) => {
        if (response.resultCode === 0) {
          this.router.navigate(['users'])
        }
        else{
         this.toastservice.warning('نام کاربری از اعداد استفاده شود')
        }
      },
      error: (err) => {
      console.log('err');

      }
    })


  }

  isValidIranianNationalCode(e: any) {
    let nationalId = e.value
    if (!nationalId?.length) {
      this._nationalCode = false;
      return
    }
    // STEP 0: Validate national Id

    // Check length is 10
    if (nationalId?.length < 8 || 10 < nationalId?.length) {
      this._nationalCode = true;
      return false;
    }

    // Check if all of the numbers are the same
    if (
      nationalId == "0000000000" ||
      nationalId == "1111111111" ||
      nationalId == "2222222222" ||
      nationalId == "3333333333" ||
      nationalId == "4444444444" ||
      nationalId == "5555555555" ||
      nationalId == "6666666666" ||
      nationalId == "7777777777" ||
      nationalId == "8888888888" ||
      nationalId == "9999999999"
    ) {
      this._nationalCode = true;
      return false;
    }

    // STEP 00 : if nationalId.lenght==8 add two zero on the left
    if (nationalId.length < 10) {
      let zeroNeeded = 10 - nationalId.length;

      let zeroString = "";
      if (zeroNeeded == 2) {
        zeroString = "00";
      } else {
        zeroString = "0";
      }

      nationalId = zeroString.concat(nationalId);
    }

    // STEP 1: Sum all numbers
    let sum = 0;
    for (let i = 0; i < 9; i++) {
      sum += nationalId.charAt(i) * (10 - i);
    }

    // STEP 2: MOD ON 11
    let mod = sum % 11;

    // STEP 3: Check with 2
    let finalValue;
    if (mod >= 2) {
      finalValue = 11 - mod;
    } else {
      finalValue = mod;
    }

    // STEP 4: Final Step check with control value
    if (finalValue == nationalId.charAt(9)) {
      this._nationalCode = false;
      return true;
    } else {
      this._nationalCode = true;
      return false;
    }
  }

  uploadimage(input: any) {
    input.click();
  }
  getimageValue(e: any) {
    const file = e.target.files[0];
    this.uploadFile(file)
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imageUrl = this.domSanitizer.bypassSecurityTrustUrl('data:image/png;base64' + reader.result);
      this.selectedImage = reader.result
    };

  }
  uploadFile(file: any) {
    const formData = new FormData();
    formData.append('file', file);
    this.upload.UploadFile(formData).subscribe(response => {
      this.appform.controls['picture'].setValue(response.cloudFileName);
    })
  }

}
