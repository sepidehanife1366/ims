import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  Renderer2,
  ViewChildren,
} from '@angular/core';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
  constructor(private renderer: Renderer2, private elementRef: ElementRef) { }

  ngOnInit(): void {
    // this.setbg();
    setTimeout(() => {
      this.set_box_background();
    }, 0.9);

  }
  ngAfterViewInit() {
  }

  set_box_background() {
    let myElement = this.elementRef.nativeElement.querySelectorAll('._settingbox') as NodeList[];
    myElement.forEach((m: any) => {
      this.set_color(m);
    });
  }

  set_color(el: Element) {
    var randColor = this.generate_random_color();
    this.renderer.setStyle(el, 'color', randColor.color);
    this.renderer.setStyle(el, 'border', randColor.border);
    this.renderer.setStyle(el, 'cursor', 'pointer');
  }
  generate_random_color() {
    let colors =
      [{ backgroundColor: '#fd397a12', color: '#fd397a', border: '1px solid #fd397a' },
      { backgroundColor: '#5867dd12', color: '#5867dd', border: '1px solid #5867dd' },
      { backgroundColor: '#ffb82212', color: '#ffb822', border: '1px solid #ffb822' },
      { backgroundColor: '#1dc9b712', color: '#1dc9b7', border: '1px solid #1dc9b7' },
      { backgroundColor: '#b11791', color: '#b11791', border: '1px solid #b11791' },

      ];
    return this.getRandomItem<any>(colors);
  }
  getRandomItem<T>(array: T[]): T {
    const randomIndex = Math.floor(Math.random() * array.length);
    const randomItem = array.splice(randomIndex, 1)[0];
    return randomItem;
  }
}
