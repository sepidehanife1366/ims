import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateuserroleComponent } from './updateuserrole.component';

describe('UpdateuserroleComponent', () => {
  let component: UpdateuserroleComponent;
  let fixture: ComponentFixture<UpdateuserroleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateuserroleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateuserroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
