import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-updateuserrole',
  templateUrl: './updateuserrole.component.html',
  styleUrls: ['./updateuserrole.component.scss']
})
export class UpdateuserroleComponent implements OnInit {
  appform : FormGroup | any
  _isEdit = false;
  title = {
    add : 'ایجاد نقش جدید' ,
    isEdit : 'ویرایش نقش'
  }
  constructor(
    private formbuild : FormBuilder,
    private userservice : UserService,
    private activatedroute: ActivatedRoute,
    private router : Router,
    private toastservice : ToastrService 

  ) { }

  ngOnInit(): void {
    this.getstatus();
    this.initialize();
    if (this._isEdit) {
      this.getRole();
    }
  }
  initialize(){
    this.appform = this.formbuild.group({
      id : [null],
      name : ['' , Validators.required],
    })
  }
  getstatus(){
    this._isEdit = this.getIsEdit();    
  }
  getRole(){
    const id = this.activatedroute.snapshot.queryParamMap.get('id');
    const name = this.activatedroute.snapshot.queryParamMap.get('name');
    this.formsetvalue(id , name)
  }
  formsetvalue(id : any , name : any){
    this.appform.controls['id'].setValue(+id);
    this.appform.controls['name'].setValue(name);
  }
  getIsEdit(){
    if (JSON.parse(localStorage.getItem('%Up%Role') as any) === 0) {
      return false
    }
    return true
  }
  submit(){
    if (!this.appform.valid) {
      return;
    }
    this.userservice.UpsertRole(this.appform.value).subscribe({
      next :(response)=>{
        this.router.navigate(['../usersrole']);
        this.toastservice.success('')
      },
      error : (err)=>{

      }
    })
  }


}
