import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-usersrole',
  templateUrl: './usersrole.component.html',
  styleUrls: ['./usersrole.component.scss']
})
export class UsersroleComponent implements OnInit {
  usersrole: any = []
  pagefilter = {
    page: 0,
    pageCount: 10,
    sorting: "",
    filter: ""
  }
  title = 'لیست نقش ها';
  totalItems = 0
  currentPage = 0;

  modal = {
    title: ''
  }
  modalRef?: BsModalRef;
  permossions: any = [];

  selectedpermission = {
    value: '',
    selected: false
  }
  roleId: any

  constructor(
    private userservice: UserService,
    private router: Router,
    private modalService: BsModalService,
  ) { }

  ngOnInit(): void {
    this.LoadUserRoleList(this.pagefilter)
  }

  LoadUserRoleList(pagefilter: any) {
    this.userservice.GetUserRoleList(pagefilter).subscribe({
      next: (response) => {
        this.usersrole = response.items
      }
    })
  }
  updateRole(status: any, item: any) {

    if (status === 1 && item != null) {
      this.router.navigate(['/updaterole'], { queryParams: { id: item.id, name: item.name } })
    }
    else {
      this.router.navigate(['/updaterole'])
    }
    localStorage.setItem('%Up%Role', status)
  }

  getpermissions(template: any, name: string, item: any) {
    let Item = item
    this.permossions = []
    const promise = new Promise<void>((resolve, reject) => {
      this.modal.title = name

      const model = {
        page: 0,
        pageCount: 0,
        sorting: "",
        filter: ""
      }

      this._getpermissions(model).then((result: any) => {
        result.items.forEach((per: any, index: any, array: any) => {
          this.permossions.push({
            category: per.category,
            roleId: item.id,
            permissions: []
          })
          this.permossions = [... new Set(this.permossions)]
          per.permissions.forEach((element: any) => {
            this.permossions.forEach((_el: any) => {
              if (per.category === _el.category) {
                _el.permissions.push({
                  value: element,
                  selected: false,
                })
              }
            });
          });
          if (index === array.length - 1) {
            setTimeout(() => { resolve(); }, 500);
          }
        });
      });
    })

    promise.then(res => {
      const model = {
        page: 0,
        pageCount: 0,
        sorting: "",
        roleId: Item.id,
        filter: ""
      }

      this._getpermissions(model).then((response: any) => {
        response.items.forEach((respermit: any) => {
          this.permossions.forEach((apppermit: any) => {
            apppermit.roleId = model.roleId
            apppermit.permissions.forEach((_app: any) => {
              if (respermit.category === apppermit.category) {
                respermit.permissions.forEach((_res: any) => {

                  if (_res === _app.value) {
                    _app.selected = true
                  }

                });
              }
            });
          });
        });
      })
      setTimeout(() => {
        this.modalRef = this.modalService.show(template, { backdrop: 'static', keyboard: false, class: 'modal-lg' });
      }, 200);
    })
  }

  _getpermissions(model: any) {
    return new Promise(resolve => {
      setTimeout(() => {
        this.userservice.GetPermissionList(model).subscribe(response => {
          resolve(response)
        }, err => console.log('err')
        )
      }, 0);
    });
  }

  setrolepermission(selected: any) {
    let s: any = [];
    const model = {
      roleId: 0,
      permissions: []
    }
    selected.forEach((_s: any) => {
      model.roleId = _s.roleId;
      const select = _s.permissions.filter((x: any) => x.selected === true);

      select.forEach((element: any) => {
        s.push(element)
      });
    });
    model.permissions = s.map((s: any) => s.value);
    this._setrolespermission(model);
  }

  _setrolespermission(model: any) {
    this.userservice.SetRolePermission(model).subscribe(response => {
      this.modalRef?.hide();
    })
  }

}
