import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormService } from 'src/app/core/services/form.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  forms: any = []
  totalItems = 0
  currentPage = 0;

  constructor(
    private formservice: FormService,
    private router : Router,
    private toastservice : ToastrService

  ) { }

  ngOnInit(): void {
    this.loadforms();
    
  }
  loadforms(){
    this.formservice.GetForms().subscribe({
      next : (response)=>{
        this.forms = response
        this.forms.forEach((element : any) => {
          try {
            if (element.description != "") {
              element.description = JSON.parse(element.description.replace(/'/g, '"'));
            }
          } catch (error) {
            console.log('opps');
          }
          
        });
      }
    })
  }

  updateform(status : any , item : any){    
    this.router.navigate(['/updateform'])
    localStorage.setItem('%Up%form' , JSON.stringify({status : status , id : item?.id}))
  }
  remove(item : any){
    Swal.fire({
      html : 
      '<strong class="fs-14 mb-3">'+ 'آیا از حذف ' + item.name + ' '+  'مطمئن هستید ؟ ' +'</strong>'
      + '<p class="fs-12 m-0 text-danger"> این عملیات برگشت ناپذیر است !</p>'
      ,
      showCancelButton: true,
      confirmButtonText: 'بله, حذف شود!',
      cancelButtonText: 'انصراف',
      customClass: {
        confirmButton: 'btn btn-success fs-12 round-0 mr-5',
        cancelButton: 'btn btn-danger fs-12'
      },
      buttonsStyling: false,
      reverseButtons: true,
    }).then(result =>{
      if (result.isConfirmed) {
        const model = {
          id : item.id
        }
      this.formservice.DeleteForm(model).subscribe({
        next :()=>{
          this.toastservice.success('حذف شد');
          this.loadforms()
        }
      })
      }
    })

  }

}
