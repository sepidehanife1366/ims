import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleformComponent } from './titleform.component';

describe('TitleformComponent', () => {
  let component: TitleformComponent;
  let fixture: ComponentFixture<TitleformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TitleformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TitleformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
