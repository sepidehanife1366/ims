import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-titleform',
  templateUrl: './titleform.component.html',
  styleUrls: ['./titleform.component.scss']
})
export class TitleformComponent implements OnInit {
  toppings = new FormControl('');
  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Output() configuration = new EventEmitter()
  @Output() stepper_configuration = new EventEmitter()

  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }

  selectedtitle: any;

  titles = [
    { id: 1, name: 'H1'  , size : '1.2rem'},
    { id: 2, name: 'H2' ,  size : '1rem' },
    { id: 3, name: 'H3' , size : '0.9rem' },
    { id: 4, name: 'H4' , size : '0.8rem'},
    { id: 5, name: 'H5' , size : '0.7rem'},
    { id: 6, name: 'H6' , size : '0.6rem'},

  ];
  titlevalue : any
  constructor(
    private formbuild: FormBuilder,
  ) { }

  ngOnChanges() {
    this.initialize();
    if (this.item.type === 'TitleField' && this.item.configuration != undefined) {
      this.formsetValue(this.item)
    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.type.setValue(item.configuration.type);
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag

  }

  ngOnInit(): void {
  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['تیتر', Validators.required],
      type: [1, Validators.required],
    })
  }
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }

  submit(item: any) {

    item.configuration ={
      title: this.appform.controls['title'].value,
      type: this.appform.controls['type'].value,
      restrictedAccess: this.restrictedAccess,

    }

    item.index = 2
    item.isvalid = null        
    this.configuration.emit(item)
    this.item.isOpen = false

  }
  typechage(e : any , titles : any){
    this.titlevalue =  titles.find((x : any)=> x.id === +e.target.value)
  }
}
