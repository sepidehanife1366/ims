import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { FormType } from './formType';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { FormService } from 'src/app/core/services/form.service';

@Component({
  selector: 'app-updateform',
  templateUrl: './updateform.component.html',
  styleUrls: ['./updateform.component.scss'],
})
export class UpdateformComponent implements OnInit {
  formtype = new FormType();
  appform : FormGroup | any
  moveItems: any = [];
  _isEdit = false;
  title = {
    add: 'ایجاد فرم جدید',
    isEdit: 'ویرایش فرم',
  };
  configurations : any = []
  submited = false;
  content : any;
  public id: Guid | any;
  constructor(
    private formservice : FormService,
    private formbuild : FormBuilder,
    private toastservice : ToastrService ,
    private router : Router
  ){

  }

  ngOnInit(): void {
    this.initialize();
    this.getstatus();
  }

  initialize(){
    this.appform = this.formbuild.group({
      id : [null],
      title : ['' , Validators.required],

    })
  }

  getstatus() {
    this._isEdit = this.getIsEdit();
    if (this._isEdit) {
      this.getform();
    }
    else{
      this.moveItems = []
    }
  }

  getIsEdit() {
    if (JSON.parse(localStorage.getItem('%Up%form') as any).status === 0) {
      return false;
    }
    return true;
  }

  drop(event: CdkDragDrop<any>) {
    if (event.previousContainer === event.container) {
      this.moveItem(event);
    } else {
      this.transferItem(event);
      this.getformtypes();
    }
  }

  getformtypes() {
    this.formtype = new FormType();
  }

  open(item: any) {
    item.isOpen = true;
  }

  remove(item: any) {    
    this.moveItems = this.moveItems.filter((r: any) => {
      return r !== item;
    });
    this.configurations = this.moveItems

  }

  moveItem(event: any) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }

  transferItem(event: any) {
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
    this.add_GUId(event.container.data);

  }
  add_GUId(e : any){
    e.forEach((el : any) =>{
      this.id = Guid.create()
      el.id = this.id.value
    })    
    
  }

  configuration(e : any){

  this.configurations =  this.configurations.filter(function(item : any) {
    return item.index !== e.index && item.index != undefined
})
  this.configurations.push(e)     
  }
  validateClass(controlname : any){
    return controlname.invalid && this.submited;
   }
  submit(){
    this.submited = true
    if (this.configurations.length ===0) {
      this.toastservice.warning('فرم ذخیره نشده است')
    }
    if (!this.appform.valid || this.configurations.length ===0) {
      return;
    }    
    const content = JSON.stringify(this.configurations)
    const model = {
        entity: {
          id: this.appform.controls['id'].value,
          name: this.appform.controls['title'].value,
          content: content,
          description: ""
        }
      
    }
    
    this.formservice.CreateOrUpdateform(model).subscribe({
      next :(response)=>{
        this.router.navigate(['/forms']);
        this.configurations =[]

      }
    })

  }

  getform(){
    const id = JSON.parse(localStorage.getItem('%Up%form') as any).id;
    this.formservice.GetFormById(id).subscribe({
      next : (response)=>{
        this.appform.controls['title'].setValue(response[0].name)
        this.appform.controls['id'].setValue(response[0].id);
        this.content = JSON.parse(response[0].content.replace(/'/g, '"'))         
        this.moveItems = this.content 
        this.configurations = this.content
        
      }
    })
  }
}
