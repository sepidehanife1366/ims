import { Component, EventEmitter, Input, OnInit, Output, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-longtextform',
  templateUrl: './longtextform.component.html',
  styleUrls: ['./longtextform.component.scss']
})
export class LongtextformComponent implements OnInit, OnChanges {

  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Input() valid: boolean | undefined;
  @Output() configuration = new EventEmitter()

  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }
  titles = [
    { id: 1, name: 'فیلد متن طولانی' },
    { id: 2, name: 'tinymce' },
    { id: 3, name: 'quill' },
  ];
  submitted = false
  constructor(
    private formbuild: FormBuilder,
  ) { }

  ngOnChanges() {
    this.initialize();
    if (this.item.type === 'longTextField' && this.item.configuration != undefined) {
      this.formsetValue(this.item)

    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.helpText.setValue(item.configuration.helpText);
    this.appform?.controls?.required.setValue(item.configuration.required);
    this.appform?.controls?.value.setValue(item.configuration.value);
    this.appform?.controls?.maxlength.setValue(item.configuration.maxlength);
    this.appform?.controls?.lines.setValue(item.configuration.lines);
    this.appform?.controls?.type.setValue(item.configuration.type);
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag
    
  }

  ngOnInit(): void {
  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['فرم متن طولانی', Validators.required],
      helpText: [''],
      required: [false],
      value: [''],
      type: [''],
      maxlength: [''],
      lines: [''],
    })
  }
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null  && this.submitted) {
      return true
    }
    return false;

}

  submit(item: any) {
    this.submitted = true;
    if (!this.appform.valid) {
      return;
    }
    item.configuration={
      helpText: this.appform.controls['helpText'].value,
      required: this.appform.controls['required'].value,
      restrictedAccess: this.restrictedAccess,
      title: this.appform.controls['title'].value,
      type: this.appform.controls['type'].value,
      value: this.appform.controls['value'].value,
      maxlength: this.appform.controls['maxlength'].value,
      lines: this.appform.controls['lines'].value

    }
    item.index = 8
    item.isvalid = null        
    this.configuration.emit(item)
    this.item.isOpen = false

  }

  maxlength(e : any){
    if (e.value != '') {
      this.appform.controls['value'].setValue(this.appform.controls['value'].value.slice(0, +e.value))  
    }

  }
  textvalue(e : any , item : any){
    item.configuration.value=e.value
    if (e.value != '') {
      item.isvalid = true
    }else{
      item.isvalid= false
    }

    this.configuration.emit(item)
  }

  
}
