import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LongtextformComponent } from './longtextform.component';

describe('LongtextformComponent', () => {
  let component: LongtextformComponent;
  let fixture: ComponentFixture<LongtextformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LongtextformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LongtextformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
