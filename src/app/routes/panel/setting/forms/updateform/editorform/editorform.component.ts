import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef } from '@angular/core';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-editorform',
  templateUrl: './editorform.component.html',
  styleUrls: ['./editorform.component.scss']
})
export class EditorformComponent implements OnInit {
  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Output() configuration = new EventEmitter();
  @ViewChild('editor') editor: ElementRef<any> | any

  editorData = ''
  model = {
    editorData: ''
  }
  appform: FormGroup | any
  _model ={
      configuration: {
        content: ''
      },
  }

  public Editor = ClassicEditor;
  constructor(
    private formbuild: FormBuilder,

  ) { }

  ngOnChanges() {
    this.initialize();
    if (this.item.type === 'EditorField' && this.item.configuration != undefined) {
      this.formsetValue(this.item)
    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.model.editorData = item.configuration.content
  }

  ngOnInit(): void {

  }
  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
    })
  }
  close() {
    this.item.isOpen = false
  }

  submit(item: any) {
    const model = {
      configuration: {
        content: this.model.editorData
      },
      icon: item.icon,
      id: this.appform?.controls?.id.value,
      type: item.type,
      text: 'فرم ویرایشگر',
      index : 10,

    }
    this.configuration.emit(model)
  }
  changevalue(item : any){
    const model = {
      configuration: {
        content: this.model.editorData
      },
      icon: item.icon,
      id: this.appform?.controls?.id.value,
      type: item.type,
      text: 'فرم ویرایشگر',
      index : 10,
      isvalid : true

    }
    this.configuration.emit(model)

  }

}
