import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateformComponent } from './dateform.component';

describe('DateformComponent', () => {
  let component: DateformComponent;
  let fixture: ComponentFixture<DateformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
