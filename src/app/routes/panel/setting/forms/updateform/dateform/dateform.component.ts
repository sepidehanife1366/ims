import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'jalali-moment';

@Component({
  selector: 'app-dateform',
  templateUrl: './dateform.component.html',
  styleUrls: ['./dateform.component.scss']
})
export class DateformComponent implements OnInit {

  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Input() valid: boolean | undefined;
  @Output() configuration = new EventEmitter()

  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }
  selectedDate: any
  submitted = false

  constructor(
    private formbuild: FormBuilder,
  ) { }
  ngOnChanges() {
    this.initialize();
    if (this.item.configuration != undefined && this.item.type === 'DateField') {
      this.formsetValue(this.item)
    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.helpText.setValue(item.configuration.helpText);
    this.appform?.controls?.switchMode.setValue(item.configuration.switchMode);
    this.appform?.controls?.required.setValue(item.configuration.required);
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag
    
  }

  ngOnInit(): void {
    this.item.isvalid = null
  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['فرم تاریخ', Validators.required],
      helpText: [''],
      switchMode: [false],
      required: [false],
      date: [''],
    })
  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null  && this.submitted) {
      return true
    }
    return false;

}
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }

  submit(item: any) {
    this.submitted = true;
    if (!this.appform.valid) {
      return;
    }
    item.configuration = {
      helpText: this.appform.controls['helpText'].value,
      required: this.appform.controls['required'].value,
      restrictedAccess: this.restrictedAccess,
      switchMode: this.appform.controls['switchMode'].value,
      title: this.appform.controls['title'].value,
      date: moment(this.selectedDate).locale('en').format('YYYY-MM-DD')
    }
    item.index = 1
    item.isvalid = null   
    this.configuration.emit(item)
    this.item.isOpen = false
  }
  DATE_VALUE(e: any, item: any) {
    if (e != undefined) {
      if (this.selectedDate != '' || this.selectedDate != undefined) {
        item.isvalid = true
        this.configuration.emit(item)
      }
      else{
        item.isvalid = false
        this.configuration.emit(item)
      }
    }
    this.appform?.controls?.date.setValue(moment(this.selectedDate))
  }

}
