import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-singlechoiceform',
  templateUrl: './singlechoiceform.component.html',
  styleUrls: ['./singlechoiceform.component.scss']
})
export class SinglechoiceformComponent implements OnInit {

  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Input() valid: boolean | undefined;

  noCheckbox = false
  @Output() configuration = new EventEmitter()

  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }
  selectedcheckbox: any = [{
    checked: false,
    id: null,
    name: "گزینه-1",
    value: "مقدار-1"
  }];
  choicindex = 1;
  titlevalue = ''
  btnselected : any
  submitted = false

  constructor(
    private formbuild: FormBuilder,
  ) { }

  ngOnChanges() {
    this.initialize();
    if (this.item.type === 'singleChoice' && this.item.configuration != undefined) {
      this.formsetValue(this.item)

    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.helpText.setValue(item.configuration.helpText);
    this.appform?.controls?.switchMode.setValue(item.configuration.switchMode);
    this.appform?.controls?.required.setValue(item.configuration.required);
    this.selectedcheckbox = item.configuration.radiobuttonList;
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag
    
  }

  ngOnInit(): void {
  }
  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['فرم تک انتخابی', Validators.required],
      helpText: [''],
      switchMode: [false],
      required: [false],
    })
  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null  && this.submitted) {
      return true
    }
    return false;

}
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }
  addtolist() {
    this.choicindex = this.choicindex + 1
    this.selectedcheckbox.push({
      checked: false,
      id: null,
      name: "گزینه-" + (this.choicindex),
      value: "مقدار-" + (this.choicindex)
    })

  }
  submit(item: any) {
    this.submitted = true;
    if (!this.appform.valid) {
      return;
    }
    item.configuration = {
      radiobuttonList: this.selectedcheckbox,
      helpText: this.appform.controls['helpText'].value,
      inline: false,
      otherOption: {},
      required: false,
      restrictedAccess: this.restrictedAccess,
      switchMode: this.appform.controls['switchMode'].value,
      title: this.appform.controls['title'].value
    }
    item.index = 5
    item.isvalid = null
    this.configuration.emit(item)
    this.item.isOpen = false
  }
  singleselect(e: any, selectedcheckbox: any, index: any) {
    selectedcheckbox.forEach((element: any) => {
      element.checked = false
    });
    selectedcheckbox[index].checked = true;
  }
  changeValue(e : any , item : any , select : any){ 
    this.selectedcheckbox.forEach((el : any) => el.checked = false)
    select.checked=true
    const valid = this.selectedcheckbox.some((s : any)=> s.checked === true);
    item.isvalid = valid 
    this.configuration.emit(item)
  }

}
