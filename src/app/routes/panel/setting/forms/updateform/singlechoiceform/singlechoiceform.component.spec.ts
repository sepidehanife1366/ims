import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglechoiceformComponent } from './singlechoiceform.component';

describe('SinglechoiceformComponent', () => {
  let component: SinglechoiceformComponent;
  let fixture: ComponentFixture<SinglechoiceformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinglechoiceformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglechoiceformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
