import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescribtionformComponent } from './describtionform.component';

describe('DescribtionformComponent', () => {
  let component: DescribtionformComponent;
  let fixture: ComponentFixture<DescribtionformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescribtionformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescribtionformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
