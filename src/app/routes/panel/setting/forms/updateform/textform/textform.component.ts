import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-textform',
  templateUrl: './textform.component.html',
  styleUrls: ['./textform.component.scss']
})
export class TextformComponent implements OnInit {

  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Input() valid: boolean | undefined;
  @Output() configuration = new EventEmitter()
  @Output() stepper_configuration = new EventEmitter()

  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }
  titlevalue = '';
  _model : any ={
    configuration: {
      helpText: '',
      required: '',
      restrictedAccess: null,
      title: '',
      value: '',
    },
    icon: null,
    id: null,
    type: null,
    text: '',
    index : null,
    isvalid : false
  }
  submitted= false
  constructor(
    private formbuild: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.initialize();
    if (this.item.type === 'TextField' && this.item.configuration != undefined) {
      this.formsetValue(this.item)
    }
  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['فرم متنی', Validators.required ],
      helpText: [''],
      switchMode: [false],
      required: [false],
      value: [''],
      maxlength: [''],
    })
  }

  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.helpText.setValue(item.configuration.helpText);
    this.appform?.controls?.required.setValue(item.configuration.required);
    this.appform?.controls?.value.setValue(item.configuration.value);
    this.appform?.controls?.maxlength.setValue(item.configuration.maxlength);
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag;
  }
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null  && this.submitted) {
      return true
    }
    return false;

}
  submit(item: any) {
    this.submitted = true;
    if (!this.appform.valid) {
      return;
    }
    item.configuration ={
      helpText: this.appform.controls['helpText'].value,
      required: this.appform.controls['required'].value,
      restrictedAccess: this.restrictedAccess,
      switchMode: this.appform.controls['switchMode'].value,
      title: this.appform.controls['title'].value,
      value: this.appform.controls['value'].value,
      maxlength: this.appform.controls['maxlength'].value,
    }

    item.index = 7
    item.isvalid = null        
    this.configuration.emit(item)
    this.item.isOpen = false
  }

  maxlength(e : any){
    if (e.value != '') {
      this.appform.controls['value'].setValue(this.appform.controls['value'].value.slice(0, +e.value))  
    }

  }

  changetext(e : any , item : any){
    item.configuration.value =e.value
    if (e.value != '') {
      item.isvalid = true
    }  
    else{
      item.isvalid = true
    }

    this.configuration.emit(item)

  }

}
