import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-multiplechoiceform',
  templateUrl: './multiplechoiceform.component.html',
  styleUrls: ['./multiplechoiceform.component.scss']
})
export class MultiplechoiceformComponent implements OnInit, OnChanges {
  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Input() valid: boolean | undefined;

  noCheckbox = false
  @Output() configuration = new EventEmitter()
  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }
  selectedcheckbox: any = [{
    checked: false,
    id: null,
    name: "گزینه-1",
    value: "مقدار-1"
  }];
  addIndex = 1;
  _model : any
  submitted = false

  constructor(
    private formbuild: FormBuilder,
  ) {

  }
  ngOnChanges(changes: SimpleChanges) { 
    
    this.initialize();
    if (this.item.type === 'multipleChoice' && this.item.configuration != undefined) {
      this.formsetValue(this.item)      
    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.helpText.setValue(item.configuration.helpText);
    this.appform?.controls?.switchMode.setValue(item.configuration.switchMode);
    this.appform?.controls?.required.setValue(item.configuration.required);
    this.selectedcheckbox = item.configuration.checkboxList;
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag;
    
  }

  ngOnInit(): void {
  }
  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['فرم چند انتخابی', Validators.required],
      helpText: [''],
      switchMode: [false],
      required: [false],
    })
  }
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null  && this.submitted) {
      return true
    }
    return false;

}
  addtolist() {
    this.addIndex = this.addIndex + 1
    this.selectedcheckbox.push({
      checked: false,
      id: null,
      name: "گزینه-" + (this.addIndex),
      value: "مقدار-" + (this.addIndex)
    })
  }

  submit(item: any) {
    this.submitted = true;
    if (!this.appform.valid) {
      return;
    }
    item.configuration = 
    {
      checkboxList: this.selectedcheckbox,
      helpText: this.appform.controls['helpText'].value,
      inline: false,
      otherOption: {},
      required: this.appform.controls['required'].value,
      restrictedAccess: this.restrictedAccess,
      switchMode: this.appform.controls['switchMode'].value,
      title: this.appform.controls['title'].value
    }
    item.index = 0
    item.isvalid = null        
    this.configuration.emit(item)
    this.item.isOpen = false
  }
  changeChecked(e : any , item : any){
   const valid = this.selectedcheckbox.some((s : any)=> s.checked === true);
   item.isvalid = valid
   this.configuration.emit(item)
}

}


