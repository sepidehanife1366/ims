import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplechoiceformComponent } from './multiplechoiceform.component';

describe('MultiplechoiceformComponent', () => {
  let component: MultiplechoiceformComponent;
  let fixture: ComponentFixture<MultiplechoiceformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiplechoiceformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplechoiceformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
