export class FormType {
constructor(
   public formtypelist = [
        {
        type : "multipleChoice",
        text : 'چند انتخابی',
        id : '' ,
        icon : '<i class="fa fa-list fs-12">'
      } ,
      {
        type : 'DateField',
        text : 'فیلد تاریخ',
        id : '',
        icon : '<i class="far fa-calendar-alt"></i>'
    
      },
      {
        type : 'TitleField',
        text : 'تیتر',
        id : '',
        icon : '<i class="fas fa-heading"></i>'
      },
      {
        type : 'NumberField',
        text : 'فیلد عدد',
        id : '',
        icon : '<i class="fas fa-hashtag"></i>'
      },
      {
        type : 'DescribtionField',
        text : 'متن توضیحات',
        id : '',
        icon : '<i class="fas fa-paragraph"></i>'
      }
      ,
      { 
        type : "singleChoice",
        text : 'تک انتخابی',
        id : '',
        icon : '<i class="far fa-outdent"></i>'
      },
      {
        type : 'selectField',
        text : 'لیست کشویی',
        id : '',
        icon : '<i class="fal fa-th-list"></i>'
      },
      {
        type : 'TextField',
        text : 'فیلد متن',
        id : '',
        icon : '<i class="fas fa-text"></i>'
      },
      {
        type : "longTextField",
        text : 'فیلد متن طولانی',
        id : '',
        icon : '<i class="fal fa-file-alt"></i>'
      },
      {
        type : 'EditorField',
        text : 'ویرایشگر',
        id : '',
        icon : '<i class="fas fa-text-width"></i>'
      },
      {
        type : 'FormField',
        text : 'فرم',
        id : '',
        icon : '<i class="fas fa-tasks-alt"></i>'
      }
    ]
){

}
    
}