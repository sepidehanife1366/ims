import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-formfield',
  templateUrl: './formfield.component.html',
  styleUrls: ['./formfield.component.scss']
})
export class FormfieldComponent implements OnInit {
  toppings = new FormControl('');
  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Output() configuration = new EventEmitter()


  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }
  _model : any ={
    configuration: {
      helpText: '',
      required: '',
      restrictedAccess: null,
      title: '',
      value: '',
    },
    icon: null,
    id: null,
    type: null,
    text: '',
    index : null,
  }

  constructor(
    private formbuild: FormBuilder,
  ) { }

  ngOnChanges() {
    this.initialize();
    if (this.item.type === 'FormField' && this.item.configuration != undefined) {
      this.formsetValue(this.item)

    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.value.setValue(item.configuration.value);
    this.appform?.controls?.helpText.setValue(item.configuration.helpText);
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag
  }

  ngOnInit(): void {
  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['فیلد فرم', Validators.required],
      helpText: ['', Validators.required],
      value: ['', Validators.required],
    })
  }
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }

  submit(item: any) {
    item.configuration ={
      title: this.appform.controls['title'].value,
      value: this.appform.controls['value'].value,
      helpText: this.appform.controls['helpText'].value,
      restrictedAccess: this.restrictedAccess,

    }
    item.index = 9
    item.isvalid = null        
    this.configuration.emit(item)
    this.item.isOpen = false

  }
  changetext(e : any , item : any){
    item.configuration.value = e.value
    item.isvalid = e.value != '' ? true : false 
    this.configuration.emit(item)
  }
}
