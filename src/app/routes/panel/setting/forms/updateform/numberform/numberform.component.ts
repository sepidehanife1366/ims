import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-numberform',
  templateUrl: './numberform.component.html',
  styleUrls: ['./numberform.component.scss']
})
export class NumberformComponent implements OnInit {
  @Input() item: any;
  @Input() closeMode: any;
  @Input() hidden: any;
  @Input() valid: boolean | undefined;

  @Output() configuration = new EventEmitter()
  appform: FormGroup | any
  _restrictedAccess = false;
  restrictedAccess = {
    accessTo: [{
      checked: false,
      id: 1,
      title: "adminstrator"
    }],
    flag: false
  }

  titles = [
    { id: 1, name: 'H1' },
    { id: 2, name: 'H2' },
    { id: 3, name: 'H3' },
    { id: 4, name: 'H4' },
    { id: 5, name: 'H5' },
    { id: 6, name: 'H6' },

  ];
  submitted = false
  constructor(
    private formbuild: FormBuilder,
  ) { }

  ngOnChanges() {
    this.initialize();
    if (this.item.type === 'NumberField' && this.item.configuration != undefined) {
      this.formsetValue(this.item)      
    }
  }
  formsetValue(item: any) {
    this.appform?.controls?.id.setValue(item.id);
    this.appform?.controls?.title.setValue(item.configuration.title);
    this.appform?.controls?.helpText.setValue(item.configuration.helpText);
    this.appform?.controls?.required.setValue(item.configuration.required);
    this.appform?.controls?.value.setValue(item.configuration.value);
    this.appform?.controls?.min.setValue(item.configuration.min);
    this.appform?.controls?.max.setValue(item.configuration.max);
    this.appform?.controls?.step.setValue(item.configuration.step);
    this.restrictedAccess = item.configuration.restrictedAccess;
    this._restrictedAccess = item.configuration.restrictedAccess.flag
  }

  ngOnInit(): void {
  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      title: ['فرم عدد', Validators.required],
      helpText: [''],
      required: [false],
      value: [''],
      max: ['', Validators.required],
      min: ['', Validators.required],
      step: [''],

    })
  }
  validateClass(controlname: any) {
    if (this.appform?.controls[controlname].errors != null  && this.submitted) {
      return true
    }
    return false;

}
  toggle(e: any) {
    this._restrictedAccess = e.checked
  }

  submit(item: any) {
    this.submitted = true;
    if (!this.appform.valid) {
      return;
    }
    item.configuration = {
      helpText: this.appform.controls['helpText'].value,
      required: this.appform.controls['required'].value,
      restrictedAccess: this.restrictedAccess,
      title: this.appform.controls['title'].value,
      value: this.appform.controls['value'].value,
      min: this.appform.controls['min'].value,
      max: this.appform.controls['max'].value,
      step: this.appform.controls['step'].value,
    }
    item.index = 3
    this.item.isvalid = undefined
    this.configuration.emit(item)
    this.item.isOpen = false
  }

  valueChange(e: any) {
    const number = Number(e.value)
    if (e.value != '') {
      if (this.appform.controls['min'].value !='' && +this.appform.controls['min'].value > number) {
        this.appform.controls['value'].setValue(this.appform.controls['min'].value)
      }
      else if (this.appform.controls['max'].value !='' && number > +this.appform.controls['max'].value) {
        this.appform.controls['value'].setValue(this.appform.controls['max'].value)
      }
      else {
        this.appform.controls['value'].setValue(e.value)
      }
  
    }
      
    }
    changetext(e : any , item : any){
      const number = Number(e.value)
      item.configuration.value = e.value;
      if ( e.value != '' &&  +item.configuration.min <= number && number <= +item.configuration.max ) {
        item.isvalid = true
      }
      else{
        item.isvalid = false
      }
      this.configuration.emit(item)
    }
    rangeValue(e : any){
      this.appform.controls['value'].setValue('')
    }


}
