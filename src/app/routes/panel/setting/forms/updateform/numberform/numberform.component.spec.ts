import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberformComponent } from './numberform.component';

describe('NumberformComponent', () => {
  let component: NumberformComponent;
  let fixture: ComponentFixture<NumberformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NumberformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
