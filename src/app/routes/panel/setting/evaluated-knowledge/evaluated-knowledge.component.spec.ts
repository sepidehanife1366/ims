import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluatedKnowledgeComponent } from './evaluated-knowledge.component';

describe('EvaluatedKnowledgeComponent', () => {
  let component: EvaluatedKnowledgeComponent;
  let fixture: ComponentFixture<EvaluatedKnowledgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluatedKnowledgeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluatedKnowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
