import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PackageService } from 'src/app/core/services/package.service';
import { evaluated } from './model';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-evaluated-knowledge',
  templateUrl: './evaluated-knowledge.component.html',
  styleUrls: ['./evaluated-knowledge.component.scss']
})
export class EvaluatedKnowledgeComponent implements OnInit {
  appform: FormGroup | any;
  packages: any;
  eveluatedtype = [
    { id: null, name: '...' },
    { id: 0, name: 'پاسخ به پرسش های مرتبط با نوع دانش' },
    { id: 1, name: 'پاسخ به پرسش های مرتبط با نوع دانش به اضافه مرتبط با حوزه خبرگی' },
    { id: 2, name: 'پاسخ به پرسش های مرتبط با نوع دانش به اضافه مرتبط با حوزه عضویت' },

  ]
  managergroups = [
    { id: null, name: '...' },
    { id: 0, name: 'این نوع دانش نیازی به ارزیابی ندارد' },
    { id: 1, name: 'مسئول دانش' },
    { id: 2, name: 'خبره های موضوعات مرتبط' },
    { id: 3, name: 'مدیران  گروهای مرتبط' },
    { id: 4, name: 'اعضای  گروهای مرتبط' },

  ]
  choosenType = [
    { id: null, name: '...' },
    { id: 0, name: 'موضوعاتی به صورت ثابت' },
    { id: 1, name: 'فقط یک موضوع' },
    { id: 2, name: 'انتخاب آزاد ازمیان موضوعات' },
  ]
  searchable = [
    { id: null, name: '...' },
    { id: 0, name: 'ثبت دانش' },
    { id: 1, name: 'تایید توسط مسئول دانش' },
    { id: 2, name: 'ارزیابی' },
  ]

  disabled = true;
  modalRef?: BsModalRef;
  EvaluationSettingId = null;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-lg'
  };


  constructor(
    private packageservice: PackageService,
    private formbuild: FormBuilder,
    private toastservice: ToastrService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.getPackageTree();
    this.initialize();
  }

  initialize() {
    this.appform = this.formbuild.group({
      approveScore: [0, Validators.required],
      automate_Evaluator_To_Expert: [null, Validators.required],
      evaluate_By_Registerer: [null, Validators.required],
      evaluationType: [null, Validators.required],
      evaluators: [null, Validators.required],
      fill_Required_Fieldes: [null, Validators.required],
      id: [null, Validators.required],
      limitedTo: null,
      packageId: [null],
      relatedTopicDecision: [null, Validators.required],
      requieredAttachment: [false],
      requieredKeyWord: [false],
      requieredSummary: [false],
      requiered_Description: [false,],
      requiered_Encyclopedia_Paragraph: [false],
      requiered_Related_Topic: [false],
      scoringScale: [0, Validators.required],
      searchableType: [null, Validators.required],
    })
  }

  getPackageTree() {
    this.packageservice.GetPackageTree().subscribe({
      next: (response: any) => {
        this.packages = {
          tree: response,
          topicMode: false
        };
      },
      error: (err) => {

      }
    })
  }
  GETITEM(e: any) {
    this.disabled = false;
    this.GetEvaluationSetting(e.item.id)
  }

  GetEvaluationSetting(id: any) {
    this.packageservice.GetEvaluationSetting(id).subscribe({
      next: (response: evaluated) => {
        this.setformValue(response)
      }
    })
  }
  setformValue(data: any) {
    this.EvaluationSettingId = data.id
    this.appform.controls['id'].setValue(data.id);
    this.appform.controls['approveScore'].setValue(data.approveScore);
    this.appform.controls['automate_Evaluator_To_Expert'].setValue(data.automate_Evaluator_To_Expert);
    this.appform.controls['evaluate_By_Registerer'].setValue(data.evaluate_By_Registerer);
    this.appform.controls['evaluationType'].setValue(data.evaluationType);
    this.appform.controls['evaluators'].setValue(data.evaluators);
    this.appform.controls['fill_Required_Fieldes'].setValue(data.fill_Required_Fieldes);
    this.appform.controls['packageId'].setValue(data.packageId);
    this.appform.controls['relatedTopicDecision'].setValue(Number(data.relatedTopicDecision));
    this.appform.controls['requieredAttachment'].setValue(data.requieredAttachment);
    this.appform.controls['requieredKeyWord'].setValue(data.requieredKeyWord);
    this.appform.controls['requieredSummary'].setValue(data.requieredSummary);
    this.appform.controls['requiered_Description'].setValue(data.requiered_Description);
    this.appform.controls['requiered_Encyclopedia_Paragraph'].setValue(data.requiered_Encyclopedia_Paragraph);
    this.appform.controls['requiered_Related_Topic'].setValue(data.requiered_Related_Topic);
    this.appform.controls['scoringScale'].setValue(data.scoringScale);
    this.appform.controls['searchableType'].setValue(data.searchableType);
  }
  submit() {
    if (!this.appform.valid) {
      return;
    }
    this.appform.controls['relatedTopicDecision'].setValue(Number(this.appform.controls['relatedTopicDecision'].value))
    const model = { entity: this.appform.value }
    this.packageservice.UpdateEvaluationSetting(model).subscribe({
      next: (response) => {
        if (response.resultCode === 0) {
          this.toastservice.success('با موفقیت ذخیره شد')
        }
        else {
          this.toastservice.error('ناموفق')
        }

      },
      error: (err) => {
        this.toastservice.error('ناموفق')
      }
    })

  }
  alert() {
    if (this.disabled) {
      this.toastservice.warning('بسته مورد نظر را انتخاب نمایید')
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

}
