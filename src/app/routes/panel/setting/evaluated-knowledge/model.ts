export interface evaluated {
    approveScore: number;
    automate_Evaluator_To_Expert: boolean;
    evaluate_By_Registerer: boolean;
    evaluationType: number;
    evaluators: number;
    fill_Required_Fieldes: boolean;
    id: number;
    limitedTo: null;
    packageId: number;
    relatedTopicDecision: number;
    requieredAttachment: boolean;
    requieredKeyWord: boolean;
    requieredSummary: boolean;
    requiered_Description: boolean;
    requiered_Encyclopedia_Paragraph: boolean;
    requiered_Related_Topic: boolean;
    scoringScale: number;
    searchableType: number;

}