import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EveluatedquastionComponent } from './eveluatedquastion.component';

describe('EveluatedquastionComponent', () => {
  let component: EveluatedquastionComponent;
  let fixture: ComponentFixture<EveluatedquastionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EveluatedquastionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EveluatedquastionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
