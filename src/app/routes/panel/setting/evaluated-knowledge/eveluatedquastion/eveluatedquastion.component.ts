import { Component, Input, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PackageService } from 'src/app/core/services/package.service';

@Component({
  selector: 'app-eveluatedquastion',
  templateUrl: './eveluatedquastion.component.html',
  styleUrls: ['./eveluatedquastion.component.scss']
})
export class EveluatedquastionComponent implements OnInit, OnChanges {
  appform: FormGroup | any;
  @Input() EvaluationSettingId: any;
  questions : any =[]

  type=[
    {id : null , name : '...'},
    {id : 0 , name : 'تشریحی'},
    {id : 1 , name : 'امتیازی'},
  ]
  contact =[
    {id : null , name : '...'},
    {id : 0 , name : 'عمومی'},
    {id : 1 , name : 'اعضا'},
    {id : 2 , name : 'خبره ها'},
  ]
  @Output() CLOSE = new EventEmitter<any>()

  constructor(
    private packageservice: PackageService,
    private formbuild: FormBuilder,
  ) { }

  ngOnChanges(): void {
    this.initialize();
    if (this.EvaluationSettingId != undefined) {
      this.loadEvaluationquastions(this.EvaluationSettingId)
    }
  }

  ngOnInit(): void {

  }

  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      contact: [null, Validators.required],
      type: [null, Validators.required],
      question: [null, Validators.required],
      priority: [null, Validators.required],
      evaluationSettingId : [null]

    })
  }

  loadEvaluationquastions(id: any) {
    this.packageservice.GetEvaluationQuestions(id).subscribe({
      next: (response) => {
        this.questions = response;
      }
    })

  }
  submit(){
    if (!this.appform.valid) {
      return;
    }
    const model ={entity : {
      contact : Number(this.appform.controls['contact'].value),
      id : this.appform.controls['id'].value ,
      priority : this.appform.controls['priority'].value,
      question :this.appform.controls['question'].value ,
      type : Number(this.appform.controls['type'].value),
      evaluationSettingId : this.EvaluationSettingId
    }}
    this.packageservice.UpsertEvaluationQuestion(model).subscribe({
      next : (reponse) =>{
        this.loadEvaluationquastions(this.EvaluationSettingId) 
        this.initialize()       
      }
    })

  }
  update(item : any){    
    this.appform.controls['id'].setValue(item.id);
    this.appform.controls['contact'].setValue(item.contact);
    this.appform.controls['priority'].setValue(item.priority);
    this.appform.controls['question'].setValue(item.question);
    this.appform.controls['type'].setValue(item.type);
    this.appform.controls['evaluationSettingId'].setValue(item.evaluationSettingId);

  }
  remove(item : any){
    const model = {
      evaluationQuestionId : item.id
    }
    this.packageservice.DeleteEvaluationQuestion(model).subscribe({
      next : (response)=>{
        this.loadEvaluationquastions(this.EvaluationSettingId)   
      }
    })

  }
  close(){
    this.CLOSE.emit(true);
  }

}
