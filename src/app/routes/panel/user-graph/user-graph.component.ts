import {
  Component,
  OnDestroy,
  OnInit,
  Inject,
  NgZone,
  PLATFORM_ID,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription, debounceTime, distinctUntilChanged } from 'rxjs';
import { SearchService } from 'src/app/core/services/search.service';
import { isPlatformBrowser } from '@angular/common';

// amCharts imports
import * as am5 from '@amcharts/amcharts5';
import * as am5hierarchy from '@amcharts/amcharts5/hierarchy';
import am5themes_Animated from '@amcharts/amcharts5/themes/Animated';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-graph',
  templateUrl: './user-graph.component.html',
  styleUrls: ['./user-graph.component.scss'],
})
export class UserGraphComponent implements OnInit, OnDestroy {
  userForm!: FormGroup;
  userList: any[] = [];
  userGraph?: any;
  private colors: readonly string[] = [
    '#17a2b8', //center
    '#a0dc67', //members
    '#dcd267', //managers
    '#dc8c67', //topics
    '#dc6788', //registerd
    '#dc67ce', //groups
    '#a367dc', //experts
    '#6771dc'  //colleagues
  ];
  private readonly _subscription: Subscription = new Subscription();

  private root!: am5.Root;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _searchSrv: SearchService,
    @Inject(PLATFORM_ID) private readonly _platformId: Object,
    private readonly _zone: NgZone,
    private router: Router
  ) { }

  ngOnInit(): void {
    this._initUserForm();
    this._subscribeUsernameInputChanges();
    this._searchUser('');
  }

  private _subscribeUsernameInputChanges(): void {
    this._subscription.add(
      this.userForm.controls['username'].valueChanges
        .pipe(distinctUntilChanged(), debounceTime(500))
        .subscribe((value: string) => {
          this._searchUser(value);
        })
    );
  }

  private _initUserForm(): void {
    this.userForm = this._formBuilder.group({
      username: [null],
    });
  }

  private _searchUser(value: string): void {
    this._searchSrv.getLightUserList(value).subscribe((res: any[]) => {
      this.userList = res;
    });
  }

  getUserGraph(value: any): void {
    this._unsubscribeSubscriptions();
    const user = this.userList.find((item: any) => {
      return item.id === value.id;
    });
    this.userForm.patchValue({ username: user.userName });
    this._subscribeUsernameInputChanges();
    this._getUserGraph(value.id);
  }

  private _getUserGraph(userId: number): void {
    this._searchSrv.getUserGraph(userId).subscribe((res: any) => {
      this.userGraph = res;

      if (this.root) {
        this._browserOnly(() => {
          if (this.root) {
            this.root.dispose();
          }
        });
      }
      this._browserOnly(() => {
        const root = am5.Root.new('chartDiv');
        root._logo?.dispose()
        root.setThemes([am5themes_Animated.new(root)]);
        const data = {
          name: res.data.name,
          value: 0,
          root: 0,
          id : res.data.id,
          graphEntityType: 0,
          nodeSettings: {
            fill: am5.color(this.colors[0]),
          },
          children: [
            {
              name: res.data.name,
              value: 1,
              root: 1,
              id : res.data.id,
              graphEntityType: 1,
              nodeSettings: {
                fill: am5.color(this.colors[0]),
              },
              children: res.children.map((element: any) => {
                return {
                  name: element.data.name,
                  value: element.data.name,
                  id : element.data.id,
                  nodeSettings: {
                    fill: am5.color(this.colors[element.data.graphEntityType]),
                  },
                  children: this._generateGraphData(element.children, element.data.graphEntityType),
                };
              }),
            },
          ],
        };
        // Create wrapper container
        const container = root.container.children.push(
          am5.Container.new(root, {
            width: am5.percent(100),
            height: am5.percent(100),
            layout: root.verticalLayout,
          })
        );
        const series = container.children.push(
          am5hierarchy.ForceDirected.new(root, {
            singleBranchOnly: false,
            downDepth: 1,
            topDepth: 1,
            maxRadius: 60,
            minRadius: 30,
            valueField: 'value',
            categoryField: 'name',
            childDataField: 'children',
            idField: 'name',
            linkWithStrength: 0.3,
            linkWithField: 'linkWith',
            manyBodyStrength: -15,
            centerStrength: 1,
            nodePadding: 10,

          })
        );
        series.links.template.setAll({
          distance: 1,
          strokeWidth: 2,
          stroke: am5.color('#ccc'),
          strokeOpacity: 0.3
        })
        series.circles.template.setAll({
          templateField: 'nodeSettings',
          fill: am5.color('#000'),
          strokeWidth: 3,
        });
        series.nodes.template.events.on("click", (event: any) => {
          debugger;
          if (event.target.dataItem?.dataContext['graphEntityType'] === 1) {
            this.router.navigate(['profile'], { queryParams: { id: event.target.dataItem?.dataContext['id'] } })
          }
          if (event.target.dataItem?.dataContext['graphEntityType'] === 5) {
            this.router.navigate(['subject'], { queryParams: { id: event.target.dataItem?.dataContext['id'] } })
          }
        });

        series.labels.template.setAll({
          fontSize: 18,
          fill: am5.color('#000'),
          textAlign: 'center',
        });

        if (series) {
          series.get('colors')?.set('step', 2);
          series.data.setAll([data]);
          series.set('selectedDataItem', series.dataItems[0]);
          const legend = container.children.push(
            am5.Legend.new(root, {
              nameField: 'name',
              fillField: 'color',
              strokeField: 'color',
              width: am5.percent(100),
              centerX: am5.percent(90),
              fill: am5.color('#000'),
              x: am5.percent(95),
              layout: root.horizontalLayout,
              maxWidth: 100,
            })
          );
          legend.valueLabels.template.setAll({
            fontSize: 10,
            fontWeight: '700',
            fill: am5.color('#000'),
            textAlign: 'end',

          });
          legend.markers.template.setAll({
            marginRight: 0,
          })

          legend.data.setAll([
            {
              name: 'گروه ها',
              value: 'Groups',
              color: am5.color(this.colors[4]),
            },
            {
              name: 'ثبت کننده',
              value: 'Registarer',
              color: am5.color(this.colors[5]),
            },
            {
              name: 'اعضا',
              value: 'Members',
              color: am5.color(this.colors[7]),
            },
            {
              name: 'مدیران',
              value: 'Managers',
              color: am5.color(this.colors[6]),
            },
            {
              name: 'کارشناس',
              value: 'Experts',
              color: am5.color(this.colors[3]),
            },
          ]);
          series.appear(1000, 100);
        }
        this.root = root;
      });
    });
  }

  private _generateGraphData(item: any[], root: number): any[] {
    return item.map((item: any) => {
      return {
        name: item.data.name,
        value: item.data.name,
        id: item.data.id,
        graphEntityType: item.data.graphEntityType,
        nodeSettings: {
          fill: am5.color(this.colors[root]),
        },
        children: this._generateGraphData(item.children, root),
      };
    });
  }

  private _browserOnly(f: () => void) {
    if (isPlatformBrowser(this._platformId)) {
      this._zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  private _unsubscribeSubscriptions(): void {
    if (this._subscription) this._subscription.unsubscribe();
  }

  ngOnDestroy(): void {
    this._unsubscribeSubscriptions();
    this._browserOnly(() => {
      if (this.root) {
        this.root.dispose();
      }
    });
  }
}
