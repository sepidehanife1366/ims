import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, EventEmitter, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { ChatService } from 'src/app/core/services/chat.service';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { UserService } from 'src/app/core/services/user.service';
import { WebSocketService } from 'src/app/core/services/websocket.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  animations: [
    trigger('dialog', [
      state('visible',
        style({
          transform: 'translate(0 , 0) scale(1)',
          opacity: 1
        })),
      state('void,hidden', style({
        transform: 'translate(0, 0) scale(0)',
        opacity: 0
      })),
      transition('*=>vissible', animate('250ms')),
      transition('*=>void , *=>vissible', animate('180ms')),
    ])
  ]
})
export class ChatComponent implements OnInit {
  title = 'همه اتاق ها'
  spaces: any = []
  spaceFormGroup: FormGroup | any;
  roomFormGroup: FormGroup | any;
  state = false;
  isprivate = false
  imgUrl: any = ''
  submitted = false;
  addroom = false;
  room: any
  rooms: any = []
  roomsFilter = {
    page: 0,
    pageCount: 0,
    search: "",
    sort: ""
  }
  @Output() CLOSE = new EventEmitter()
  user: any;
  message: any;
  messages: any = [];
  roomId: any = null;
  spaceId: any
  @ViewChild('popup') popup: any;
  showAll = false;
  allpublics: any[] = [];
  modalRef?: BsModalRef;
  publicroomdialog = {
    isVissible: false
  }
  publicroom: any;
  members: any = []
  main = {
    main: true,
    header: true,
    title: true,
    content: true,
    search: true,
    footer: true,
    addroombtn: false
  }
  space = {
    main: false,
    state: false,
    spaceform: false,
    roomform: false,
    isprivate: false,
    addroom: false,
    memeberlist: false,
  }
  chatroom = {
    room: false
  }
  submittedroom = {
    roomId: '',
    accessType: 0
  }
  users: any = []
  joinedtoroomUsers: any[] = []
  window: any;
  file: any;
  uploadMode = '';
  disabled = false;

  constructor(
    private chatservice: ChatService,
    public elementRef: ElementRef,
    private renderer: Renderer2,
    private formbuild: FormBuilder,
    private upload: CloudStorageService,
    private socketservice: WebSocketService,
    private modalService: BsModalService,
    private autenticationservice: AuthenticationService,
    private userservice: UserService

  ) {
    this.window = window.innerWidth
  }
  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('userConfig') as any);
    this.initialize();
    this.loadRooms(this.roomsFilter);
  }
  init() {
    this.main = {
      main: true,
      header: true,
      title: true,
      content: true,
      search: true,
      footer: true,
      addroombtn: false
    }

    this.space = {
      main: false,
      state: false,
      spaceform: false,
      roomform: false,
      isprivate: false,
      addroom: false,
      memeberlist: false,

    }
    this.chatroom = {
      room: false
    }
  }

  validateClass(controlname: any) {
    if (this.spaceFormGroup?.controls[controlname].errors != null && this.submitted) {
      return true
    }
    return false;
  }
  loadRooms(model: any) {
    this.chatservice.GetRoomList(model).subscribe({
      next: (response) => {
        if (response.resultCode === 0) {
          this.rooms = response.items
        }
      }
    })
  }
  openspacePopup(popup: any) {
    popup.classList.add('show');
    this.chatservice.GetMySpaces().subscribe({
      next: (response) => {
        this.spaces = response;
      }
    })
  }
  initialize() {
    this.spaceFormGroup = this.formbuild.group({
      id: [null],
      creatorId: [null],
      name: ['', Validators.required],
      description: [''],
      isPrivate: [false],
      joinableMembers: [0],
      userName: [''],
      imagePath: [''],
    })
    this.roomFormGroup = this.formbuild.group({
      id: [null],
      spaceId: [null],
      creatorId: [null],
      name: ['', Validators.required],
      description: [''],
      enableEncryption: [false],
      accessType: [0],
      userName: [''],
      imagePath: [''],
      subject: [''],
    })
    this.imgUrl = ''
  }
  addSpace(popup: any) {
    this.main.main = false;
    this.space.main = true;
    this.space.state = true;
    popup.classList.remove('show')
  }
  cancelPopup(popup: any) {
    popup.classList.remove('show');
  }
  State(isprivate: boolean) {
    this.state = isprivate;
    this.isprivate = isprivate
    this.space.spaceform = true;
    this.space.state = false
    this.spaceFormGroup.controls['isPrivate'].setValue(isprivate)
  }

  member(accesstype: number) {
    this.roomFormGroup.controls['accessType'].setValue(accesstype)
    this.space.isprivate = false;
    this.space.roomform = true;
    this.submittedroom.accessType = accesstype
  }

  loadusers() {
    const model = {
      filter: ''
    }
    this.userservice.GetLightUserList(model).subscribe({
      next: (response) => {
        this.users = response
      }
    })

  }
  submitSpace(state: any) {
    this.disabled = true;
    const userId = JSON.parse(localStorage.getItem('userConfig') as any).userId;
    this.spaceFormGroup.controls['creatorId'].setValue(userId)
    if (!this.spaceFormGroup.valid) {
      this.submitted = true;
      return
    }

    const model = {
      entity: this.spaceFormGroup.value
    }
    setTimeout(() => {
      this._upserspace(model)
    }, 300);

  }
  _upserspace(model: any) {
    this.chatservice.UpsertSpace(model).subscribe({
      next: (response) => {
        this.init();
        this.initialize();
        this.loadRooms(this.roomsFilter);
        this.disabled = false;
      },
      error: (err) => {
        this.disabled = false;
      }
    }
    )
  }
  open(input: any) {
    input.click();
  }
  select(e: any, name: string) {
    this.file = e.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(this.file);
    reader.onload = () => {
      this.imgUrl = reader.result
    };
    this.uploadMode = name
  }
  uploadFile(file: any) {
    const formData = new FormData();
    formData.append('file', file);
    this._uploadfile(formData)
  }
  _uploadfile(formData: any) {
    this.upload.UploadFile(formData).subscribe(response => {
      this.spaceFormGroup.controls['imagePath'].setValue(response.cloudFileName);
      this.roomFormGroup.controls['imagePath'].setValue(response.cloudFileName);
      if (this.uploadMode === 'space') {
        const model = {
          entity: this.spaceFormGroup.value
        }
        setTimeout(() => {
          this._upserspace(model)
        }, 300);
      }
      else {
        const model = {
          entity: this.roomFormGroup.value
        }
        setTimeout(() => {
          this._upsertroom(model)
        }, 300);
      }

    })
  }

  addRoom(popup: any) {
    this.main.main = false;
    popup.classList.remove('show');
    this.space.main = true;
    this.space.isprivate = true;
  }
  submitRoom(popup: any) {
    this.disabled = true
    const userId = this.autenticationservice.getUserConfig().userId;
    this.roomFormGroup.controls['creatorId'].setValue(userId)
    if (!this.roomFormGroup.valid) {
      this.submitted = true;
      return
    }
    if (this.file) {
      this.uploadFile(this.file)
    }
    else {
      const model = {
        entity: this.roomFormGroup.value
      }

      setTimeout(() => {
        this._upsertroom(model);
      }, 300);
    }
  }
  _upsertroom(model: any) {
    this.chatservice.UpsertRoom(model).subscribe({
      next: (response) => {
        this.roomFormGroup.controls['id'].setValue(response)
        this.submittedroom.roomId = response;

        if (this.submittedroom.accessType === 1) {
          this.space.roomform = false;
          this.space.memeberlist = true;
          this.loadusers();
        }
        else {

          this.init();
          this.initialize();
          this.disabled = false;
          this.loadRooms(this.roomsFilter);
        }

      },
      error: (err) => {
        this.disabled = false;
      }
    })
  }
  SEARCH_VALUE(e: any, mode: number) {
    this.roomsFilter.search = e
    if (mode === 0) this.loadRooms(this.roomsFilter);
    else this.loadpublicRooms({ page: 0, pageCount: 0, userName: e })
  }

  chat(item: any, popup: any) {
    this.roomId = item.id;
    this.main.main = false;
    this.chatroom.room = true;
    this.space.main = false;
    this.main.header = false;
    this.room = item
    this.getroomMessages(item.id)
    popup.classList.remove('show');
  }

  getroomMessages(id: any) {
    const model = {
      roomId: id
    }
    this.chatservice.GetRoomMessages(model).subscribe({
      next: (response) => {
        this.messages = response
        this.socketservice.websoket(id)
      }
    })
  }

  close(mode: number) {
    this.messages = [];
    this.initialize();
    this.init();
    this._showAll('همه اتاق ها')
    if (mode === 0) {
      this.CLOSE.emit(false);
    }
    else {
      this.chatroom.room = false;
      this.main.main = true,
        this.main.header = true
      this.loadRooms(this.roomsFilter);
    }
  }


  async getspaceRooms(item: any, popup: any) {
    this.roomFormGroup.controls['spaceId'].setValue(item.id)
    this.spaceId = item.id;
    this.title = item.name;
    this.showAll = true;
    await this._getspacerooms(item.id).then(result => {
      popup.classList.remove('show')
      this.main.addroombtn = true;
    })
  }

  async _getspacerooms(id : any) {
    this.chatservice.GetSpaceRooms(id).subscribe({
      next: (response) => {
        this.rooms = response
      }
    })
  }

  _showAll(title: string) {
    this.title = title;
    this.showAll = false;
    this.loadRooms(this.roomsFilter);
    this.init();
    this.initialize();
    this.popup.nativeElement.classList.remove('show');
    this.spaceId = ''
  }

  openpublicRooms(_publicRooms: any) {
    _publicRooms.classList.add('show');
    const model = {
      page: 0,
      pageCount: 0,
      userName: ""
    }
    this.loadpublicRooms(model)
  }

  loadpublicRooms(model: any) {
    this.chatservice.GetPublicRooms(model).subscribe({
      next: (response) => {
        this.allpublics = response.items

      }
    })
  }
  openModal(item: any) {
    this.publicroom = item
    this.roomMembers({ page: 0, pageCount: 0, userName: '', roomId: item.id })
  }

  roomMembers(model: any) {
    this.chatservice.GetRoomMembers(model).subscribe({
      next: (response) => {
        this.members = response.items
      }
    })
  }

  join(item: any, publicRooms: any) {
    this.joinroom(item, publicRooms)
  }

  joinroom(item: any, publicRooms: any) {
    const userId = JSON.parse(localStorage.getItem('userConfig') as any).userId;
    const model = {
      userId: userId,
      roomId: item.id
    }
    this.chatservice.JoinToRoom(model).subscribe({
      next: (response) => {
        if (response) {
          this.publicroomdialog.isVissible = false
          this.room = item
          publicRooms.classList.remove('show');
          this.getroomMessages(item.id);
          this.main.main = false;
          this.chatroom.room = item.id;
          this.space.main = false;
          this.main.header = false;
        }
      }
    })
  }

  selectedUsers(e: any) {
    this.joinedtoroomUsers = e
  }

  jointoroomusers() {
    const promise = new Promise<void>((resolve, reject) => {
      this.joinedtoroomUsers.forEach((el, i, array) => {
        el.roomId = this.submittedroom.roomId
        this._jointoRoom({
          roomId: el.roomId,
          userId: el.id
        })
        if (i === array.length - 1) { resolve() }
      })
    });

    promise.then(res => {
      this.init();
      this.initialize();
      this.disabled = false;
      this.loadRooms(this.roomsFilter);
    })
  }

  _jointoRoom(model: any) {
    this.chatservice.JoinToRoom(model).subscribe({
      next: (response) => {
      }
    })
  }

  _deleteroom(item: any) {
    this.chatservice.DeleteRoom(item.id).subscribe({
      next: () => {
        if (this.showAll) {
          this._getspacerooms(this.spaceId)
        }
        else {
          this.loadRooms(this.roomsFilter);
        }
      }
    })
  }
}
