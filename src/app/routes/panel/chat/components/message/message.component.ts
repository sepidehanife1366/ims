import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { WebSocketService } from 'src/app/core/services/websocket.service';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit, OnChanges, OnDestroy {
  @Input() messages: any;
  @Output() LOADING = new EventEmitter();
  msgs: any = []
  user: any;
  video: any;
  picture: any;

  constructor(private socketservice: WebSocketService, private cloudservice: CloudStorageService) { }
  ngOnChanges(changes: SimpleChanges): void {
    if (!this.messages?.length)
      return

    this.msgs = this.messages
    setTimeout(() => {
      this._getrecievedmsgImage(this.msgs);
    }, 100);

  }
  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userConfig') as any);
    this.WebSocket();
  }
  WebSocket() {
    this.socketservice.webSocketRecive.subscribe((msg: any) => {
      if (msg != null) {
        const _msg = JSON.parse(msg);
        this.msgs.push(_msg);
        setTimeout(() => {
          this._getrecievedmsgImage(this.msgs);
          this.LOADING.emit(true)
        }, 100);
      }
    })
  }

  _getrecievedmsgImage(msgs: any) {
    msgs.forEach((_msg: any) => {
      if (_msg.fileType === 3) {
        this.getImage(_msg.filePath)
      }
    });
  }
  ngOnDestroy(): void {
    this.messages = [];
    this.socketservice.disconnected();
  }
  getImage(path: any) {
    const model = {
      imageQuality: 0,
      filePath: path
    }
    this.cloudservice.GetImageBase64(model).subscribe({
      next: (response) => {
        this.picture = 'data:image/png;base64,' + response;
      }
    })
  }
  _download(item : any) {
    this._getFile(item);
  }

  _getFile(item : any){
    this.cloudservice.GetFile(item.filePath).subscribe(res=>{
      const blob = new Blob([res]);
       FileSaver.saveAs(blob ,item.filePath);
    })
  }
}
