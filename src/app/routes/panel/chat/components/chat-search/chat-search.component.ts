import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-chat-search',
  templateUrl: './chat-search.component.html',
  styleUrls: ['./chat-search.component.scss']
})
export class ChatSearchComponent implements OnInit {
  @Output() SEARCH_VALUE = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }

  search(e : any){
    this.SEARCH_VALUE.emit(e.value)    
  }

}
