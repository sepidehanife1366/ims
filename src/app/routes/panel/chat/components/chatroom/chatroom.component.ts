import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ChatService } from 'src/app/core/services/chat.service';
import { CloudStorageService } from 'src/app/core/services/cloudStorage.service';
import { UserService } from 'src/app/core/services/user.service';
import { WebSocketService } from 'src/app/core/services/websocket.service';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.scss'],
  animations: [
    trigger('dialog', [
      state('visible',
        style({
          transform: 'translate(0 , 0) scale(1)',
          opacity: 1
        })),
      state('void,hidden', style({
        transform: 'translate(0, 0) scale(0)',
        opacity: 0
      })),
      transition('*=>vissible', animate('250ms')),
      transition('*=>void , *=>vissible', animate('180ms')),
    ])
  ]
})
export class ChatroomComponent implements OnInit, OnChanges {
  appform: FormGroup | any
  @Input() room: any;
  @Input() messages: any;
  @Output() CLOSE = new EventEmitter();

  roomId: any;
  user: any;
  emojis = ['😀', '😄', '👋', '👍', '😇', '😂']
  emoji = false;

  profile = {
    show: false
  }
  filter = {
    page: 0,
    pageCount: 0,
    userName: "",
    roomId: ""
  }
  members: any[] = []
  addmemberdialog = {
    isVissible: false
  }
  users: any = []
  userfilter = {
    filter: ''
  }
  selectedUser: any = []
  @ViewChild('user') _user !: any;
  loading = false;

  constructor(
    private chatservice: ChatService,
    public elementRef: ElementRef,
    private renderer: Renderer2,
    private formbuild: FormBuilder,
    private upload: CloudStorageService,
    private socketservice: WebSocketService,
    private toast: ToastrService,
    private userservice: UserService,
    private router: Router
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('userConfig') as any);
    this.initialize();
  }
  initialize() {
    this.appform = this.formbuild.group({
      id: [null],
      roomId: [''],
      senderId: [''],
      text: ['', Validators.required],
      senderName: [''],
      filePath: [''],
      fileType: [''],
      hasFile: [false]
    })
  }

  sendMessage() {
    this.appform.controls['senderId'].setValue(this.user.userId);
    this.appform.controls['roomId'].setValue(this.room.id);
    this.appform.controls['senderName'].setValue(this.user.username);
    this.appform.controls['filePath'].setValue('')
    this.appform.controls['fileType'].setValue(null)
    this.appform.controls['hasFile'].setValue(false);
    if (this.appform.controls['text'].value != '') {
      this.socketservice.onSend(this.room.id, JSON.stringify(this.appform.value))
    }
    this.appform.controls['text'].setValue('')
  }
  selectEmoji(item: any, input: any) {
    this.appform.controls['text'].setValue(item);
    input.focus();
  }

  uploadSocketFile(file: any) {
    const formData = new FormData();
    formData.append('file', file);
    this.upload.UploadFile(formData).subscribe(response => {
      if (response.resultCode === 0) {
        this.appform.controls['filePath'].setValue(response.cloudFileName)
        this.appform.controls['fileType'].setValue(response.fileType)
        this.appform.controls['hasFile'].setValue(true);
        this.appform.controls['senderId'].setValue(this.user.userId);
        this.appform.controls['senderName'].setValue(this.user.username);
        this.appform.controls['text'].setValue('');
        this.appform.controls['roomId'].setValue(this.room.id);

        setTimeout(() => {
          this.socketservice.onSend(this.room.id, JSON.stringify(this.appform.value))
        }, 300);
      }
    }, err => {
      this.loading = false;
    })
  }
  _loading(e : boolean) {
    this.loading = false;
  }
  openfile(input: any) {
    input.click();
  }
  socketfile(e: any) {
    this.loading = true;
    const myPromise = new Promise((resolve, reject) => {
      setTimeout(() => {
        const file = e.files[0];
          if (file != '') {
            resolve(file);
          }
      }, 300);
    });
    myPromise.then(res => {
      this.uploadSocketFile(res)
    })
  }

  close(mode: number) {
    if (mode === 2) {
      this.profile.show = false;
    }
    else {
      this.CLOSE.emit(mode);
      this.socketservice.disconnected();
    }
  }

  _profile() {
    this.profile.show = true;
    this.filter.roomId = this.room.id
    this._getmembers(this.filter);
  }

  search_value(e: any) {
    this.filter.userName = e
    this._getmembers(this.filter);
  }

  _getmembers(model: any) {
    this.chatservice.GetRoomMembers(model).subscribe({
      next: (response) => {
        this.members = response.items
      }
    })
  }

  _addmemberdialog() {
    this.getusers(this.userfilter)
  }
  getusers(filter: any) {
    this.userservice.GetLightUserList(filter).subscribe(response => {
      this.users = response
    })
  }

  _selectedUser(e: any, item: any) {
    item.selected = e
    this.selectedUser.push(item);
    this.selectedUser = [...new Set(this.selectedUser)]
  }

  _addmember() {
    this.selectedUser.forEach((user: any) => {
      const model = {
        userId: user.id,
        roomId: this.room.id
      }
      this.joinmember(model)
    });
  }

  joinmember(model: any) {
    this.chatservice.JoinToRoom(model).subscribe(response => {
      this._getmembers(this.filter);
    })
  }
  _navigatetoprofile(item: any) {
    this.router.navigate(['profile'], { queryParams: { id: item.id } })
  }

}
