import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/core/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cartable',
  templateUrl: './cartable.component.html',
  styleUrls: ['./cartable.component.scss']
})
export class CartableComponent implements OnInit {
  cartables : any
  modalRef?: BsModalRef;
  subjectId = ''
  constructor(
    private userservice : UserService,
    private modalService: BsModalService,
    private router : Router,
    private activatedRoute : ActivatedRoute,
    private toastservice : ToastrService

  ) { 

  }
 
  open(item : any) {
    Swal.fire({
      html : 
      '<strong class="fs-14 mb-3">'+ 'آیا می خواهید ' + item.title + ' '+  'اقدام شود؟ ' +'</strong>'
      ,
      showCancelButton: true,
      confirmButtonText: 'بله',
      cancelButtonText: 'انصراف',
      customClass: {
        confirmButton: 'btn btn-success fs-12 round-0 mr-5',
        cancelButton: 'btn btn-danger fs-12'
      },
      buttonsStyling: false,
      reverseButtons: true,
    }).then(result =>{
        if (result.isConfirmed) {
          this.router.navigate(['/panel'] , {queryParams : {id : item.subjectId , approverRole : item.approverRole , action : 'edit'}})
        }
      
    })
  }

  ngOnInit(): void {
    this.userservice.reload.subscribe(result =>{
      if (result) {
        this.loadcartables();
      }
    })
    this.loadcartables();
  }
  loadcartables(){
    this.userservice.cartables.subscribe((result : any) =>{
      this.cartables = result      
    })
  }

  remove(item : any){
    Swal.fire({
      html : 
      '<strong class="fs-14 mb-3">'+ 'آیا از حذف ' + item.title + ' '+  'مطمئن هستید ؟ ' +'</strong>'
      + '<p class="fs-12 m-0 text-danger"> این عملیات برگشت ناپذیر است !</p>'
      ,
      showCancelButton: true,
      confirmButtonText: 'بله, حذف شود!',
      cancelButtonText: 'انصراف',
      customClass: {
        confirmButton: 'btn btn-success fs-12 round-0 mr-5',
        cancelButton: 'btn btn-danger fs-12'
      },
      buttonsStyling: false,
      reverseButtons: true,
    }).then(result =>{
        if (result.isConfirmed) {
          this.userservice.DeleteUserCartable(item.id).subscribe({
            next : (respose)=>{
              if (respose.resultCode === 0) {
                this.toastservice.success('حذف شد');
                this.loadcartables();
                
              }
            }
          })
        }
    })
  }

}
