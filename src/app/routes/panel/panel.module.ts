import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingComponent } from './setting/setting.component';
import { FormsComponent } from './setting/forms/forms.component';
import { SharedModule } from 'src/app/sharedComponents/shared.module';
import { UsersComponent } from './setting/users/users.component';
import { UpdateuserComponent } from './setting/users/updateuser/updateuser.component';
import { ProfileComponent } from './profile/profile.component';
import { EditprofileComponent } from './profile/editprofile/editprofile.component';
import { PostcardComponent } from './dashboard/postcard/postcard.component';
import { ColleaguesComponent } from './profile/colleagues/colleagues.component';
import { UsersroleComponent } from './setting/usersrole/usersrole.component';
import { UpdateuserroleComponent } from './setting/usersrole/updateuserrole/updateuserrole.component';
import { PermissionsComponent } from './setting/permissions/permissions.component';
import { UpdateformComponent } from './setting/forms/updateform/updateform.component';
import { MultiplechoiceformComponent } from './setting/forms/updateform/multiplechoiceform/multiplechoiceform.component';
import { DateformComponent } from './setting/forms/updateform/dateform/dateform.component';
import { TitleformComponent } from './setting/forms/updateform/titleform/titleform.component';
import { NumberformComponent } from './setting/forms/updateform/numberform/numberform.component';
import { DescribtionformComponent } from './setting/forms/updateform/describtionform/describtionform.component';
import { SinglechoiceformComponent } from './setting/forms/updateform/singlechoiceform/singlechoiceform.component';
import { SelectformComponent } from './setting/forms/updateform/selectform/selectform.component';
import { TextformComponent } from './setting/forms/updateform/textform/textform.component';
import { LongtextformComponent } from './setting/forms/updateform/longtextform/longtextform.component';
import { EditorformComponent } from './setting/forms/updateform/editorform/editorform.component';
import { FormfieldComponent } from './setting/forms/updateform/formfield/formfield.component';
import { KnowledgepackageComponent } from './setting/knowledgepackage/knowledgepackage.component';
import { EvaluatedKnowledgeComponent } from './setting/evaluated-knowledge/evaluated-knowledge.component';
import { EveluatedquastionComponent } from './setting/evaluated-knowledge/eveluatedquastion/eveluatedquastion.component';
import { SettopicComponent } from './setting/knowledgepackage/settopic/settopic.component';
import { SetknowledgeengineertopicComponent } from './setting/knowledgepackage/setknowledgeengineertopic/setknowledgeengineertopic.component';
import { SearchComponent } from './search/search.component';
import { SubjectComponent } from './subject/subject.component';
import { TopicsComponent } from './dashboard/topics/topics.component';
import { CartableComponent } from './cartable/cartable.component';
import { ReportComponent } from './report/report.component';
import { UserGraphComponent } from './user-graph/user-graph.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data : {
      title : 'صفحه اصلی'
    }
  },
    {
    path: 'panel',
    component: DashboardComponent,
    data : {
      title : 'صفحه اصلی'
    }
  },
  {
    path: 'setting',
    component: SettingComponent,
    data : {
      title : 'صفحه اصلی/ تنظیمات'
    }
  },
  {
    path: 'forms',
    component: FormsComponent,
    data : {
      title : 'صفحه اصلی/ فرم ها'
    }
  }
  ,
  {
    path: 'updateform',
    component: UpdateformComponent,
    data : {
      title : 'صفحه اصلی/ ویرایش فرم'
    }
  },
  {
    path: 'users',
    component: UsersComponent,
    data : {
      title : 'صفحه اصلی/ کاربران'
    }

  },
  {
    path: 'updateuser',
    component: UpdateuserComponent,
    data : {
      title : 'صفحه اصلی/ کاربر',
    }
  },
  {
    path: 'usersrole',
    component: UsersroleComponent,
    data : {
      title : 'صفحه اصلی/ نقش ها'
    }

  },
  {
    path: 'permissions',
    component: PermissionsComponent,
    data : {
      title : 'صفحه اصلی/ سطوح دسترسی'
    }

  },
  {
    path: 'knowledge',
    component: KnowledgepackageComponent,
    data : {
      title : 'صفحه اصلی/ بسته های دانشی'
    }

  },
  {
    path: 'evaluatedknowledge',
    component: EvaluatedKnowledgeComponent,
    data : {
      title : 'صفحه اصلی/ ارزیابی دانش'
    }

  },
  // {
  //   path: 'updatepermission',
  //   component: PermissionsComponent,

  // },
  {
    path: 'updaterole',
    component: UpdateuserroleComponent
  },
  {
    path: 'profile',
    component: ProfileComponent ,
    data : {
      title : 'صفحه اصلی/ پروفایل'
    }
   
  }
  ,
  {
    path: 'search',
    component: SearchComponent ,
    children : 
    [
      { path: '', loadChildren: () => import('./search/search.module').then(m => m.SearchModule) },
    ]
   
  },
  {
    path: 'editprofile',
    component: EditprofileComponent ,
    children : 
    [
      { path: '', loadChildren: () => import('./profile/editprofile/editprofile.module').then(m => m.EditprofileModule) },

    ]

  },
  {
    path: 'subject',
    component: SubjectComponent ,
    data : {
      title : 'صفحه اصلی/ موضوعات'
    }
   
  },

  {
    path: 'cartable',
    component: CartableComponent ,
    data : {
      title : 'صفحه اصلی/ کارتابل'
    }
   
  },
    {
    path: 'report',
    component: ReportComponent ,
    children : 
    [
      { path: '', loadChildren: () => import('./report/report.module').then(m => m.ReportModule) },
    ],
    data : {
      title : 'صفحه اصلی/ گزارشات'
    }
   
  },
  {
    path: 'graph',
    component: UserGraphComponent ,
    data : {
      title : 'صفحه اصلی/ گراف'
    }
   
  },
];

@NgModule({
  declarations: [
    DashboardComponent,
    SettingComponent,
    FormsComponent,
    UsersComponent,
    UpdateuserComponent,
    ProfileComponent,
    EditprofileComponent,
    PostcardComponent,
    ColleaguesComponent,
    UsersroleComponent,
    UpdateuserroleComponent,
    PermissionsComponent,
    UpdateformComponent,
    MultiplechoiceformComponent,
    DateformComponent,
    TitleformComponent,
    NumberformComponent,
    DescribtionformComponent,
    SinglechoiceformComponent,
    SelectformComponent,
    TextformComponent,
    LongtextformComponent,
    EditorformComponent,
    FormfieldComponent,
    KnowledgepackageComponent,
    EvaluatedKnowledgeComponent,
    EveluatedquastionComponent,
    SettopicComponent,
    SetknowledgeengineertopicComponent,
    SearchComponent,
    SubjectComponent,
    TopicsComponent,
    CartableComponent,
    ReportComponent,
    UserGraphComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule.forRoot(),
    RouterModule.forChild(routes),
  
  ]
})
export class PanelModule { }