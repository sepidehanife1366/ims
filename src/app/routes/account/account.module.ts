import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'change-password',
    // component: ChangePasswordComponent,
  },
  {
    path: 'forgetpassword',
    component: ForgetpasswordComponent,
  },
];

@NgModule({
  declarations: [
    LoginComponent,
    ForgetpasswordComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class AccountModule { }