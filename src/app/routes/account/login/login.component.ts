import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { UserService } from 'src/app/core/services/user.service';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  appform: FormGroup | any
  loginimage = true;
  show = false
  type = 'password';

  constructor(
    private formbuild: FormBuilder,
    private authservice: AuthenticationService,
    private userservice: UserService,
    private route: Router,
    private toastrservice: ToastrService,
    private permissionsService: NgxPermissionsService

  ) { 
    if (window.innerWidth <= 992) this.loginimage =  false
    else this.loginimage =  true
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    if (event.target.innerWidth >= 992) this.loginimage =  true
    else this.loginimage =  false
  }

  ngOnInit(): void {
    this.initialize();
  }

  initialize() {
    this.appform = this.formbuild.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      mfToken: [''],
    })
  }
  submit() {
    this.authservice.login(this.appform.value).subscribe({
      next: (response) => {
        if (response != undefined) {
          const _promise = new Promise((resolve, reject) => {
            this.authservice.setUserConfig(response.roles, response.token, true, response.username, '', '', '', response.id);
            localStorage.setItem('token', response.token)
            this.authservice.token.next(response.token);
            this.toastrservice.success('خوش آمدید')
            this.route.navigate(['/']);
            resolve(true);
          });
          _promise.then(res => {
            if (res) {
              this.getCurrentuser(response.roles, response.token, true, response.username, response.id);
            }
          })
        }
      }
      , error: (err) => {
        console.log('error');
        this.toastrservice.error('نام کاربری یا رمز عبور اشتباه است')

      }
    })
  }
  getCurrentuser(roles: any, token: string, islogged: boolean, username: string, userId: number) {
    this.userservice.GetCurrentUser().subscribe(response => {
      this.authservice.setUserConfig(roles, token, islogged, username, response.firstname, response.lastname, response.pictureName, userId);
      this.authservice.currentUser.next(response);
      this.permissionsService.loadPermissions(response.permissions)
    })
  }

  showpass() {
    this.show = !this.show
    if (this.show) {
      this.type = 'text'
    }
    else {
      this.type = 'password'
    }
  }

  

}
