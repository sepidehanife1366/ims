import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {

  appform : FormGroup | any

  constructor(
    private formbuild : FormBuilder,
    private authservice : AuthenticationService,
    private route : Router
  ) { }

  ngOnInit(): void {
    this.initialize();

  }

  initialize(){
    this.appform = this.formbuild.group({
      userName : ['' , Validators.required],
      urlCallback : [''],
    })
  }
  submit(){
    this.authservice.ForgetPassword(this.appform.value).subscribe({
      next : (response) => {
        if (response !=undefined) {
          if (response.resultCode === 0) {
            this.route.navigate(['/auth']);
          }
      }
    }
      , error : (err) =>{
        console.log('error');
      }
    })
  }
}
