export const environment = {
  production: false,
  httpConfig: {
    headers: {
      // 'Content-Type': 'application/json',
      Accept: 'application/json',
      'Accept-Language': 'fa',
      Authorization: '',
    },
    url: 'http://188.121.109.156:5036/api/',
    imageUrl: 'http://188.121.109.156:4000/cloudStorage/uploads/',
    HubQueueName : '' ,
    // HubexchangeName: '/exchange/VisitorsExchange/arguments=',
    webSocketAddress: 'ws://188.121.109.156:15674/ws',
    webSocketLogin: 'admin',
    webSocketPassword: 'admin',
    webSocketDebugMode: false,

  }
}
